'use strict';
window.onload = function () {
    colorearInputEscrituraDespuesErrorNuevoUsuario();
    eliminarLetraETypeNumber();
};

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    window.location.href = '../index/index.php';
});

function eliminarLetraETypeNumber() {

    let ids = [];
    let inputs = document.getElementsByTagName('input');
    for (let input of inputs) {
        if (input.id != '' && input.type == 'number') {
            ids.push(input.id);
        }
    }

    for (let id of ids) {
        document.getElementById(id).addEventListener('focus', () => {
            document.getElementById(id).addEventListener('keydown', function (event) {
                if (event.keyCode === 69) {
                    event.preventDefault();
                }
            });
        });
    }
}

function colorearInputEscrituraDespuesErrorNuevoUsuario() {
    let ids = [];

    let inputs = document.getElementsByTagName('input');

    for (let input of inputs) {
        if (input.id != '' && input.type != 'date' && input.type != 'radio') {
            ids.push(input.id);
        }
    }

    for (let id of ids) {
        document.getElementById(id).addEventListener('focus', () => {
            document.getElementById(id).addEventListener('keydown', () => {
                if (isNaN(document.getElementById(id).valueAsNumber)) {
                    colorVerdeGeneral(id);
                } else {

                    const teclaPresionada = String.fromCharCode(event.keyCode);

                    if (teclaPresionada == '`' || teclaPresionada == 'a' || teclaPresionada == 'b' ||
                            teclaPresionada == 'c' || teclaPresionada == 'd' || teclaPresionada == 'e' ||
                            teclaPresionada == 'f' || teclaPresionada == 'g' || teclaPresionada == 'h' ||
                            teclaPresionada == 'i' || teclaPresionada == '0' || teclaPresionada == '1' ||
                            teclaPresionada == '2' || teclaPresionada == '3' || teclaPresionada == '4' ||
                            teclaPresionada == '5' || teclaPresionada == '6' || teclaPresionada == '7' ||
                            teclaPresionada == '8' || teclaPresionada == '9') {
                        colorVerdeGeneral(id);

                    }
                }
            });
        });
    }
}

function colorVerdeGeneral(id) {
    document.getElementById(id).style.color = 'var(--green-900)';
    document.getElementById(id).style.letterSpacing = '0.2rem';
    document.getElementById(id).style.fontWeight = 'bolder';
}