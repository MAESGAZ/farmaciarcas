'use strict';
function ocultarMenus() {
    document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
    document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
    document.querySelector('.listado-pedidos').style.display = 'none';
    document.querySelector('.contenedor-formulario-cambiar-clave').style.display = 'none';
    document.querySelector('.contenedor-formCambClave').style.display = 'none';
    cambiarClasesBotonHamburguesa();
}

function cambiarClasesBotonHamburguesa() {
    if (document.getElementById('tDivBotonHamburguesa').classList[1] === 'boton-hamburguesa-blanco') {
        document.getElementById('tDivBotonHamburguesa').classList.remove('boton-hamburguesa-blanco');
        document.getElementById('tUlContenedorMenuNav').classList.remove('contenedor-menu-nav-movil');
        document.getElementById('tDivBotonHamburguesa').classList.add('boton-hamburguesa-verde');
        document.getElementById('tDivLinea1').classList.remove('linea-hamburguesa-verde');
        document.getElementById('tDivLinea2').classList.remove('linea-hamburguesa-verde');
        document.getElementById('tDivLinea3').classList.remove('linea-hamburguesa-verde');
    }
}

function programarEventos() {
    document.querySelector('.gestionar-clientes').onclick = mostrarDesplegableClientes;
    document.querySelector('.gestionar-productos').onclick = mostrarDesplegableProductos;
    document.querySelector('.gestionar-medicamentos').onclick = mostrarDesplegableMedicamentos;
    document.querySelector('.gestionar-pedidos').onclick = mostrarDesplegablePedidos;
    document.querySelector('.desplegable-listado-pedidos').onclick = mostrarDesplegableListadoPedidos;
    document.querySelector('.contenedor-triangulo-cambiar-clave').onclick = mostrarFormularioCambioClave;
    document.querySelector('.desplegable-cambiar-clave').onclick = mostrarFormCambClave;
    document.getElementById('tDivBotonHamburguesa').onclick = mostrarMenuNav;
}

function mostrarDesplegableClientes() {
    document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
    if (document.querySelector('.contenedor-desplegable-clientes').style.display === 'none') {
        document.querySelector('.contenedor-desplegable-clientes').style.display = 'block';
    } else {
        document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
    }
    borderRadiusMenuNavMdLg();
}

function mostrarDesplegableProductos() {
    document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
    document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
    if (document.querySelector('.contenedor-desplegable-productos').style.display === 'none') {
        document.querySelector('.contenedor-desplegable-productos').style.display = 'block';
    } else {
        document.querySelector('.contenedor-desplegable-productos').style.display = "none";
    }
    borderRadiusMenuNavMdLg();
}

function mostrarDesplegableMedicamentos() {
    document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
    document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
    if (document.querySelector('.contenedor-desplegable-medicamentos').style.display === 'none') {
        document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'block';
    } else {
        document.querySelector('.contenedor-desplegable-medicamentos').style.display = "none";
    }
    borderRadiusMenuNavMdLg();
}

function borderRadiusMenuNavMdLg() {
    if (screen.width >= 577 && screen.width <= 992) {
        if (document.querySelector('.contenedor-desplegable-pedidos').style.display === 'none') {
            document.querySelector('.gestionar-pedidos').style.borderRadius = '0 0 0 1.5rem';
        } else {
            if (document.querySelector('.contenedor-desplegable-pedidos').style.display === 'none') {
                document.querySelector('.gestionar-pedidos').style.borderRadius = '0';
            } else {
                document.querySelector('.gestionar-pedidos').style.borderRadius = '0';
            }
        }
    } else {
        document.querySelector('.gestionar-pedidos').style.borderRadius = '0';
    }
}

function mostrarDesplegablePedidos() {
    document.querySelector('.listado-pedidos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
    document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
    if (document.querySelector('.contenedor-desplegable-pedidos').style.display === 'none') {
        document.querySelector('.contenedor-desplegable-pedidos').style.display = 'block';
    } else {
        document.querySelector('.contenedor-desplegable-pedidos').style.display = "none";
    }
    borderRadiusMenuNavMdLg();
}

function mostrarDesplegableListadoPedidos() {
    document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
    document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
    document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
    if (document.querySelector('.listado-pedidos').style.display === 'none') {
        document.querySelector('.desplegable-listado-pedidos').style.borderBottom = 'none';
        document.querySelector('.listado-pedidos').style.display = 'flex';
    } else {
        document.querySelector('.listado-pedidos').style.display = 'none';
        document.querySelector('.desplegable-listado-pedidos').style.borderBottom = 'solid 0.5px var(--green-900)';
    }
    borderRadiusMenuNavMdLg();
}

function mostrarFormularioCambioClave() {
    if (document.querySelector('.contenedor-formulario-cambiar-clave').style.display === 'none') {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.display = 'flex';
        document.querySelector('.contenedor-formCambClave').style.display = 'none';
        document.querySelector('.desplegable-cambiar-clave').style.borderRadius = '0 0 0.7rem 0.7rem';
        if (document.getElementById('tUlContenedorMenuNav').classList.value === 'contenedor-menu-nav') {
            document.querySelector('section').style.opacity = 0.3;
            document.querySelector('section').style.zIndex = -1;
        } else {
            document.getElementById('tUlContenedorMenuNav').classList.remove('contenedor-menu-nav-movil');
            document.getElementById('tUlContenedorMenuNav').classList.add('contenedor-menu-nav');
            if (document.getElementById('tDivBotonHamburguesa').classList[0] === 'boton-hamburguesa-blanco') {
                document.getElementById('tDivBotonHamburguesa').classList.remove('boton-hamburguesa-blanco');
                document.getElementById('tDivBotonHamburguesa').classList.add('boton-hamburguesa-verde');
                document.getElementById('tDivLinea1').classList.remove('linea-hamburguesa-verde');
                document.getElementById('tDivLinea2').classList.remove('linea-hamburguesa-verde');
                document.getElementById('tDivLinea3').classList.remove('linea-hamburguesa-verde');
                document.querySelector('section').style.opacity = 0.3;
                document.querySelector('section').style.zIndex = -1;
                document.getElementById('tDivLinea1').classList.add('linea-hamburguesa-blanca');
                document.getElementById('tDivLinea2').classList.add('linea-hamburguesa-blanca');
                document.getElementById('tDivLinea3').classList.add('linea-hamburguesa-blanca');
                document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
                document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
                document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
                document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
                document.querySelector('.listado-pedidos').style.display = 'none';
            }
        }
    } else {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.display = 'none';
        document.querySelector('.contenedor-formCambClave').style.display = 'none';
        document.querySelector('.desplegable-cambiar-clave').style.borderRadius = '0.7rem 0.7rem 0 0';
        document.querySelector('section').style.opacity = 1;
        document.querySelector('section').style.zIndex = 0;
    }
}

function mostrarFormCambClave() {
    if (document.querySelector('.contenedor-formCambClave').style.display === 'none') {
        document.querySelector('.desplegable-cambiar-clave').style.borderRadius = '0.7rem 0.7rem 0 0';
        document.querySelector('.contenedor-formCambClave').style.display = 'flex';
    } else {
        document.querySelector('.desplegable-cambiar-clave').style.borderRadius = '0 0 0.7rem 0.7rem';
        document.querySelector('.contenedor-formCambClave').style.display = 'none';
    }
}

function opacidadSection_XL_XXL() {
    if (screen.width >= 993) {
        if (document.querySelector('.contenedor-formulario-cambiar-clave').style.display === 'flex') {
            document.querySelector('section').style.opacity = 0.3;
            document.querySelector('section').style.zIndex = -1;
        } else {
            document.querySelector('section').style.opacity = 1;
            document.querySelector('section').style.zIndex = 0;
        }
    } else {
        if (document.getElementById('tDivBotonHamburguesa').classList[0] === 'boton-hamburguesa-verde') {
            if (document.querySelector('.contenedor-formulario-cambiar-clave').style.display === 'flex') {
                document.querySelector('section').style.opacity = 0.3;
                document.querySelector('section').style.zIndex = -1;
            } else {
                document.querySelector('section').style.opacity = 1;
                document.querySelector('section').style.zIndex = 0;
            }
        } else {
            document.querySelector('section').style.opacity = 0.3;
            document.querySelector('section').style.zIndex = -1;
        }
    }
}

function mostrarMenuNav() {
    document.querySelector('.contenedor-formulario-cambiar-clave').style.display = 'none';
    document.querySelector('.contenedor-formCambClave').style.display = 'none';
    document.querySelector('.desplegable-cambiar-clave').style.borderRadius = '0 0 0.7rem 0.7rem';
    if (document.getElementById('tDivBotonHamburguesa').classList[0] === 'boton-hamburguesa-verde') {
        document.getElementById('tDivBotonHamburguesa').classList.remove('boton-hamburguesa-verde');
        document.getElementById('tDivBotonHamburguesa').classList.add('boton-hamburguesa-blanco');
        document.querySelector('section').style.opacity = 0.3;
        document.querySelector('section').style.zIndex = -1;
        document.getElementById('tDivLinea1').classList.remove('linea-hamburguesa-blanca');
        document.getElementById('tDivLinea2').classList.remove('linea-hamburguesa-blanca');
        document.getElementById('tDivLinea3').classList.remove('linea-hamburguesa-blanca');
        document.getElementById('tDivLinea1').classList.add('linea-hamburguesa-verde');
        document.getElementById('tDivLinea2').classList.add('linea-hamburguesa-verde');
        document.getElementById('tDivLinea3').classList.add('linea-hamburguesa-verde');
    } else {
        document.getElementById('tDivBotonHamburguesa').classList.remove('boton-hamburguesa-blanco');
        document.getElementById('tDivBotonHamburguesa').classList.add('boton-hamburguesa-verde');
        document.getElementById('tDivLinea1').classList.remove('linea-hamburguesa-verde');
        document.getElementById('tDivLinea2').classList.remove('linea-hamburguesa-verde');
        document.getElementById('tDivLinea3').classList.remove('linea-hamburguesa-verde');
        document.getElementById('tDivLinea1').classList.add('linea-hamburguesa-blanca');
        document.getElementById('tDivLinea2').classList.add('linea-hamburguesa-blanca');
        document.getElementById('tDivLinea3').classList.add('linea-hamburguesa-blanca');
        document.querySelector('section').style.opacity = 1;
        document.querySelector('section').style.zIndex = 0;
        document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
        document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
        document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
        document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
        document.querySelector('.listado-pedidos').style.display = 'none';
    }

    if (document.getElementById('tUlContenedorMenuNav').classList.value === 'contenedor-menu-nav') {
        document.getElementById('tUlContenedorMenuNav').classList.remove('contenedor-menu-nav');
        document.getElementById('tUlContenedorMenuNav').classList.add('contenedor-menu-nav-movil');
    } else {
        document.getElementById('tUlContenedorMenuNav').classList.remove('contenedor-menu-nav-movil');
        document.getElementById('tUlContenedorMenuNav').classList.add('contenedor-menu-nav');
    }
}

function pincharFueraMenuNav() {
    window.addEventListener('mouseup', function (event) {
        const nMenuNav = document.getElementById('tUlContenedorMenuNav');
        const nCambiarClave = document.querySelector('.contenedor-formulario-cambiar-clave');

        if (event.target != nMenuNav && event.target != nCambiarClave &&
                verificarElementosPinchados(event) == true
                ) {
            menuAbierto(nMenuNav, nCambiarClave);
            limpiarFormularioCambiarClave();
        } else if (elementosMenuHamburguesa() == true) {
            limpiarFormularioCambiarClave();
        }
    });
}

function limpiarFormularioCambiarClave() {
    document.querySelector('.form-cambiar-clave').reset();
}

function elementosMenuHamburguesa() {
    if (elementoPinchado(event) == 'conjunto-lineas-hamburguesa' ||
            elementoPinchado(event) == 'linea-hamburguesa-verde' ||
            elementoPinchado(event) == 'linea-hamburguesa-blanca' ||
            elementoPinchado(event) == 'boton-hamburguesa-verde' ||
            elementoPinchado(event) == 'boton-hamburguesa-blanco') {
        return true;
    }
}

function verificarElementosPinchados(event) {
    if ((elementoPinchado(event) != 'gestionar-clientes' &&
            elementoPinchado(event) != 'contenedor-desplegable-clientes' &&
            elementoPinchado(event) != 'ul-contenedor-desplegable-clientes' &&
            elementoPinchado(event) != 'li-listado-clientes' &&
            elementoPinchado(event) != 'a-listado-clientes' &&
            elementoPinchado(event) != 'li-nuevo-cliente' &&
            elementoPinchado(event) != 'a-nuevo-cliente' &&
            elementoPinchado(event) != 'gestionar-productos' &&
            elementoPinchado(event) != 'contenedor-desplegable-productos' &&
            elementoPinchado(event) != 'ul-contenedor-desplegable-productos' &&
            elementoPinchado(event) != 'li-listado-productos' &&
            elementoPinchado(event) != 'a-listado-productos' &&
            elementoPinchado(event) != 'li-nuevo-producto' &&
            elementoPinchado(event) != 'a-nuevo-producto' &&
            elementoPinchado(event) != 'gestionar-medicamentos' &&
            elementoPinchado(event) != 'contenedor-desplegable-medicamentos' &&
            elementoPinchado(event) != 'ul-contenedor-desplegable-medicamentos' &&
            elementoPinchado(event) != 'li-listado-medicamentos' &&
            elementoPinchado(event) != 'a-listado-medicamentos' &&
            elementoPinchado(event) != 'li-nuevo-medicamento' &&
            elementoPinchado(event) != 'a-nuevo-medicamento' &&
            elementoPinchado(event) != 'gestionar-pedidos' &&
            elementoPinchado(event) != 'contenedor-desplegable-pedidos' &&
            elementoPinchado(event) != 'ul-contenedor-desplegable-pedidos' &&
            elementoPinchado(event) != 'li-listado-pedidos' &&
            elementoPinchado(event) != 'desplegable-listado-pedidos' &&
            elementoPinchado(event) != 'listado-pedidos' &&
            elementoPinchado(event) != 'ul-listado-pedidos' &&
            elementoPinchado(event) != 'li-listado-pedidos' &&
            elementoPinchado(event) != 'a-listado-pedidos' &&
            elementoPinchado(event) != 'li-graficas-pedidos' &&
            elementoPinchado(event) != 'a-graficas-pedidos' &&
            elementoPinchado(event) != 'li-nuevo-pedido' &&
            elementoPinchado(event) != 'a-nuevo-pedido' &&
            elementoPinchado(event) != 'conjunto-lineas-hamburguesa' &&
            elementoPinchado(event) != 'linea-hamburguesa-verde' &&
            elementoPinchado(event) != 'linea-hamburguesa-blanca' &&
            elementoPinchado(event) != 'boton-hamburguesa-verde' &&
            elementoPinchado(event) != 'boton-hamburguesa-blanco' &&
            elementoPinchado(event) != 'contenedor-triangulo-cambiar-clave' &&
            elementoPinchado(event) != 'triangulo-cambiar-clave' &&
            elementoPinchado(event) != 'contenedor-formulario-cambiar-clave' &&
            elementoPinchado(event) != 'formularioCambiarClave' &&
            elementoPinchado(event) != 'desplegable-cambiar-clave' &&
            elementoPinchado(event) != 'contenedor-formCambClave' &&
            elementoPinchado(event) != 'form-cambiar-clave' &&
            elementoPinchado(event) != 'div-form-cambiar-clave' &&
            elementoPinchado(event) != 'label-form-cambiar-clave' &&
            elementoPinchado(event) != 'input-form-cambiar-clave')
            ) {
        return true;
    }
}

function elementoPinchado(event) {
    if (event.srcElement) {
        const claseElementoPinchado = event.srcElement.className;
        return claseElementoPinchado;
    } else if (event.target) {
        const claseElementoPinchado = event.target.className;
        return claseElementoPinchado;
    }
}

function menuAbierto(nMenuNav, nCambiarClave) {

    if (nMenuNav.classList[0] === 'contenedor-menu-nav-movil') {
        if (document.getElementById('tDivBotonHamburguesa').classList[0] === 'boton-hamburguesa-blanco') {
            document.getElementById('tDivBotonHamburguesa').classList.remove('boton-hamburguesa-blanco');
            document.getElementById('tDivBotonHamburguesa').classList.add('boton-hamburguesa-verde');
            document.getElementById('tDivLinea1').classList.remove('linea-hamburguesa-verde');
            document.getElementById('tDivLinea2').classList.remove('linea-hamburguesa-verde');
            document.getElementById('tDivLinea3').classList.remove('linea-hamburguesa-verde');
            document.getElementById('tDivLinea1').classList.add('linea-hamburguesa-blanca');
            document.getElementById('tDivLinea2').classList.add('linea-hamburguesa-blanca');
            document.getElementById('tDivLinea3').classList.add('linea-hamburguesa-blanca');
            document.querySelector('section').style.opacity = 1;
            document.querySelector('section').style.zIndex = 0;
            document.querySelector('.contenedor-desplegable-clientes').style.display = 'none';
            document.querySelector('.contenedor-desplegable-productos').style.display = 'none';
            document.querySelector('.contenedor-desplegable-medicamentos').style.display = 'none';
            document.querySelector('.contenedor-desplegable-pedidos').style.display = 'none';
            document.querySelector('.listado-pedidos').style.display = 'none';
        }

        if (document.getElementById('tUlContenedorMenuNav').classList.value === 'contenedor-menu-nav') {
            document.getElementById('tUlContenedorMenuNav').classList.remove('contenedor-menu-nav');
            document.getElementById('tUlContenedorMenuNav').classList.add('contenedor-menu-nav-movil');
        } else {
            document.getElementById('tUlContenedorMenuNav').classList.remove('contenedor-menu-nav-movil');
            document.getElementById('tUlContenedorMenuNav').classList.add('contenedor-menu-nav');
        }

    } else if (nCambiarClave.style.display === 'flex') {
        nCambiarClave.style.display = 'none';
        document.querySelector('.contenedor-formCambClave').style.display = 'none';
        document.querySelector('.desplegable-cambiar-clave').style.borderRadius = '0.7rem 0.7rem 0 0';
        document.querySelector('section').style.opacity = 1;
        document.querySelector('section').style.zIndex = 0;
    }

}

function eliminarLetraETypeNumber() {

    let ids = [];
    let inputs = document.getElementsByTagName('input');
    for (let input of inputs) {
        if (input.id != '' && input.type == 'number') {
            ids.push(input.id);
        }
    }

    for (let id of ids) {
        document.getElementById(id).addEventListener('focus', () => {
            document.getElementById(id).addEventListener('keydown', function (event) {
                if (event.keyCode === 69) {
                    event.preventDefault();
                }
            });
        });
    }
}

function colorearInputEscrituraDespuesError() {

    let ids = [];
    let inputs = document.getElementsByTagName('input');
    for (let input of inputs) {
        if (input.id != '' && input.type != 'date' && input.type != 'radio') {
            ids.push(input.id);
        }
    }

    for (let id of ids) {
        document.getElementById(id).addEventListener('focus', () => {
            document.getElementById(id).addEventListener('keydown', () => {
                if (isNaN(document.getElementById(id).valueAsNumber)) {
                    colorVerdeGeneral(id);
                } else {
                    const teclaPresionada = String.fromCharCode(event.keyCode);
                    if (teclaPresionada == '`' || teclaPresionada == 'a' || teclaPresionada == 'b' ||
                            teclaPresionada == 'c' || teclaPresionada == 'd' || teclaPresionada == 'e' ||
                            teclaPresionada == 'f' || teclaPresionada == 'g' || teclaPresionada == 'h' ||
                            teclaPresionada == 'i' || teclaPresionada == '0' || teclaPresionada == '1' ||
                            teclaPresionada == '2' || teclaPresionada == '3' || teclaPresionada == '4' ||
                            teclaPresionada == '5' || teclaPresionada == '6' || teclaPresionada == '7' ||
                            teclaPresionada == '8' || teclaPresionada == '9') {
                        colorVerdeGeneral(id);
                    } else {
                        if (teclaPresionada == '' || teclaPresionada == '.') {
                            colorVerdeGeneral(id);
                        }
                    }
                }
            });
        });
    }

}

function colorearTextAreaEscrituraDespuesError() {
    let ids = [];
    let textareas = document.getElementsByTagName('textarea');
    for (let textarea of textareas) {
        if (textarea.id != '') {
            ids.push(textarea.id);
        }
    }

    for (let id of ids) {
        document.getElementById(id).addEventListener('focus', () => {
            document.getElementById(id).addEventListener('keydown', () => {
                colorVerdeGeneral(id);
            });
        });
    }
}

function colorVerdeGeneral(id) {
    document.getElementById(id).style.color = 'var(--green-900)';
    document.getElementById(id).style.letterSpacing = '0.2rem';
    document.getElementById(id).style.fontWeight = 'bolder';
}

function redimensionarCambiarClave() {
    if (screen.width >= 1201) {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.width = '100%';
    } else if (screen.width >= 993 && screen.width <= 1200) {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.width = '100%';
    } else if (screen.width >= 769 && screen.width <= 992) {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.width = '30%';
    } else if (screen.width >= 577 && screen.width <= 768) {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.width = '50%';
    } else if (screen.width >= 430 && screen.width <= 576) {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.width = '60%';
    } else if (screen.width < 430) {
        document.querySelector('.contenedor-formulario-cambiar-clave').style.width = '75%';
    }
}

function redimensionarElementosListado() {
    if (screen.width >= 577 && screen.width <= 768) {
        document.getElementById('tDivResultados').style.width = '100%';
        document.getElementById('tDivPdfBuscador').style.width = '90%';
        document.getElementById('tDivPdfBuscador').style.marginLeft = '0';
    } else if (screen.width >= 430 && screen.width <= 576) {
        document.getElementById('tDivResultados').style.width = '90%';
        document.getElementById('tDivPdfBuscador').style.width = '90%';
        document.getElementById('tDivPdfBuscador').style.marginLeft = '5%';
    } else if (screen.width < 430) {
        document.getElementById('tDivResultados').style.width = '100%';
        document.getElementById('tDivPdfBuscador').style.width = '100%';
        document.getElementById('tDivPdfBuscador').style.marginLeft = '0';
    }
}