'use strict';

window.onload = function () {
    ocultarMenus();
    programarEventos();
    programarRadioButtonsMedicamentos();
    eliminarLetraETypeNumber();
    colorearInputEscrituraDespuesError();
    colorearTextAreaEscrituraDespuesError();
    pincharFueraMenuNav();
    redimensionarCambiarClave();
    borderRadiusMenuNavMdLg();
}

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    window.location.href = '../medicamentos/listado_medicamentos.php';
});

window.onresize = function () {
    redimensionarCambiarClave();
    borderRadiusMenuNavMdLg();
    opacidadSection_XL_XXL();
}

cargarScript('../../javascript/codigo_comun.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;

    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }

    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}

function programarRadioButtonsMedicamentos() {
    document.getElementById('tRadMedSoloConReceta').onchange = pulsarConReceta_sinPulsarBotonModificar;
    document.getElementById('tRadMedSoloSinReceta').onchange = pulsarSinReceta_sinPulsarBotonModificar;
    document.getElementById('tRadMedNingunaRestriccion').onchange = pulsarNingunaRestriccion_sinPulsarBotonModificar;
    deshabilitarCampos();
}

function pulsarConReceta_sinPulsarBotonModificar() {
    if (document.getElementById('tRadMedSoloConReceta').checked) {
        document.getElementById('tInpPrecioPVP').disabled = true;
        document.getElementById('tInpPrecioPVP').removeAttribute('placeholder');
        document.getElementById('tInpPrecioPVP').removeAttribute('value');
        document.getElementById('tInpPrecioAbono').disabled = false;
        document.getElementById('tInpPrecioAbono').placeholder = 'Ingrese precio abonado del medicamento';
        document.getElementById('tInpPrecioPVP').style.backgroundColor = 'var(--grey-300)';
        document.getElementById('tInpPrecioAbono').removeAttribute('style');
    }
}

function pulsarSinReceta_sinPulsarBotonModificar() {
    if (document.getElementById('tRadMedSoloSinReceta').checked) {
        document.getElementById('tInpPrecioAbono').disabled = true;
        document.getElementById('tInpPrecioAbono').removeAttribute('placeholder');
        document.getElementById('tInpPrecioAbono').removeAttribute('value');
        document.getElementById('tInpPrecioPVP').disabled = false;
        document.getElementById('tInpPrecioPVP').placeholder = 'Ingrese precio pvp del medicamento';
        document.getElementById('tInpPrecioAbono').style.backgroundColor = 'var(--grey-300)';
        document.getElementById('tInpPrecioPVP').removeAttribute('style');
    }
}

function pulsarNingunaRestriccion_sinPulsarBotonModificar() {
    if (document.getElementById('tRadMedNingunaRestriccion').checked) {
        document.getElementById('tInpPrecioPVP').disabled = false;
        document.getElementById('tInpPrecioPVP').placeholder = 'Ingrese precio pvp del medicamento';
        document.getElementById('tInpPrecioAbono').disabled = false;
        document.getElementById('tInpPrecioAbono').placeholder = 'Ingrese precio abonado del medicamento';
        document.getElementById('tInpPrecioAbono').removeAttribute('value');
        document.getElementById('tInpPrecioPVP').removeAttribute('value');
        document.getElementById('tInpPrecioAbono').removeAttribute('style');
        document.getElementById('tInpPrecioPVP').removeAttribute('style');
    }
}

function deshabilitarCampos() {
    if (document.getElementById('tRadMedSoloConReceta').checked) {
        if (document.getElementById('tInpPrecioPVP').value == '') {
            document.getElementById('tInpPrecioPVP').disabled = true;
            document.getElementById('tInpPrecioPVP').removeAttribute('placeholder');
            document.getElementById('tInpPrecioPVP').removeAttribute('value');
        }
    }

    if (document.getElementById('tRadMedSoloSinReceta').checked) {
        if (document.getElementById('tInpPrecioAbono').value == '') {
            document.getElementById('tInpPrecioAbono').disabled = true;
            document.getElementById('tInpPrecioAbono').removeAttribute('placeholder');
            document.getElementById('tInpPrecioAbono').removeAttribute('value');
        } 
    }
}