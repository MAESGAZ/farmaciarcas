'use strict';
window.onload = function () {
    consulta('');
    document.querySelector('#tInpMedicamentos').addEventListener('keyup', obtenerValor);
    ocultarMenus();
    programarEventos();
    borderRadiusMenuNavMdLg();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    pincharFueraMenuNav();
};

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    window.location.href = '../index/index.php';
});

window.onresize = function () {
    obtenerValor();
    borderRadiusMenuNavMdLg();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    opacidadSection_XL_XXL();
}

cargarScript('../../javascript/codigo_comun.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;

    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }

    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}

function obtenerValor() {
    const valor = document.querySelector('#tInpMedicamentos').value;
    if (valor != "") {
        consulta(valor);
    } else {
        consulta(valor);
    }
}

function consulta(valor) {
    const url = './busqueda_medicamentos.php';
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(valor),
        headers: {
            'Content-Type': 'application/json'
        }
    })
            .then(response => response.json())
            .then(res => {
                mostrarInformacion(res);
            })
            .catch(err => console.log(err));

}

function mostrarInformacion(medicamentos) {
    const nPadre = document.querySelector('#tDivResultados');
    while (nPadre.firstChild) {
        nPadre.removeChild(nPadre.lastChild);
    }
    const cabeceraXXL = ['ID', 'Nombre', 'Descripción', 'Restricciones Medicamento', 'Precio Abono', 'Precio PVP', 'Cantidad', 'Modificar', 'Eliminar'];
    if (screen.width >= 993) {
        const nDiv1 = crearNodo('div', nPadre);
        for (const resultado of cabeceraXXL) {
            const nDiv = crearNodoTexto('div', nDiv1, resultado);
        }
        
        for (const medicamento of medicamentos) {
            const nDiv2 = crearNodo('div', nPadre);
            const nId = crearNodoTexto('div', nDiv2, medicamento.id);
            const nNombre = crearNodoTexto('div', nDiv2, medicamento.nombre);
            const nDescripcion = crearNodoTexto('div', nDiv2, medicamento.descripcion);

            if (medicamento.restricciones_medicamento === null) {
                var restricciones_medicamento = '---';
            } else {
                var restricciones_medicamento = medicamento.restricciones_medicamento;
            }

            const nMedSoloReceta = crearNodoTexto('div', nDiv2, restricciones_medicamento);

            if (medicamento.precio_abono === null) {
                var valor_precioAbono = '---';
            } else {
                var valor_precioAbono = `${medicamento.precio_abono}€`;
            }

            const nPrecioAbono = crearNodoTexto('div', nDiv2, valor_precioAbono);

            if (medicamento.precio_pvp === null) {
                var valor_precioPVP = '---';
            } else {
                var valor_precioPVP = `${medicamento.precio_pvp}€`;
            }

            const nPrecioPVP = crearNodoTexto('div', nDiv2, valor_precioPVP);
            const nCantidad = crearNodoTexto('div', nDiv2, `${medicamento.cantidad}ud/s`);
            const nModificar = crearNodoBotonGrande('div', nDiv2, 'a', `modificar_medicamento.php?id=${medicamento.id}`, 'MODIFICAR');
            const nEliminarForm = crearNodoBotonGrandeEliminarForm('div', nDiv2, 'form', './listado_medicamentos.php', medicamento.id, 'medicamento', 'ELIMINAR');
        }
    } else {
        const cabeceraSM = ['Nombre', 'Descripción', 'Restricciones', 'Precio Abono', 'Precio PVP', 'Cantidad'];
        for (const medicamento of medicamentos) {
            const nDiv1 = crearNodo('div', nPadre); 
            const nDiv2 = crearNodo('div', nDiv1);
            const nDivText1 = crearNodoTexto('div', nDiv2, `Medicamento nº ${medicamento.id}`);
            const nDiv3 = crearNodo('div', nDiv1);
            for (const resultado of cabeceraSM) {
                const nDiv4 = crearNodo('div', nDiv3);
                const nDivText4 = crearNodoTexto('div', nDiv4, resultado);
                switch (resultado) {
                    case cabeceraSM[0]:
                        const nNombre = crearNodoTexto('div', nDiv4, medicamento.nombre);
                        break;
                    case cabeceraSM[1]:
                        const nDescripcion = crearNodoTexto('div', nDiv4, medicamento.descripcion);
                        break;
                    case cabeceraSM[2]:
                        if (medicamento.restricciones_medicamento === null) {
                            var restricciones_medicamento = '---';
                        } else {
                            var restricciones_medicamento = medicamento.restricciones_medicamento;
                        }
                        const nRestricciones = crearNodoTexto('div', nDiv4, restricciones_medicamento);
                        break;
                    case cabeceraSM[3]:
                        if (medicamento.precio_abono === null) {
                            var valor_precioAbono = '---';
                        } else {
                            var valor_precioAbono = `${medicamento.precio_abono}€`;
                        }
                        const nPrecioAbono = crearNodoTexto('div', nDiv4, valor_precioAbono);
                        break;
                    case cabeceraSM[4]:
                        if (medicamento.precio_pvp === null) {
                            var valor_precioPVP = '---';
                        } else {
                            var valor_precioPVP = `${medicamento.precio_pvp}€`;
                        }

                        const nPrecioPVP = crearNodoTexto('div', nDiv4, valor_precioPVP);
                        break;
                    case cabeceraSM[5]:
                        const nCantidad = crearNodoTexto('div', nDiv4, `${medicamento.cantidad}ud/s`);
                        break;
                }

            }

            const nDiv5 = crearNodo('div', nDiv3);
            const nDiv6 = crearNodo('div', nDiv5);
            const nModificar = crearNodoBoton(nDiv6, 'a', `modificar_medicamento.php?id=${medicamento.id}`, 'MODIFICAR');
            const nDiv7 = crearNodo('div', nDiv5);
            const nEliminarForm = crearNodoBotonPequeEliminarForm(nDiv7, 'form', './listado_medicamentos.php', medicamento.id, 'medicamento', 'ELIMINAR');
        }
    }
}

function crearNodoTexto(elemento, padre, texto) {
    const nodo = document.createElement(elemento);
    padre.appendChild(nodo);
    const text = document.createTextNode(texto);
    nodo.appendChild(text);
    return nodo;
}

function crearNodoBoton(padre, elemento, ruta, texto) {
    const nEnlace = document.createElement(elemento);
    nEnlace.setAttribute('href', ruta);
    padre.appendChild(nEnlace);
    const text = document.createTextNode(texto);
    nEnlace.appendChild(text);
    return nEnlace;
}

function crearNodoBotonGrande(elemento1, padre, elemento2, ruta, texto) {
    const nodo = document.createElement(elemento1);
    padre.appendChild(nodo);
    const nEnlace = document.createElement(elemento2);
    nEnlace.setAttribute('href', ruta);
    nodo.appendChild(nEnlace);
    const text = document.createTextNode(texto);
    nEnlace.appendChild(text);
    return nodo;
}

function crearNodoBotonGrandeEliminarForm(elemento1, padre, elemento2, ruta, id, name, texto) {
    const nodo = document.createElement(elemento1);
    padre.appendChild(nodo);
    const nForm = document.createElement(elemento2);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    nodo.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${name}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nInputSubmit = document.createElement('input');
    nInputSubmit.setAttribute('type', 'submit');
    nInputSubmit.setAttribute('name', `eliminar-${name}`);
    nInputSubmit.setAttribute('value', texto);
    nForm.appendChild(nInputSubmit);
    return nodo;
}

function crearNodoBotonPequeEliminarForm(padre, elemento, ruta, id, name, texto) {
    const nForm = document.createElement(elemento);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    padre.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${name}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nInputSubmit = document.createElement('input');
    nInputSubmit.setAttribute('type', 'submit');
    nInputSubmit.setAttribute('name', `eliminar-${name}`);
    nInputSubmit.setAttribute('value', texto);
    nForm.appendChild(nInputSubmit);
    return nForm;
}

function crearNodo(elemento, padre) {
    const nodo = document.createElement(elemento);
    padre.appendChild(nodo);
    return nodo;
}