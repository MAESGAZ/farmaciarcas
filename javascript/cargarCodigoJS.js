'use strict';

window.onload = function () {
    ocultarMenus();
    programarEventos();
    eliminarLetraETypeNumber();
    colorearInputEscrituraDespuesError();
    colorearTextAreaEscrituraDespuesError();
    redimensionarCambiarClave();
    pincharFueraMenuNav();
}

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    const locationActual = location.href.split('/');
    const ultimaParteLocationActual = locationActual[locationActual.length - 1];
    if (ultimaParteLocationActual.includes('index')) {
        history.pushState(null, document.title, location.href);
    } else if (ultimaParteLocationActual.includes('modificar_producto')) {
        window.location.href = '../productos/listado_productos.php';
    } else {
        window.location.href = '../index/index.php';
    }
});

window.onresize = function () {
    redimensionarCambiarClave();
    borderRadiusMenuNavMdLg();
    opacidadSection_XL_XXL();
}

cargarScript('../../javascript/codigo_comun.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;

    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }

    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}