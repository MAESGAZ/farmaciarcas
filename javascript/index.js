'use strict';

window.onload = function () {
    programarEventosIndex();
}

function programarEventosIndex() {
    document.querySelector('.a-clave-olvidada').onclick = limpiarFormularioLogin;
    document.querySelector('.input-crear-nuevo-usuario').onclick = limpiarFormularioLogin;
}

function limpiarFormularioLogin() {
    document.querySelector('.formulario-control-acceso').reset();
}

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    history.pushState(null, document.title, location.href);
});