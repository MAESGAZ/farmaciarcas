'use strict';

window.onload = function () {
    consulta('');
    document.querySelector('#tInpProductos').addEventListener('keyup', obtenerValor);
    ocultarMenus();
    programarEventos();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    borderRadiusMenuNavMdLg();
    pincharFueraMenuNav();
};

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    window.location.href = '../index/index.php';
});

window.onresize = function () {
    obtenerValor();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    borderRadiusMenuNavMdLg();
    opacidadSection_XL_XXL();
}


cargarScript('../../javascript/codigo_comun.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;

    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }

    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}

function obtenerValor() {
    const valor = document.querySelector('#tInpProductos').value;
    if (valor.length != 0) {
        consulta(valor);
    } else {
        consulta(valor);
    }
}

function consulta(valor) {
    const url = './busqueda_productos.php';
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(valor),
        headers: {
            'Content-Type': 'application/json'
        }
    })
            .then(res => res.json())
            .then(res => {
                mostrarInformacion(res);
            })
            .catch(err => console.log(err));
}
;

function mostrarInformacion(productos) {
    const nPadre = document.querySelector('#tDivResultados');
    while (nPadre.firstChild) {
        nPadre.removeChild(nPadre.lastChild);
    }
    const cabeceraXXL = ['ID', 'Nombre', 'Descripción', 'Precio', 'Cantidad', 'Modificar', 'Eliminar'];
    if (screen.width >= 993) {
        const nDiv1 = crearNodo('div', nPadre);
        for (const resultado of cabeceraXXL) {
            const nDiv = crearNodoTexto('div', nDiv1, resultado);
        }
        
        for (const producto of productos) {
            const nDiv2 = crearNodo('div', nPadre);
            const nId = crearNodoTexto('div', nDiv2, producto.id);
            const nNombre = crearNodoTexto('div', nDiv2, producto.nombre);
            const nDescripcion = crearNodoTexto('div', nDiv2, producto.descripcion);
            const nPrecio = crearNodoTexto('div', nDiv2, `${producto.precio}€`);
            const nCantidad = crearNodoTexto('div', nDiv2, `${producto.cantidad}ud/s`);
            const nModificar = crearNodoBotonGrande('div', nDiv2, 'a', `modificar_producto.php?id=${producto.id}`, 'MODIFICAR');
            const nEliminarForm = crearNodoBotonGrandeForm('div', nDiv2, 'form', './listado_productos.php', producto.id, 'producto', 'eliminar-producto', 'ELIMINAR');
        }
    } else {
        const cabeceraSM = ['Nombre', 'Descripción', 'Precio', 'Cantidad'];
        for (const producto of productos) {
            const nDiv1 = crearNodo('div', nPadre); 
            const nDiv2 = crearNodo('div', nDiv1);
            const nDivText1 = crearNodoTexto('div', nDiv2, `Producto nº ${producto.id}`);
            const nDiv3 = crearNodo('div', nDiv1);
            for (const resultado of cabeceraSM) {
                const nDiv4 = crearNodo('div', nDiv3);
                const nDivText4 = crearNodoTexto('div', nDiv4, resultado);

                switch (resultado) {
                    case cabeceraSM[0]:
                        const nNombre = crearNodoTexto('div', nDiv4, producto.nombre);
                        break;
                    case cabeceraSM[1]:
                        const nDescripcion = crearNodoTexto('div', nDiv4, producto.descripcion);
                        break;
                    case cabeceraSM[2]:
                        const nPrecio = crearNodoTexto('div', nDiv4, `${producto.precio}€`);
                        break;
                    case cabeceraSM[3]:
                        const nCantidad = crearNodoTexto('div', nDiv4, `${producto.cantidad}ud/s`);
                        break;
                }
            }

            const nDiv5 = crearNodo('div', nDiv3);
            const nDiv6 = crearNodo('div', nDiv5);
            const nModificar = crearNodoBoton(nDiv6, 'a', `modificar_producto.php?id=${producto.id}`, 'MODIFICAR');
            const nDiv7 = crearNodo('div', nDiv5);
            const nEliminarForm = crearNodoBotonPequeForm(nDiv7, 'form', './listado_productos.php', producto.id, 'producto','eliminar-producto', 'ELIMINAR');
        }
    }
}

function crearNodoTexto(elemento, padre, texto) {
    const nodo = document.createElement(elemento);
    padre.appendChild(nodo);
    const text = document.createTextNode(texto);
    nodo.appendChild(text);
    return nodo;
}

function crearNodoBoton(padre, elemento, ruta, texto) {
    const nEnlace = document.createElement(elemento);
    nEnlace.setAttribute('href', ruta);
    padre.appendChild(nEnlace);
    const text = document.createTextNode(texto);
    nEnlace.appendChild(text);
    return nEnlace;
}

function crearNodoBotonGrande(elemento1, padre, elemento2, ruta, texto) {
    const nodo = document.createElement(elemento1);
    padre.appendChild(nodo);
    const nEnlace = document.createElement(elemento2);
    nEnlace.setAttribute('href', ruta);
    nodo.appendChild(nEnlace);
    const text = document.createTextNode(texto);
    nEnlace.appendChild(text);
    return nodo;
}

function crearNodoBotonPequeForm(padre, elemento, ruta, id, hidden, name, texto) {
    const nForm = document.createElement(elemento);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    padre.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${hidden}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nInputSubmit = document.createElement('input');
    nInputSubmit.setAttribute('type', 'submit');
    nInputSubmit.setAttribute('name', `eliminar-${name}`);
    nInputSubmit.setAttribute('value', texto);
    nForm.appendChild(nInputSubmit);
    return nForm;
}

function crearNodoBotonGrandeForm(elemento1, padre, elemento2, ruta, id, hidden, name, texto) {
    const nodo = document.createElement(elemento1);
    padre.appendChild(nodo);
    const nForm = document.createElement(elemento2);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    nodo.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${hidden}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nInputSubmit = document.createElement('input');
    nInputSubmit.setAttribute('type', 'submit');
    nInputSubmit.setAttribute('name', name);
    nInputSubmit.setAttribute('value', texto);
    nForm.appendChild(nInputSubmit);
    return nodo;
}

function crearNodo(elemento, padre) {
    const nodo = document.createElement(elemento);
    padre.appendChild(nodo);
    return nodo;
}
