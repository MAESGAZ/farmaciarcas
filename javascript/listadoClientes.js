'use strict';
window.onload = function () {
    consulta('');
    document.querySelector('#tInpClientes').addEventListener('keyup', obtenerValor);
    ocultarMenus();
    programarEventos();
    borderRadiusMenuNavMdLg();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    pincharFueraMenuNav();
};

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    window.location.href = '../index/index.php';
});

window.onresize = function () {
    obtenerValor();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    borderRadiusMenuNavMdLg();
    opacidadSection_XL_XXL();
}

cargarScript('../../javascript/codigo_comun.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;

    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }

    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}

function obtenerValor() {
    const valor = document.querySelector('#tInpClientes').value;
    if (valor != "") {
        consulta(valor);
    } else {
        consulta(valor);
    }
}

function consulta(valor) {
    const url = './busqueda_clientes.php';
    fetch(url, {
        method: 'POST',
        body: JSON.stringify(valor),
        headers: {
            'Content-Type': 'application/json'
        }
    })
            .then(res => res.json())
            .then(res => {
                mostrarInformacion(res);
            })
            .catch(err => console.log(err));
}

function mostrarInformacion(clientes) {
    const nPadre = document.querySelector('#tDivResultados');
    while (nPadre.firstChild) {
        nPadre.removeChild(nPadre.lastChild);
    }
    const cabeceraXXL = ['ID', 'Nombre', 'Apellidos', 'Fecha Nacimiento', 'DNI', 'Nº Seg. Social', 'Dirección', 'Email', 'Teléfono', 'Modificar', 'Eliminar'];
    if (screen.width >= 1483) {
        document.querySelector('section').style.padding = '0 1rem';

        const nDiv1 = crearNodo('div', nPadre);
        for (const resultado of cabeceraXXL) {
            const nDiv = crearNodoTexto('div', nDiv1, resultado);
        }
        for (const cliente of clientes) {
            const nDiv2 = crearNodo('div', nPadre);
            const nId = crearNodoTexto('div', nDiv2, cliente.id);
            const nNombre = crearNodoTexto('div', nDiv2, cliente.nombre);
            const nApellidos = crearNodoTexto('div', nDiv2, cliente.apellidos);
            const nFecha = crearNodoTexto('div', nDiv2, validar_fecha_salida(cliente.fecha_nacimiento));
            const nDni = crearNodoTexto('div', nDiv2, cliente.dni);
            const nNumSegSocial = crearNodoTexto('div', nDiv2, cliente.num_afi_ss);
            const nDireccion = crearNodoTexto('div', nDiv2, cliente.direccion);
            const nEmail = crearNodoTexto('div', nDiv2, cliente.email);
            const nTelefono = crearNodoTexto('div', nDiv2, cliente.telefono);
            const nModificar = crearNodoBotonGrande('div', nDiv2, 'a', `modificar_cliente.php?id=${cliente.id}`, 'MODIFICAR');
            const nEliminarForm = crearNodoBotonGrandeEliminarForm('div', nDiv2, 'form', './listado_clientes.php', cliente.id, 'cliente', 'ELIMINAR');
        }

    } else if (screen.width >= 993) {
        document.querySelector('section').style.padding = '0';
        const cabeceraXL = ['ID', 'Nombre', 'Apellidos', 'Fecha Nac.', 'DNI', 'Nº Seg. Social', 'Dirección', 'Email', 'Tel.', 'Modificar', 'Eliminar'];
        const nDiv1 = crearNodo('div', nPadre);
        for (const resultado of cabeceraXL) {
            if (resultado == 'Modificar' || resultado == 'Eliminar') {
                const nDiv = crearNodoTexto('div', nDiv1, '');
            } else {
                const nDiv = crearNodoTexto('div', nDiv1, resultado);
            }
        }
        for (const cliente of clientes) {
            const nDiv2 = crearNodo('div', nPadre);
            const nId = crearNodoTexto('div', nDiv2, cliente.id);
            const nNombre = crearNodoTexto('div', nDiv2, cliente.nombre);
            const nApellidos = crearNodoTexto('div', nDiv2, cliente.apellidos);
            const nFecha = crearNodoTexto('div', nDiv2, validar_fecha_salida(cliente.fecha_nacimiento));
            const nDni = crearNodoTexto('div', nDiv2, cliente.dni);
            const nNumSegSocial = crearNodoTexto('div', nDiv2, cliente.num_afi_ss);
            const nDireccion = crearNodoTexto('div', nDiv2, cliente.direccion);
            const nEmail = crearNodoTexto('div', nDiv2, cliente.email);
            const nTelefono = crearNodoTexto('div', nDiv2, cliente.telefono);
            const nModificar = crearFormButton('form', nDiv2, `modificar_cliente.php?id=${cliente.id}`, 'button', 'img', '../../imagenes/modificar.png', 'Modificar');
            const nEliminar = crearFormButtonEliminar('form', nDiv2, './listado_clientes.php', cliente.id, 'button', 'cliente', 'img', '../../imagenes/eliminar.png', 'Eliminar');
        }
    } else {
        const cabeceraSM = ['Nombre', 'Apellidos', 'Fecha Nacimiento', 'DNI', 'Nº Seg. Social', 'Dirección', 'Email', 'Teléfono'];
        for (const cliente of clientes) {
            const nDiv1 = crearNodo('div', nPadre); 
            const nDiv2 = crearNodo('div', nDiv1);
            const nDivText1 = crearNodoTexto('div', nDiv2, `Cliente nº ${cliente.id}`);
            const nDiv3 = crearNodo('div', nDiv1);
            for (const resultado of cabeceraSM) {
                const nDiv4 = crearNodo('div', nDiv3);
                const nDivText4 = crearNodoTexto('div', nDiv4, resultado);

                switch (resultado) {
                    case cabeceraSM[0]:
                        const nNombre = crearNodoTexto('div', nDiv4, cliente.nombre);
                        break;
                    case cabeceraSM[1]:
                        const nApellidos = crearNodoTexto('div', nDiv4, cliente.apellidos);
                        break;
                    case cabeceraSM[2]:
                        const nFecha = crearNodoTexto('div', nDiv4, validar_fecha_salida(cliente.fecha_nacimiento));
                        break;
                    case cabeceraSM[3]:
                        const nDni = crearNodoTexto('div', nDiv4, cliente.dni);
                        break;
                    case cabeceraSM[4]:
                        const nNumSegSocial = crearNodoTexto('div', nDiv4, cliente.num_afi_ss);
                        break;
                    case cabeceraSM[5]:
                        const nDireccion = crearNodoTexto('div', nDiv4, cliente.direccion);
                        break;
                    case cabeceraSM[6]:
                        const nEmail = crearNodoTexto('div', nDiv4, cliente.email);
                        break;
                    case cabeceraSM[7]:
                        const nTelefono = crearNodoTexto('div', nDiv4, cliente.telefono);
                        break;
                }
            }

            const nDiv5 = crearNodo('div', nDiv3);
            const nDiv6 = crearNodo('div', nDiv5);
            const nModificar = crearNodoBoton(nDiv6, 'a', `modificar_cliente.php?id=${cliente.id}`, 'MODIFICAR');
            const nDiv7 = crearNodo('div', nDiv5);
            const nEliminarForm = crearNodoBotonPequeEliminarForm(nDiv7, 'form', './listado_clientes.php', cliente.id, 'cliente', 'ELIMINAR');
        }
    }
}

function validar_fecha_salida(fecha_entrada) {
    const fecha_separada = fecha_entrada.split("-");
    const anio = fecha_separada[0];
    const mes = fecha_separada[1];
    const dia = fecha_separada[2];

    const fecha_salida = `${dia}/${mes}/${anio}`;

    return fecha_salida;
}

function crearNodoTexto(elemento, padre, texto) {
    const nodo = document.createElement(elemento);
    padre.appendChild(nodo);
    const text = document.createTextNode(texto);
    nodo.appendChild(text);
    return nodo;
}

function crearNodoBoton(padre, elemento, ruta, texto) {
    const nEnlace = document.createElement(elemento);
    nEnlace.setAttribute('href', ruta);
    padre.appendChild(nEnlace);
    const text = document.createTextNode(texto);
    nEnlace.appendChild(text);
    return nEnlace;
}

function crearNodo(elemento, padre) {
    const nodo = document.createElement(elemento);
    padre.appendChild(nodo);
    return nodo;
}

function crearNodoBotonGrande(elemento1, padre, elemento2, ruta, texto) {
    const nodo = document.createElement(elemento1);
    padre.appendChild(nodo);
    const nEnlace = document.createElement(elemento2);
    nEnlace.setAttribute('href', ruta);
    nodo.appendChild(nEnlace);
    const text = document.createTextNode(texto);
    nEnlace.appendChild(text);
    return nodo;
}

function crearNodoBotonGrandeEliminarForm(elemento1, padre, elemento2, ruta, id, name, texto) {
    const nodo = document.createElement(elemento1);
    padre.appendChild(nodo);
    const nForm = document.createElement(elemento2);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    nodo.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${name}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nInputSubmit = document.createElement('input');
    nInputSubmit.setAttribute('type', 'submit');
    nInputSubmit.setAttribute('name', `eliminar-${name}`);
    nInputSubmit.setAttribute('value', texto);
    nForm.appendChild(nInputSubmit);
    return nodo;
}

function crearNodoBotonPequeEliminarForm(padre, elemento, ruta, id, name, texto) {
    const nForm = document.createElement(elemento);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    padre.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${name}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nInputSubmit = document.createElement('input');
    nInputSubmit.setAttribute('type', 'submit');
    nInputSubmit.setAttribute('name', `eliminar-${name}`);
    nInputSubmit.setAttribute('value', texto);
    nForm.appendChild(nInputSubmit);
    return nForm;
}

function crearFormButton(elemento1, padre, ruta, elemento2, elemento3, src, alt) {
    const nForm = document.createElement(elemento1);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    padre.appendChild(nForm);
    const nButton = document.createElement(elemento2);
    nButton.setAttribute('type', 'submit');
    nForm.appendChild(nButton);
    const nImg = document.createElement(elemento3);
    nImg.setAttribute('src', src);
    nImg.setAttribute('alt', alt);
    nButton.appendChild(nImg);
}

function crearFormButtonEliminar(elemento1, padre, ruta, id, elemento2, name, elemento3, src, alt) {
    const nForm = document.createElement(elemento1);
    nForm.setAttribute('action', ruta);
    nForm.setAttribute('method', 'POST');
    padre.appendChild(nForm);
    const nInputHidden = document.createElement('input');
    nInputHidden.setAttribute('type', 'hidden');
    nInputHidden.setAttribute('name', `id_${name}_hidden`);
    nInputHidden.setAttribute('value', id);
    nForm.appendChild(nInputHidden);
    const nButton = document.createElement(elemento2);
    nButton.setAttribute('type', 'submit');
    nButton.setAttribute('name', `eliminar-${name}`);
    nForm.appendChild(nButton);
    const nImg = document.createElement(elemento3);
    nImg.setAttribute('src', src);
    nImg.setAttribute('alt', alt);
    nButton.appendChild(nImg);
}