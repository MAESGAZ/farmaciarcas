'use strict';
window.onload = function () {
    ocultarMenus();
    programarEventos();
    borderRadiusMenuNavMdLg();
    redimensionarCambiarClave();
    redimensionarElementosListado();
    pincharFueraMenuNav();
};

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    window.location.href = '../index/index.php';
});

window.onresize = function () {
    redimensionarCambiarClave();
    redimensionarElementosListado();
    borderRadiusMenuNavMdLg();
    opacidadSection_XL_XXL();
}

cargarScript('../../javascript/menu.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;
    
    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }
    
    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}
