'use strict';
window.onload = function () {
    ocultarMenus();
    programarEventos();
    colorearTypeDate();
    eliminarLetraETypeNumber();
    colorearInputEscrituraDespuesError();
    colorearTextAreaEscrituraDespuesError();
    pincharFueraMenuNav();
    redimensionarCambiarClave();
    borderRadiusMenuNavMdLg();
}

history.pushState(null, document.title, location.href);
window.addEventListener('popstate', function () {
    const locationActual = location.href.split('/');
    const ultimaParteLocationActual = locationActual[locationActual.length - 1]
    if (ultimaParteLocationActual.includes('modificar')) {
        window.location.href = '../clientes/listado_clientes.php';
    } else {
        window.location.href = '../index/index.php';
    }
});

window.onresize = function () {
    redimensionarCambiarClave();
    borderRadiusMenuNavMdLg();
    opacidadSection_XL_XXL();
}

cargarScript('../../javascript/codigo_comun.js');

function cargarScript(ruta) {
    const nHead = document.getElementsByTagName('head')[0];

    const conjuntoScripts = document.getElementsByTagName('script');
    const arrayPorcionesRuta = ruta.split('/');
    const menuJS = arrayPorcionesRuta[3];
    let i = 0;

    for (let script of conjuntoScripts) {
        if (script.src.includes(`${menuJS}`)) {
            i++;
        }
    }

    const nScriptMenuJS = document.getElementsByTagName('script')[i];
    if (nScriptMenuJS.src.includes(`${menuJS}`)) {
        nScriptMenuJS.remove();
    }

    const nTitle = document.getElementsByTagName('title')[0];
    const nScript = document.createElement('script');
    nScript.setAttribute('defer', '');
    nScript.setAttribute('src', ruta);
    nHead.insertBefore(nScript, nTitle);
}

function colorearTypeDate() {

    if (document.getElementById('tInpFechaNac')) {

        document.getElementById('tInpFechaNac').addEventListener('focus', () => {

            document.getElementById('tInpFechaNac').addEventListener('keydown', () => {

                const teclaPresionada = String.fromCharCode(event.keyCode);

                if (teclaPresionada == '`' || teclaPresionada == 'a' || teclaPresionada == 'b' ||
                        teclaPresionada == 'c' || teclaPresionada == 'd' || teclaPresionada == 'e' ||
                        teclaPresionada == 'f' || teclaPresionada == 'g' || teclaPresionada == 'h' ||
                        teclaPresionada == 'i' || teclaPresionada == '0' || teclaPresionada == '1' ||
                        teclaPresionada == '2' || teclaPresionada == '3' || teclaPresionada == '4' ||
                        teclaPresionada == '5' || teclaPresionada == '6' || teclaPresionada == '7' ||
                        teclaPresionada == '8' || teclaPresionada == '9') {
                    colorVerde();
                } else {

                    if (teclaPresionada == '' || teclaPresionada == '.') {
                        colorVerde();
                    }
                }
            });

        });

        document.getElementById('tInpFechaNac').addEventListener('blur', () => {
            if (document.getElementById('tInpFechaNac').value == '') {
                document.getElementById('tInpFechaNac').value = '';
                colorGris();
            } else {
                if (document.getElementById('tInpFechaNac').style.color != 'red') {
                    colorVerde();
                }
            }
        });
    }
}

function colorVerde() {
    document.getElementById('tInpFechaNac').style.color = 'var(--green-900)';
    document.getElementById('tInpFechaNac').style.letterSpacing = '0.2rem';
    document.getElementById('tInpFechaNac').style.fontWeight = 'bolder';
}

function colorGris() {
    document.getElementById('tInpFechaNac').style.color = 'var(--grey-400)';
    document.getElementById('tInpFechaNac').style.letterSpacing = 'normal';
    document.getElementById('tInpFechaNac').style.fontWeight = 'normal';
}