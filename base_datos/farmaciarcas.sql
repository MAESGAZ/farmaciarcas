-- Crear Base Datos --
CREATE DATABASE IF NOT EXISTS `FARMACIARCAS` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
USE `FARMACIARCAS`;

-- Crear Usuario--
CREATE USER 'FARMACIARCAS'@'%' IDENTIFIED BY 'farmaciarcas2020';
GRANT ALL PRIVILEGES ON *.* TO 'FARMACIARCAS'@'%' REQUIRE NONE WITH GRANT OPTION MAX_QUERIES_PER_HOUR 0 MAX_CONNECTIONS_PER_HOUR 0 MAX_UPDATES_PER_HOUR 0 MAX_USER_CONNECTIONS 0;

-- Crear tabla USUARIOS
CREATE TABLE USUARIOS 
(
    id INT(3) PRIMARY KEY,
    usuario VARCHAR(20) UNIQUE,
    clave VARCHAR(200),
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    genero CHAR(1),
    telefono INT(9) UNIQUE,
    email VARCHAR(50) UNIQUE,
    usuario_confirmado VARCHAR(5)
);

-- Crear tabla CLIENTES
CREATE TABLE CLIENTES 
(
    id INT(3) PRIMARY KEY,
    nombre VARCHAR(50),
    apellidos VARCHAR(50),
    fecha_nacimiento DATE,
    dni VARCHAR(9) UNIQUE,
    num_afi_ss BIGINT(12) UNIQUE,
    direccion VARCHAR(100),
    email VARCHAR(100) UNIQUE,
    telefono INT(9) UNIQUE
);

-- Crear tabla PRODUCTOS
CREATE TABLE PRODUCTOS
(
    id INT(3) PRIMARY KEY,
    nombre VARCHAR(100),
    descripcion VARCHAR(500),
    precio DECIMAL(9,2),
    cantidad INT(3)
);

-- Crear tabla MEDICAMENTOS
CREATE TABLE MEDICAMENTOS
(
    id INT(3) PRIMARY KEY,
    nombre VARCHAR(50),
    descripcion VARCHAR(500),
    restricciones_medicamento VARCHAR(19),
    precio_abono DECIMAL(9,2),
    precio_pvp DECIMAL(9,2),
    cantidad INT(3)
);

-- Crear tabla PEDIDOS
CREATE TABLE PEDIDOS
(
    id INT(5) PRIMARY KEY,
    id_cliente INT(3),
    fecha DATE,
    FOREIGN KEY (id_cliente) REFERENCES CLIENTES(id)
);

-- Crear tabla LINEA_PEDIDOS
CREATE TABLE LINEA_PEDIDOS
(
    id INT(8) PRIMARY KEY,
    id_pedido INT(3),
    id_cliente INT(3),
    id_producto INT(3),
    id_medicamento INT(3),
    FOREIGN KEY (id_pedido) REFERENCES PEDIDOS(id),
    FOREIGN KEY (id_cliente) REFERENCES CLIENTES(id),
    FOREIGN KEY (id_producto) REFERENCES PRODUCTOS(id),
    FOREIGN KEY (id_medicamento) REFERENCES MEDICAMENTOS(id)
);

-- Ejemplos de CLIENTES para insertar en la Base de Datos
INSERT INTO `clientes`(`id`, `nombre`, `apellidos`, `fecha_nacimiento`, `dni`, `num_afi_ss`, `direccion`, `email`, `telefono`) VALUES
(1, 'Pedro', 'Sánchez Pérez-Castejón', '1972-02-29', '07656254Z', '111111111111', 'Congreso (Madrid)', 'infopsoe@psoe.es', 611111111),
(2, 'Pablo', 'Iglesias Turrión', '1978-10-17', '56221386W', '222222222222', 'Congreso (Madrid)', 'pablo.iglesias@congreso.es', 622222222),
(3, 'Pablo', 'Casado Blanco', '1981-02-01', '46883755A', '333333333333', 'Congreso (Madrid)', 'pablo.casado@pp.es', 633333333),
(4, 'Santiago', 'Abascal Conde', '1976-04-14', '05542814K', '444444444444', 'Congreso (Madrid)', 'info@voxespana.es', 644444444),
(5, 'Inés', 'Arrimadas García', '1981-07-03', '40572925M', '555555555555', 'Congreso (Madrid)', ' info@ciudadanos-cs.org', 655555555);

-- Ejemplos de PRODUCTOS para insertar en la Base de Datos
INSERT INTO `productos`(`id`, `nombre`, `descripcion`, `precio`, `cantidad`) VALUES
(1, 'SALVELOX - Aqua Resist apósito adhesivo (x16)', 'Salvelox Aqua Resist apósito adhesivo aloe vera son unas tiritas protectoras que potencian las propiedades beneficiosas del aloe vera para proteger y cuidar nuestras heridas cutáneas. Transpirables y repelentes de agua y suciedad, estos apósitos protegen las heridas superficiales de la piel, evitando que se infecten por el contacto con agentes externos.', '1.55', '200'),
(2, 'INDAS - gasa esteril algodón (x25)', 'Alta capacidad de absorción, fundamentada en la extensión del apósito y su composición de algodón 100 % hidrófilo. Limpia la herida sin desprender partículas. Gracias a su apertura no interfiere en la esterilidad del producto.', '1.60', '150'),
(3, 'DELIPLUS - Alcohol etilico 96 grados (250ml)', 'Se trata de un fantástico antiséptico para piel sana que te permitirá desinfectar la piel de manera rápida y segura.', '0.85', '220'),
(4, 'PIZ BUIN - spray piel sensible (2 x 200ml)', 'La loción se ha desarrollado especialmente para la piel sensible al sol, se absorbe rápidamente y proporciona horas de hidratación para calmar la piel sensible al sol.', '17.50', '60'),
(5, 'IA - Gel de Baño Centella Asiática (400ml)', 'Gel de baño de Interapothek con propiedades reafirmantes, contiene agentes acondicionadores y emolientes que aportan a la piel suavidad, hidratación y tersura. Formulado con Centella Asiática que actúa como reafirmante, drenante y regenerador.', '2.90', '100');

-- Ejemplos de MEDICAMENTOS para insertar en la Base de Datos
INSERT INTO `medicamentos` (`id`, `nombre`, `descripcion`, `restricciones_medicamento`, `precio_abono`, `precio_pvp`, `cantidad`) VALUES
(1, 'Aterina 15mg capsulas blandas', 'ATERINA cápsulas contiene sulodexida, que pertenece al grupo de medicamentos denominados\r\nantitrombóticos, que se utilizan para prevenir y tratar los coágulos de sangre (trombos) que pueden\r\nformarse en los vasos sanguíneos.', 'Ninguna restricción', '3.00', '10.00', 2),
(2, 'Navixen Plus 600 mg/12,5 mg', 'NAVIXEN plus es un medicamento que se utiliza para el tratamiento de la tensión arterial elevada. Contiene una asociación de dos principios activos, eprosartán e hidroclorotiazida.', 'CON receta', '4.00', null, 4),
(3, 'Noctamid 2mg comprimidos', 'Noctamid es un medicamento del grupo de los hipnóticos que favorece el sueño: normaliza el tiempo necesario para conciliarlo y su duración total, a la vez que reduce el número de interrupciones del mismo.', 'SIN receta', null, '10.00', 5),
(4, 'Sertralina STADA 100 mg', 'Sertralina STADA contiene sertralina como principio activo. Sertralina pertenece al grupo de medicamentos denominados inhibidores selectivos de la recaptación de serotonina (ISRS); estos medicamentos se usan para tratar la depresión y/o trastornos de ansiedad.', 'SIN receta', null, '40.00', 1),
(5, 'Ezetimiba pensa 10mg comprimidos EFG', 'Ezetimiba Pensa es un medicamento para reducir los niveles elevados de colesterol. Ezetimiba Pensa reduce las concentraciones de colesterol total, colesterol ¿malo¿ (colesterol LDL) y unas sustancias grasas llamadas triglicéridos que circulan en la sangre.', 'Ninguna restricción', '2.00', '10.00', 4);