<?php
//Función para recoger los posibles errores de un nuevo medicamento
function f_errores_nuevo_medicamento($conexionPDO) {
    //Si el campo nombre está vacío...
    if (empty($_POST['nombre_medicamento_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE.';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['nombre_medicamento_nuevo']) && (f_validar_campo_cadena_texto($_POST['nombre_medicamento_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_nombre_medicamento_nuevo = 'nombre_medicamento_nuevo';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_medicamento_nuevo'] . '\"</b> no es reconocido como cadena de texto.';
    }

    //Si el campo descripción está vacío...
    if (empty($_POST['descripcion_medicamento_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo DESCRIPCIÓN.';
    }

    //Si el botón tipo radio 'restricciones_medicamento_nuevo' NO está relleno...
    if (!isset($_POST['restricciones_medicamento_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo RESTRICCIONES';
    }
    //Si el botón tipo radio 'restricciones_medicamento_nuevo' SÍ está relleno...
    else {
        //Si el radio marcado es el de 'CON receta'
        if ($_POST['restricciones_medicamento_nuevo'] == 'CON receta') {
            //Si el campo precio abonado está vacío...
            if (empty($_POST['precio_medicamento_abono_nuevo'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO ABONADO.';
            }

            //Si el campo precio abonado NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_medicamento_abono_nuevo']) && (f_validar_campo_numerico($_POST['precio_medicamento_abono_nuevo'])) == false) {
                //Guardamos el error en la variable
                $error_precio_medicamento_abono_nuevo = 'precio_medicamento_abono_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO ABONADO <b>\"' . $_POST['precio_medicamento_abono_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }
        }
        //Si el radio marcado es el de 'SIN receta'
        if ($_POST['restricciones_medicamento_nuevo'] == 'SIN receta') {
            //Si el campo precio pvp está vacío...
            if (empty($_POST['precio_medicamento_pvp_nuevo'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO PVP.';
            }

            //Si el campo precio pvp NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_medicamento_pvp_nuevo']) && (f_validar_campo_numerico($_POST['precio_medicamento_pvp_nuevo'])) == false) {
                //Guardamos el error en la variable
                $error_precio_medicamento_pvp_nuevo = 'precio_medicamento_pvp_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO PVP <b>\"' . $_POST['precio_medicamento_pvp_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }
        }

        //Si el radio marcado es el de 'Ninguna restricción'
        if ($_POST['restricciones_medicamento_nuevo'] == 'Ninguna restricción') {

            //Si el campo precio abonado está vacío...
            if (empty($_POST['precio_medicamento_abono_nuevo'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO ABONADO.';
            }

            //Si el campo precio abonado NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_medicamento_abono_nuevo']) && (f_validar_campo_numerico($_POST['precio_medicamento_abono_nuevo'])) == false) {
                //Guardamos el error en la variable
                $error_precio_medicamento_abono_nuevo = 'precio_medicamento_abono_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO ABONO <b>\"' . $_POST['precio_medicamento_abono_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }

            //Si el campo precio pvp está vacío...
            if (empty($_POST['precio_medicamento_pvp_nuevo'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO PVP.';
            }

            //Si el campo precio pvp NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_medicamento_pvp_nuevo']) && (f_validar_campo_numerico($_POST['precio_medicamento_pvp_nuevo'])) == false) {
                //Guardamos el error en la variable
                $error_precio_medicamento_pvp_nuevo = 'precio_medicamento_pvp_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO PVP <b>\"' . $_POST['precio_medicamento_pvp_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }
        }
    }

    //Si el campo cantidad está vacío...
    if (empty($_POST['cantidad_medicamento_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo CANTIDAD';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['cantidad_medicamento_nuevo']) && (f_validar_campo_numerico($_POST['cantidad_medicamento_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_cantidad_medicamento_nuevo = 'cantidad_medicamento_nuevo';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La CANTIDAD <b>\"' . $_POST['cantidad_medicamento_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    //Guardamos todas estas variables en un array 'error'
    $error = [
        $error_nombre_medicamento_nuevo,
        $error_precio_medicamento_abono_nuevo,
        $error_precio_medicamento_pvp_nuevo,
        $error_cantidad_medicamento_nuevo
    ];

    //Devolvemos errores
    return array($error, $alertas_errores);
}

//Función sobre las comprobaciones del nuevo medicamento ( encima del DOCTYPE )
function f_comprobaciones_nuevo_medicamento($conexionPDO) {
    //Si NO está vacío el botón 'Crear Nuevo Medicamento'
    if (!empty($_POST['crear-nuevo-medicamento'])) {
        //Si la sesión 'contador_nuevo_medicamento' está vacía
        if (empty($_SESSION['contador_nuevo_medicamento'])) {
            //Realizar todas las comprobaciones de los campos del formulario NUEVO MEDICAMENTO
            //y guardarla en la variables de errores
            list($error, $alertas_errores) = f_errores_nuevo_medicamento($conexionPDO);
            //Si el botón tipo radio NO está relleno
            if (!isset($_POST['restricciones_medicamento_nuevo'])) {
                //Devolvemos los errores
                return array($error, $alertas_errores);
            }
            //Si el botón tipo radio SÍ está relleno
            else {
                //Comprobar si todos los campos nombre, descripción y cantidad NO están vacíos
                if (!empty($_POST['nombre_medicamento_nuevo']) &&
                        !empty($_POST['descripcion_medicamento_nuevo']) &&
                        !empty($_POST['cantidad_medicamento_nuevo'])) {
                    //Si el radio marcado es el de 'CON receta'
                    if ($_POST['restricciones_medicamento_nuevo'] == 'CON receta') {
                        //Si NO está vacío el precio abono
                        if (!empty($_POST['precio_medicamento_abono_nuevo'])) {
                            //Si no hay errores
                            if (!$alertas_errores) {
                                //Recogemos los registros
                                list($registros, $nombre_medicamento_nuevo) = f_insertar_nuevo_medicamento($conexionPDO);
                            }
                        }
                        //Si SÍ está vacío el precio abono
                        else {
                            //Devolvemos los errores
                            return array($error, $alertas_errores);
                        }
                    }
                    //Si el radio marcado es el de 'SIN receta'
                    elseif ($_POST['restricciones_medicamento_nuevo'] == 'SIN receta') {
                        //Si NO está vacío el precio pvp
                        if (!empty($_POST['precio_medicamento_pvp_nuevo'])) {
                            //Si no hay errores
                            if (!$alertas_errores) {
                                //Recogemos los registros
                                list($registros, $nombre_medicamento_nuevo) = f_insertar_nuevo_medicamento($conexionPDO);
                            }
                        }
                        //Si SÍ está vacío el precio pvp
                        else {
                            //Devolvemos los errores
                            return array($error, $alertas_errores);
                        }
                    }
                    //Si el radio marcado es el de 'Ninguna restricción'
                    elseif ($_POST['restricciones_medicamento_nuevo'] == 'Ninguna restricción') {
                        //Si NO está vacío el precio abono y NO está vacío el precio pvp
                        if (!empty($_POST['precio_medicamento_abono_nuevo']) &&
                                //Si NO está vacío el precio abono y NO está vacío el precio pvp
                                !empty($_POST['precio_medicamento_pvp_nuevo'])) {
                            //Si no hay errores
                            if (!$alertas_errores) {
                                //Recogemos los registros
                                list($registros, $nombre_medicamento_nuevo) = f_insertar_nuevo_medicamento($conexionPDO);
                            }
                        }
                        //Si SÍ está vacío el precio abono y SÍ está vacío el precio pvp
                        else {
                            //Devolvemos los errores
                            return array($error, $alertas_errores);
                        }
                    }
                    //Devolvemos los errores, registros y el nombre del medicamento
                    return array($error, $alertas_errores, $registros, $nombre_medicamento_nuevo);
                }
                //Si los campos nombre, descripción y cantidad están vacíos
                else {
                    //Devolvemos los errores
                    return array($error, $alertas_errores);
                }
            }
        }
        //Si la sesión 'contador_nuevo_medicamento' NO está vacía
        else {
            //Borramos la sesión
            unset($_SESSION['contador_nuevo_medicamento']);
            //Redirigimos a la página 'nuevo_medicamento' cuanto se refresque la página
            header('Location: ./listado_medicamentos.php');
        }
    } else {
        //Borramos la sesión
        unset($_SESSION['contador_nuevo_medicamento']);
    }
}

//Función para realizar la operación de INSERTAR un MEDICAMENTO (*INSERT*)
function f_insertar_nuevo_medicamento($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $sql = 'INSERT INTO medicamentos (id, nombre, descripcion, restricciones_medicamento, precio_abono, precio_pvp, cantidad)'
                . ' VALUES (:id, :nombre, :descripcion, :restricciones_medicamento, :precio_abono, :precio_pvp, :cantidad)';
        $registros = $conexionPDO->prepare($sql);
        //2º: poner nombres a las variables
        //Calculamos el código máximo existente en la base de datos y le sumamos uno
        $id_medicamento_nuevo = f_obtener_maxCodigoBD_y_sumamos_uno_PDO('id', 'medicamentos', $conexionPDO);
        $nombre_medicamento_nuevo = $_POST['nombre_medicamento_nuevo'];
        $descripcion_medicamento_nuevo = $_POST['descripcion_medicamento_nuevo'];
        $restricciones_medicamento_nuevo = $_POST['restricciones_medicamento_nuevo'];
        $precio_medicamento_abono_nuevo = $_POST['precio_medicamento_abono_nuevo'];
        $precio_medicamento_pvp_nuevo = $_POST['precio_medicamento_pvp_nuevo'];
        $cantidad_medicamento_nuevo = $_POST['cantidad_medicamento_nuevo'];
        //3º: paso de parámetros
        $registros->bindParam(":id", $id_medicamento_nuevo);
        $registros->bindParam(":nombre", $nombre_medicamento_nuevo);
        $registros->bindParam(":descripcion", $descripcion_medicamento_nuevo);
        $registros->bindParam(":restricciones_medicamento", $restricciones_medicamento_nuevo);
        $registros->bindParam(":precio_abono", $precio_medicamento_abono_nuevo);
        $registros->bindParam(":precio_pvp", $precio_medicamento_pvp_nuevo);
        $registros->bindParam(":cantidad", $cantidad_medicamento_nuevo);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor del REGISTRO de la consulta y el,
        //el NOMBRE del medicamento nuevo
        return array($registros, $nombre_medicamento_nuevo);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función sobre las comprobaciones del nuevo medicamento ( en el SECTION )
function f_section_nuevo_medicamento($error, $alertas_errores, $registros, $nombre_medicamento_nuevo) {
    //Si la sesión 'contador_nuevo_medicamento' está vacía
    if (empty($_SESSION['contador_nuevo_medicamento'])) {
        //Si el botón "Crear Nuevo Medicamento" contiene información...
        if (!empty($_POST['crear-nuevo-medicamento'])) {

            //Si hay errores...
            if ($alertas_errores) {
                //Incluimos el formulario del nuevo medicamento especificando los errores dentro del formulario
                include '../../formularios/formulario_nuevo_medicamento.php';
            } else {
                //Si hay registros...
                if ($registros) {
                    //Mostramos una alerta de éxito de la inserción
                    $alertas_exitos[] = 'MEDICAMENTO <b>\"' . $nombre_medicamento_nuevo . '\"</b> insertado correctamente.';
                    f_mostrar_alerta_exito_nuevoElemento($alertas_exitos, './listado_medicamentos.php');
                    // Inicializamos y damos un valor cualquiera a la sesión 'contador_nuevo_medicamento'
                    $_SESSION['contador_nuevo_medicamento'] = 1000;
                }
                //Si no hay registros...
                else {
                    //Mostramos un mensaje de error de la inserción
                    $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                    f_mostrar_alerta_errores($alertas_errores);
                    //Borramos la sesión
                    unset($_SESSION['contador_nuevo_medicamento']);
                }
                //Incluimos la maquetación del formulario para un nuevo medicamento
                include '../../formularios/formulario_nuevo_medicamento.php';
            }
        }
        // Si aún no se ha pulsado ningún botón... (posición inicial de la página)
        else {
            //Incluimos la maquetación del formulario para un nuevo medicamento
            include '../../formularios/formulario_nuevo_medicamento.php';
        }
    }
}

//Función que contiene la maquetación del section de listado_medicamentos
function f_section_listado_medicamentos($titulo) {
    ?>
    <div class="listado medicamentos">
        <div class="envoltura-titulo-listado">
            <div>
                <label class="titulo-listado tituloListadoSmMdLg">Listado <?php echo $titulo; ?></label>
            </div>
            <div id="tDivPdfBuscador">
                <div>
                    <a class="generarPDF xxl sm" href="./generarPDF_medicamentos.php">
                        <img src="../../imagenes/icono-pdf-verde.png" alt="PDF">
                    </a>
                </div>
                <div>
                    <input class="buscador" type="text" name="busquedaMedicamentos" id="tInpMedicamentos" placeholder="Busca un medicamento:" />
                </div>
            </div>
        </div>
        <div>
            <div id="tDivResultados"></div>
        </div>
    </div>
    <?php
    f_comprobaciones_section_listado_medicamentos();
}

function f_comprobaciones_section_listado_medicamentos() {
    if (!empty($_POST['eliminar-medicamento'])) {
        f_mostrar_alerta_para_eliminarElemento('medicamento', './eliminar_medicamento.php?id=' . $_POST['id_medicamento_hidden']);
    }
}

//Función que contiene las comprobaciones para eliminar un medicamento
function f_comprobaciones_eliminar_medicamento($conexionPDO) {
    //Si no está relleno el $_GET['id'] en la barra de direcciones
    if (!empty($_GET['id'])) {
        //Procedemos al **DELETE** del contacto
        //Recogemos los valores REGISTROS y FILA del nuevo medicamento
        //del array que nos devuelve la función creada para insertar a un medicamento nuevo
        list($fila, $registros) = f_delete_medicamento($conexionPDO);

        //Si hay registros...
        if ($registros) {
            //Devolvemos $registros
            return array($fila, $registros);
        }
    }
}

//Función para realizar la operación de ELIMINAR un MEDICAMENTO (*DELETE*)
function f_delete_medicamento($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //Guardar en una variable la información obtenida de la CONSULTA
        $fila = f_buscar_informacion_medicamento_solicitado($conexionPDO);

        //Preparamos una consulta para realizar el DELETE
        //1º: preparo la consulta
        $registros = $conexionPDO->prepare('DELETE FROM medicamentos WHERE id=' . $_GET['id']);
        //2º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor de la FILA y el REGISTRO de la consulta,
        return array($fila, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para extrar la información necesaria del MEDICAMENTO A ELIMINAR
function f_buscar_informacion_medicamento_solicitado($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //Asignamos el valor para el ID
        //Si no hemos pulsado aún el botón 'Modificar Medicamento'
        if (empty($_POST['modificar-medicamento'])) {
            //El valor del id será el valor del id de la barra de direcciones ($_GET)
            $id = $_GET['id'];
        }
        //Si SÍ hemos pulsado aún el botón 'Modificar Medicamento'
        else {
            //El valor del id será el valor del id hidden de haber pulsado el botón
            $id = $_POST['id_medicamento_hidden'];
        }

        //Realizamos una consulta para poder obtener toda la información del medicamento que queremos eliminar
        //1º: preparo la consulta
        $sql = 'SELECT * FROM medicamentos WHERE id=' . $id;

        $resultado = $conexionPDO->prepare($sql);

        //2º: ejecuto la consulta
        $resultado->execute();
        //Obtenemos el valor para luego mostrar el elemento que queremos mostrar de la tabla MEDICAMENTOS
        $fila = $resultado->fetch();

        //Devolvemos el valor completo de la FILA del MEDICAMENTO solicitado
        return $fila;
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para incluir la resolución para eliminar medicamento en el <section>
function f_section_eliminar_medicamento($fila, $registros) {
    f_section_listado_medicamentos('Medicamentos');
    //Si hay registros...
    if ($registros) {
        //Mostramos una alerta de éxito de la inserción
        $alertas_exitos[] = 'MEDICAMENTO <b>\"' . $fila['nombre'] . '\"</b> eliminado correctamente.';
        f_mostrar_alerta_exito_elimElemento('medicamento', $alertas_exitos, './listado_medicamentos.php');
    }
    //Si no hay registros...
    else {
        //Mostramos un mensaje de error de la eliminación
        $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
        f_mostrar_alerta_errores($alertas_errores);
    }
}

//Función para recoger los posibles errores de un nuevo medicamento
function f_errores_modificar_medicamento($conexionPDO) {

    //Si el campo nombre está vacío...
    if (empty($_POST['nombre_modificar_medicamento'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE.';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['nombre_modificar_medicamento']) && (f_validar_campo_cadena_texto($_POST['nombre_modificar_medicamento'])) == false) {
        //Guardamos el error en la variable
        $error_nombre_modificar_medicamento = 'nombre_modificar_medicamento';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_modificar_medicamento'] . '\"</b> no es reconocido como cadena de texto.';
    }

    //Si el campo descripción está vacío...
    if (empty($_POST['descripcion_modificar_medicamento'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo DESCRIPCIÓN.';
    }

    //Si el botón tipo radio 'restricciones_medicamento_nuevo' NO está relleno...
    if (!isset($_POST['restricciones_medicamento_modificar'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo RESTRICCIONES.';
    }
    //Si el botón tipo radio 'restricciones_medicamento_nuevo' SÍ está relleno...
    else {
        //Si el radio marcado es el de CON receta
        if ($_POST['restricciones_medicamento_modificar'] == 'CON receta') {
            //Si el campo precio abonado está vacío...
            if (empty($_POST['precio_abono_modificar_medicamento'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO ABONADO.';
            }

            //Si el campo precio abonado NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_abono_modificar_medicamento']) && (f_validar_campo_numerico($_POST['precio_abono_modificar_medicamento'])) == false) {
                //Guardamos el error en la variable
                $error_precio_abono_modificar_medicamento = 'precio_abono_modificar_medicamento';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO ABONADO <b>\"' . $_POST['precio_abono_modificar_medicamento'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }
        }
        //Si el radio marcado es el de SIN receta
        if ($_POST['restricciones_medicamento_modificar'] == 'SIN receta') {
            //Si el campo precio pvp está vacío...
            if (empty($_POST['precio_pvp_modificar_medicamento'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO PVP.';
            }

            //Si el campo precio pvp NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_pvp_modificar_medicamento']) && (f_validar_campo_numerico($_POST['precio_pvp_modificar_medicamento'])) == false) {
                //Guardamos el error en la variable
                $error_precio_pvp_modificar_medicamento = 'precio_pvp_modificar_medicamento';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO PVP <b>\"' . $_POST['precio_pvp_modificar_medicamento'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }
        }
        //Si el radio marcado es el de 'Ninguna restricción'
        if ($_POST['restricciones_medicamento_modificar'] == 'Ninguna restricción') {

            //Si el campo precio abonado está vacío...
            if (empty($_POST['precio_abono_modificar_medicamento'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO ABONADO.';
            }

            //Si el campo precio abonado NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_abono_modificar_medicamento']) && (f_validar_campo_numerico($_POST['precio_abono_modificar_medicamento'])) == false) {
                //Guardamos el error en la variable
                $error_precio_abono_modificar_medicamento = 'precio_abono_modificar_medicamento';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO ABONADO <b>\"' . $_POST['precio_abono_modificar_medicamento'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }

            //Si el campo precio pvp está vacío...
            if (empty($_POST['precio_pvp_modificar_medicamento'])) {
                //Guardamos el error en el array
                $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO PVP.';
            }

            //Si el campo precio pvp NO está vacío y NO es un campo numérico
            if (!empty($_POST['precio_pvp_modificar_medicamento']) && (f_validar_campo_numerico($_POST['precio_pvp_modificar_medicamento'])) == false) {
                //Guardamos el error en la variable
                $error_precio_pvp_modificar_medicamento = 'precio_pvp_modificar_medicamento';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El PRECIO PVP <b>\"' . $_POST['precio_pvp_modificar_medicamento'] . '\"</b> no es reconocido como campo numérico válido'
                        . ' (las cifras numéricas han de ser valores positivos).';
            }
        }
    }

    //Si el campo cantidad está vacío...
    if (empty($_POST['cantidad_modificar_medicamento'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo CANTIDAD.';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['cantidad_modificar_medicamento']) && (f_validar_campo_numerico($_POST['cantidad_modificar_medicamento'])) == false) {
        //Guardamos el error en la variable
        $error_cantidad_modificar_medicamento = 'cantidad_modificar_medicamento';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La CANTIDAD <b>\"' . $_POST['cantidad_modificar_medicamento'] . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    //Guardamos todas estas variables en un array 'error'
    $error = [
        $error_nombre_modificar_medicamento,
        $error_precio_abono_modificar_medicamento,
        $error_precio_pvp_modificar_medicamento,
        $error_cantidad_modificar_medicamento
    ];
    //Devolvemos errores
    return array($error, $alertas_errores);
}

//Función sobre las comprobaciones para modificar un medicamento ( encima del DOCTYPE )
function f_comprobaciones_modificar_medicamento($conexionPDO) {
    //Si NO está vacío el botón 'Modificar Medicamento'
    if (!empty($_POST['modificar-medicamento'])) {
        //Recogemos los valores 'error' y 'errores' de la función
        list($error, $alertas_errores) = f_errores_modificar_medicamento($conexionPDO);
        
        //Si NO está pulsado ningun botón radio ( restricciones_medicamento )
        if (!isset($_POST['restricciones_medicamento_modificar'])) {
            //Devolvemos los errores
            return array($error, $alertas_errores);
        }
        //Si SÍ está pulsado algún botón radio ( restricciones_medicamento )
        else {
            //Comprobar si los campos nombre, descripción y cantidad NO están vacíos
            if (!empty($_POST['nombre_modificar_medicamento']) &&
                    !empty($_POST['descripcion_modificar_medicamento']) &&
                    !empty($_POST['cantidad_modificar_medicamento'])) {
                //Si el botón tipo radio marcado es el de 'CON receta'
                if ($_POST['restricciones_medicamento_modificar'] == 'CON receta') {
                    //Si NO está vacío el precio abono
                    if (!empty($_POST['precio_abono_modificar_medicamento'])) {
                        //Si no hay errores
                        if (!$alertas_errores) {
                            //Recogemos los registros 
                            list($nombre_modificar_medicamento, $registros) = f_modificar_medicamento($conexionPDO);

                            //Devolvemos los errores, registros y el nombre del medicamento
                            return array($error, $alertas_errores, $nombre_modificar_medicamento, $registros);
                        }
                        //Si SÍ hay errores
                        else {
                            //Devolvemos los errores
                            return array($error, $alertas_errores);
                        }
                    }
                    //Si está vacío el precio abono
                    else {
                        //Devolvemos los errores
                        return array($error, $alertas_errores);
                    }
                }
                //Si el botón tipo radio marcado es el de 'SIN receta'
                elseif ($_POST['restricciones_medicamento_modificar'] == 'SIN receta') {
                    //Si NO está vacío el precio pvp
                    if (!empty($_POST['precio_pvp_modificar_medicamento'])) {
                        //Si no hay errores
                        if (!$alertas_errores) {
                            //Recogemos los registros 
                            list($nombre_modificar_medicamento, $registros) = f_modificar_medicamento($conexionPDO);

                            //Devolvemos los errores, registros y el nombre del medicamento
                            return array($error, $alertas_errores, $nombre_modificar_medicamento, $registros);
                        }
                        //Si SÍ hay errores
                        else {
                            //Devolvemos los errores
                            return array($error, $alertas_errores);
                        }
                    }
                    //Si está vacío el precio pvp
                    else {
                        //Devolvemos los errores
                        return array($error, $alertas_errores);
                    }
                }
                //Si el botón tipo radio marcado es el de 'Ninguna restricción'
                elseif ($_POST['restricciones_medicamento_modificar'] == 'Ninguna restricción') {
                    //Si NO está vacío el precio abono y NO está vacío el precio pvp
                    if (!empty($_POST['precio_abono_modificar_medicamento']) &&
                            !empty($_POST['precio_pvp_modificar_medicamento'])) {
                        //Si no hay errores
                        if (!$alertas_errores) {
                            //Recogemos los registros
                            list($nombre_modificar_medicamento, $registros) = f_modificar_medicamento($conexionPDO);

                            //Devolvemos los errores, registros y el nombre del medicamento
                            return array($error, $alertas_errores, $nombre_modificar_medicamento, $registros);
                        }
                        //Si SÍ hay errores
                        else {
                            //Devolvemos los errores
                            return array($error, $alertas_errores);
                        }
                    }
                    //Si están vacíos el precio abono y el precio pvp
                    else {
                        //Devolvemos los errores
                        return array($error, $alertas_errores);
                    }
                }
            }
            //Si los campos nombre, descripción y cantidad están vacíos
            else {
                //Devolvemos los errores
                return array($error, $alertas_errores);
            }
        }
    }
}

//Función para realizar la operación de MODIFICAR un MEDICAMENTO (*UPDATE*)
function f_modificar_medicamento($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //consulta UPDATE - con PDO y preparadas
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $sql = 'UPDATE medicamentos SET nombre=:nombre, '
                . 'descripcion=:descripcion, restricciones_medicamento=:restricciones_medicamento, '
                . 'precio_abono=:precio_abono, precio_pvp=:precio_pvp, cantidad=:cantidad '
                . 'WHERE id=:id';
        $registros = $conexionPDO->prepare($sql);
        //2º: poner nombres a las variables
        $nombre_modificar_medicamento = $_POST['nombre_modificar_medicamento'];
        $descripcion_modificar_medicamento = $_POST['descripcion_modificar_medicamento'];
        $restricciones_medicamento_modificar = $_POST['restricciones_medicamento_modificar'];
        $precio_abono_modificar_medicamento = $_POST['precio_abono_modificar_medicamento'];
        $precio_pvp_modificar_medicamento = $_POST['precio_pvp_modificar_medicamento'];
        $cantidad_modificar_medicamento = $_POST['cantidad_modificar_medicamento'];
        $id_modificar_medicamento = $_POST['id_medicamento_hidden'];
        //3º: paso de parámetros
        $registros->bindParam(":nombre", $nombre_modificar_medicamento);
        $registros->bindParam(":descripcion", $descripcion_modificar_medicamento);
        $registros->bindParam(":restricciones_medicamento", $restricciones_medicamento_modificar);
        $registros->bindParam(":precio_abono", $precio_abono_modificar_medicamento);
        $registros->bindParam(":precio_pvp", $precio_pvp_modificar_medicamento);
        $registros->bindParam(":cantidad", $cantidad_modificar_medicamento);
        $registros->bindParam(":id", $id_modificar_medicamento);
        //4º: ejecuto la consulta
        $registros->execute();
        //Devolvemos en un array el valor del REGISTRO de la consulta y
        //el NOMBRE del medicamento modificado
        return array($nombre_modificar_medicamento, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función sobre las comprobaciones de modificar medicamento ( en el SECTION )
function f_section_modificar_medicamento($error, $alertas_errores, $nombre_modificar_medicamento, $registros, $conexionPDO) {
    //Si NO está vacío el botón 'Modificar Medicamento'
    if (!empty($_POST['modificar-medicamento'])) {
        
        //Si hay errores...
        if ($alertas_errores) {
            //Incluimos el formulario de modificar medicamento especificando los errores dentro del formulario
            include '../../formularios/formulario_modificar_medicamento.php';
        }

        //Si NO está pulsado ningún botón tipo radio ( restricciones_medicamento )
        if (!isset($_POST['restricciones_medicamento_modificar'])) {
            //Incluimos el formulario de modificar medicamento especificando los errores dentro del formulario
            include '../../formularios/formulario_modificar_medicamento.php';
        }
        //Si SÍ está pulsado ningún botón tipo radio ( restricciones_medicamento )
        else {
            //Si no hay errores...
            if (!$alertas_errores) {
                //Comprobar si los campos nombre, descripción y cantidad NO están vacíos
                if (!empty($_POST['nombre_modificar_medicamento']) &&
                        !empty($_POST['descripcion_modificar_medicamento']) &&
                        !empty($_POST['cantidad_modificar_medicamento'])) {
                    //Si el botón tipo radio marcado es el de 'CON receta'
                    if ($_POST['restricciones_medicamento_modificar'] == 'CON receta') {
                        //Si NO está vacío el precio abono
                        if (!empty($_POST['precio_abono_modificar_medicamento'])) {
                            //Si hay registros...
                            if ($registros) {
                                //Mostramos una alerta de éxito de la inserción
                                $alertas_exitos[] = 'MEDICAMENTO <b>\"' . $nombre_modificar_medicamento . '\"</b> modificado correctamente.';
                                f_mostrar_alerta_exito_modifElemento('medicamento', $alertas_exitos, './listado_medicamentos.php');
                            }
                            //Si no hay registros...
                            else {
                                //Mostramos un mensaje de error de la inserción
                                $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                                f_mostrar_alerta_errores($alertas_errores);
                            }
                        }
                    }
                    //Si el botón tipo radio marcado es el de 'SIN receta'
                    elseif ($_POST['restricciones_medicamento_modificar'] == 'SIN receta') {
                        //Si NO está vacío el precio pvp
                        if (!empty($_POST['precio_pvp_modificar_medicamento'])) {
                            //Si hay registros...
                            if ($registros) {
                                //Mostramos una alerta de éxito de la inserción
                                $alertas_exitos[] = 'MEDICAMENTO <b>\"' . $nombre_modificar_medicamento . '\"</b> modificado correctamente.';
                                f_mostrar_alerta_exito_modifElemento('medicamento', $alertas_exitos, './listado_medicamentos.php');
                            }
                            //Si no hay registros...
                            else {
                                //Mostramos un mensaje de error de la inserción
                                $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                                f_mostrar_alerta_errores($alertas_errores);
                            }
                        }
                    }
                    //Si el botón tipo radio marcado es el de 'Ninguna restricción'
                    elseif ($_POST['restricciones_medicamento_modificar'] == 'Ninguna restricción') {
                        //Si NO está vacío el precio abono y NO está vacío el precio pvp
                        if (!empty($_POST['precio_abono_modificar_medicamento']) &&
                                !empty($_POST['precio_pvp_modificar_medicamento'])) {
                            //Si hay registros...
                            if ($registros) {
                                //Mostramos una alerta de éxito de la inserción
                                $alertas_exitos[] = 'MEDICAMENTO <b>\"' . $nombre_modificar_medicamento . '\"</b> modificado correctamente.';
                                f_mostrar_alerta_exito_modifElemento('medicamento', $alertas_exitos, './listado_medicamentos.php');
                            }
                            //Si no hay registros...
                            else {
                                //Mostramos un mensaje de error de la inserción
                                $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                                f_mostrar_alerta_errores($alertas_errores);
                            }
                        }
                    }
                }
                //Incluimos la maquetación del formulario para modificar un medicamento
                include '../../formularios/formulario_modificar_medicamento.php';
            }
        }
    }
    // Si aún no se ha pulsado ningún botón... ( posición inicial de la página )
    else {
        //Incluimos la maquetación del formulario para modificar un medicamento
        include '../../formularios/formulario_modificar_medicamento.php';
    }
}
