<?php

//Crear una CLASE para guardar la Conexión a BD
class Base_Datos_Conexion {

    public static $servidor = 'localhost';
    public static $base_datos = 'farmaciarcas';
    public static $usuario = 'FARMACIARCAS';
    public static $clave = 'farmaciarcas2020';

}

//Función para agrupar comprobaciones dentro del <head></head>
function f_comprobacion_head_index() {
    //Si NO está vacía la sesión del usuario
    if (!empty($_SESSION['usuario'])) {
        ?>
        <script defer src="../../javascript/cargarCodigoJS.js"></script>
        <?php

    }
    //Sí está vacía la sesión del usuario
    else {
        ?>
        <script defer src="../../javascript/index.js"></script>
        <?php

    }
}

//Función para ABRIR CONEXIÓN EN PDO
function f_abrir_conexion_PDO() {
    $servidor = Base_Datos_Conexion::$servidor;
    $base_datos = Base_Datos_Conexion::$base_datos;
    $usuario = Base_Datos_Conexion::$usuario;
    $clave = Base_Datos_Conexion::$clave;
    //conexion a la base de datos, con especificacion de opciones UTF8
    $opciones = array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
    $conexion = new PDO("mysql:host=$servidor;dbname=$base_datos", $usuario, $clave, $opciones);

    //errores
    $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    //Devolvemos la conexión
    return $conexion;
}

//Función para validar la existencia de un campo del formulario en la base de datos
function f_validar_existencia_campo_en_bd($boton, $campo_input, $campo_bd, $tabla) {

    //Abrimos la conexión PDO...
    $conexionPDO = f_abrir_conexion_PDO();

    //Si el boton NO está vacío
    if (!empty($_POST[$boton])) {

        //Si el campo tiene el valor 'clave'
        if ($campo_input == 'clave') {
            //Encriptamos la clave del campo de entrada con el método MD5
            $valor_campo = md5($_POST[$campo_input]);
        }
        //Si el campo NO tiene el valor 'clave'
        else {
            //Su valor será el del campo de entrada
            $valor_campo = $_POST[$campo_input];
        }

        //CONTROL DE ERRORES
        try {
            //1º: preparo la consulta
            $sql = 'SELECT ' . $campo_bd . ' FROM ' . $tabla . ' WHERE ' . $campo_bd . ' = "' . $valor_campo . '" ';
            $consulta = $conexionPDO->prepare($sql);
            //2º: ejecuto la consulta
            $consulta->execute();
            //Si la consulta devuelve una fila, usuario y clave son correctos
            if ($fila = $consulta->fetch()) {
                //Devuelve VERDADERO
                return true;
            }
            //Si no... el usuario no pasa la validación
            else {
                //Devuelve FALSO
                return false;
            }
        } catch (PDOException $p) {
            echo "Error " . $p->getMessage() . "<br />";
        }
    }
}

//Función para VALIDAR UN USUARIO y poder entrar en la aplicación
function f_validar_usuario($usuario, $clave) {
    //Abrimos la conexión PDO...
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //Consulta para obtener todos los datos de la BD de los usuarios
        //que coincidan con el usuario y la clave introducidos
        $consulta = $conexionPDO->prepare(
                'SELECT *  '
                . 'FROM usuarios '
                . 'WHERE usuario = :usuario AND clave = :clave AND usuario_confirmado = :usuario_confirmado');
        $usuario_confirmado = 'TRUE';
        $consulta->bindParam(":usuario", $usuario, PDO::PARAM_STR);
        $consulta->bindParam(":clave", $clave, PDO::PARAM_STR);
        $consulta->bindParam(":usuario_confirmado", $usuario_confirmado, PDO::PARAM_STR);
        $consulta->execute();
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }

    //Si la consulta devuelve una fila, usuario y clave son correctos
    if ($fila = $consulta->fetch()) {
        //Devuelve VERDADERO
        return true;
    }
    //Si no... el usuario no pasa la validación
    else {
        //Devuelve FALSO
        return false;
    }
}

function f_validar_usuario_confirmado($usuario, $clave) {
    //Abrimos la conexión PDO...
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //Consulta para obtener todos los datos de la BD de los usuarios
        //que coincidan con el usuario y la clave introducidos
        $consulta = $conexionPDO->prepare(
                'SELECT usuario_confirmado  '
                . 'FROM usuarios '
                . 'WHERE usuario = :usuario AND clave = :clave');
        $consulta->bindParam(":usuario", $usuario, PDO::PARAM_STR);
        $consulta->bindParam(":clave", $clave, PDO::PARAM_STR);
        $consulta->execute();
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }

    //Si la consulta devuelve una fila, usuario y clave son correctos
    if ($fila = $consulta->fetch()) {
        //Devuelve VERDADERO
        return $fila['usuario_confirmado'];
    }
}

//Función para las comprobaciones del login
function f_comprobaciones_login() {

    //Si hemos pulsado en ENTRAR la sesión del usuario está vacía...
    if ((!empty($_POST['entrar'])) && (empty($_SESSION['usuario']))) {
        //Si el campo USUARIO está vacío y el campo CLAVE está vacío
        if (empty($_POST['usuario']) && empty($_POST['clave'])) {
            //Guardamos los errores en el array de alertas
            $alertas_errores[] = 'No se puede dejar vacío el campo USUARIO.';
            $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE.';
        }
        //Si el campo USUARIO está relleno y el campo CLAVE está vacío
        elseif (!empty($_POST['usuario']) && empty($_POST['clave'])) {
            //Comprobamos que el CAMPO USUARIO no existe en la BD
            if (f_validar_existencia_campo_en_bd('entrar', 'usuario', 'usuario', 'usuarios') == false) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El USUARIO <b>\"' . $_POST['usuario'] . '\"</b> es incorrecto.';
                $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE.';
            }
            //Si el campo usuario SÍ existe en la BD
            else {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE.';
            }
        }
        //Si el campo USUARIO está vacío y el campo CLAVE está relleno
        elseif (empty($_POST['usuario']) && !empty($_POST['clave'])) {
            //Comprobamos que el CAMPO CLAVE no existe en la BD
            if (f_validar_existencia_campo_en_bd('entrar', 'clave', 'clave', 'usuarios') == false) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo USUARIO.';
                $alertas_errores[] = 'La CLAVE es incorrecta.';
                //Borramos el campo de la clave ( para que nunca se muestre en el formulario )
                unset($_POST['clave']);
            }
            //Si la clave SÍ es correcta
            else {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo USUARIO.';
                $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE';
                //Borramos el campo de la clave ( para que nunca se muestre en el formulario )
                unset($_POST['clave']);
            }
        }
        //Si los 2 campos SÍ están rellenos 
        elseif (!empty($_POST['usuario']) && !empty($_POST['clave'])) {
            //Guardamos en una variable el valor de la clave cifrándola con el método de encriptación MD5
            $clave_cifrada = md5($_POST['clave']);
            //Validamos el usuario con la función creada (pasamos por parámetro el usuario y la clave encriptada)
            if (f_validar_usuario($_POST['usuario'], $clave_cifrada) == true) {
                //Creamos una alerta de éxito al entrar en la aplicación
                $_SESSION['usuario'] = $_POST['usuario'];
                //Redirigimos a la página 'index' 
                header('Location: ./index.php');
            }
            //Si no ha sido validado el usuario con la clave...
            else {
                //Si NO existe el usuario en la base de datos
                if (f_validar_existencia_campo_en_bd('entrar', 'usuario', 'usuario', 'usuarios') == false) {
                    //Guardamos los errores en el array de alertas
                    $alertas_errores[] = 'El USUARIO <b>\"' . $_POST['usuario'] . '\"</b> es incorrecto.';
                }
                //Si NO existe la clave en la base de datos
                if (f_validar_existencia_campo_en_bd('entrar', 'clave', 'clave', 'usuarios') == false) {
                    //Guardamos los errores en el array de alertas
                    $alertas_errores[] = 'La CLAVE es incorrecta.';
                    $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE';
                    //Borramos el campo de la clave ( para que nunca se muestre en el formulario )
                    unset($_POST['clave']);
                }

                //Si el usuario NO ha confirmado su alta en FarmaciArcas a través del Email
                if (f_validar_usuario_confirmado($_POST['usuario'], $clave_cifrada) == 'FALSE') {
                    $alertas_errores[] = 'Usuario <b>\"' . $_POST['usuario'] . '\"</b> aún NO ha sido activado como usuario. '
                            . 'Mire la bandeja de su dirección de correo electrónico y confirme su alta en FarmaciArcas.';
                }
            }
        }
    }

    // La función devuelve las alertas y la clave_cifrada
    return array($alertas_errores, $clave_cifrada);
}

//Función que recoge los posibles errores al crear un usuario nuevo
function f_errores_nuevo_usuario($conexionPDO) {

    //Si la Sesión del usuario está vacía...
    if (empty($_SESSION['usuario'])) {

        //Si el Botón Entrar está vacío (no está pulsado aún...)
        if (empty($_POST['crear-nuevo-usuario'])) {
            //Incluimos el formulario del login de usuarios
            include '../../formularios/formulario_nuevo_usuario.php';
        }
        //Si el Botón Entrar NO está vacío (SÍ está pulsado)
        else {
            //Si el campo usuario está vacío...
            if (empty($_POST['usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se pueden dejar vacío el campo USUARIO';
            }

            //Si el campo usuario NO está vacío y NO es un campo de texto
            if (!empty($_POST['usuario_nuevo']) && (f_validar_campo_cadena_texto($_POST['usuario_nuevo'])) == false) {
                //Guardamos el error en una variable
                $error_usuario_nuevo = 'usuario_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El USUARIO <b>\"' . $_POST['usuario_nuevo'] . '\"</b> no es reconocido como cadena de texto.';
            }

            //Si el campo clave está vacío...
            if (empty($_POST['clave_usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE';
            } else {
                //Si el campo clave NO está vacío y NO es un campo de texto
                if (!empty($_POST['clave_usuario_nuevo']) && (f_validar_campo_cadena_texto($_POST['clave_usuario_nuevo'])) == false) {
                    //Guardamos los errores en el array de alertas
                    $alertas_errores[] = 'La CLAVE no es reconocido como cadena de texto.';
                    $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE';
                    unset($_POST['clave_usuario_nuevo']);
                }
            }

            //Si el campo nombre está vacío...
            if (empty($_POST['nombre_usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE.';
            }

            //Si el campo nombre NO está vacío y NO es un campo de texto
            if (!empty($_POST['nombre_usuario_nuevo']) && (f_validar_campo_cadena_texto($_POST['nombre_usuario_nuevo'])) == false) {
                //Guardamos el error en una variable
                $error_nombre_usuario_nuevo = 'nombre_usuario_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_usuario_nuevo'] . '\"</b> no es reconocido como cadena de texto.';
            }

            //Si el campo apellidos está vacío...
            if (empty($_POST['apellidos_usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo APELLIDOS.';
            }

            //Si el campo nombre NO está vacío y NO es un campo de texto
            if (!empty($_POST['apellidos_usuario_nuevo']) && (f_validar_campo_cadena_texto($_POST['apellidos_usuario_nuevo'])) == false) {
                //Guardamos el error en una variable
                $error_apellidos_usuario_nuevo = 'apellidos_usuario_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'Los APELLIDOS <b>\"' . $_POST['apellidos_usuario_nuevo'] . '\"</b> no es reconocido como cadena de texto.';
            }

            //Si el campo género está vacío...
            if (empty($_POST['genero_usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo GÉNERO.';
            }

            //Si el campo teléfono está vacío...
            if (empty($_POST['telefono_usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo TELÉFONO.';
            }

            //Si el campo teléfono NO está vacío...
            if (!empty($_POST['telefono_usuario_nuevo'])) {

                list($error_telefono_usuario_nuevo, $alertas_errores_telefono) = f_validar_telefono($_POST['telefono_usuario_nuevo'], 'telefono_usuario_nuevo');

                //Recorremos el array de alertas de errores
                foreach ($alertas_errores_telefono as $alerta_error_telefono) {
                    //Guardamos cada error de la fecha en el array de alertas
                    $alertas_errores[] = $alerta_error_telefono;
                }
            }

            //Si el campo email está vacío...
            if (empty($_POST['email_usuario_nuevo'])) {
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'No se puede dejar vacío el campo EMAIL.';
            }

            //Si el campo del email contiene información y NO es un email válido (lo comprobamos con la función creada)
            if ((!empty($_POST['email_usuario_nuevo'])) && (f_validar_EMAIL($_POST['email_usuario_nuevo'])) == false) {
                //Guardamos el error en una variable
                $error_email_usuario_nuevo = 'email_usuario_nuevo';
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'El EMAIL <b>\"' . $_POST['email_usuario_nuevo'] . '\"</b> no sigue el formato válido (ej: \"example@domain.com\").';
            }

            //Guardamos todas estas variables en un array 'error'
            $error = [
                $error_usuario_nuevo,
                $error_nombre_usuario_nuevo,
                $error_apellidos_usuario_nuevo,
                $error_telefono_usuario_nuevo,
                $error_email_usuario_nuevo
            ];

            //Devolvemos alertas y error
            return array($alertas_errores, $error);
        }
    }
}

//Función para realizar las comprobaciones para crear un NUEVO USUARIO
function f_comprobaciones_nuevo_usuario($conexionPDO, $mail, $plantilla1) {

    //Si NO está vacío el botón 'Crear Nuevo Usuario'
    if (!empty($_POST['crear-nuevo-usuario'])) {
        //Si la sesión 'contador_nuevo_usuario' está vacía
        if (empty($_SESSION['contador_nuevo_usuario'])) {
            //Obtenemos los posibles errores del formulario
            list($alertas_errores, $error) = f_errores_nuevo_usuario($conexionPDO);

            //Si NO están vacíos todos los campos
            if (!empty($_POST['usuario_nuevo']) &&
                    !empty($_POST['clave_usuario_nuevo']) &&
                    !empty($_POST['nombre_usuario_nuevo']) &&
                    !empty($_POST['apellidos_usuario_nuevo']) &&
                    !empty($_POST['genero_usuario_nuevo']) &&
                    !empty($_POST['telefono_usuario_nuevo']) &&
                    !empty($_POST['email_usuario_nuevo'])) {

                //Si no hay errores
                if (!$alertas_errores) {
                    //Si SÍ existe un usuario con el mismo usuario, teléfono y email
                    if (f_comprobar_usuario_existente($conexionPDO, $_POST['usuario_nuevo'], $_POST['telefono_usuario_nuevo'], $_POST['email_usuario_nuevo']) == true) {
                        //Guardamos los errores en el array de alertas
                        $alertas_errores[] = 'El USUARIO <b>\"' . $_POST['usuario_nuevo'] . '\"</b> ya existe con el mismo teléfono (<b>\"' . $_POST['telefono_usuario_nuevo'] . '\"</b>)'
                                . ' y con el mismo correo electrónico (<b>\"' . $_POST['email_usuario_nuevo'] . '\"</b>).';
                    } else {

                        if (f_buscar_campo_usuario($conexionPDO, 'usuario', $_POST['usuario_nuevo']) == true) {
                            //Guardamos los errores en el array de alertas
                            $alertas_errores[] = 'El USUARIO <b>\"' . $_POST['usuario_nuevo'] . '\"</b> ya existe en la base de datos.';
                        }

                        if (f_buscar_campo_usuario($conexionPDO, 'telefono', $_POST['telefono_usuario_nuevo']) == true) {
                            //Guardamos los errores en el array de alertas
                            $alertas_errores[] = 'El TELÉFONO <b>\"' . $_POST['telefono_usuario_nuevo'] . '\"</b> ya existe en la base de datos.';
                        }

                        if (f_buscar_campo_usuario($conexionPDO, 'email', $_POST['email_usuario_nuevo']) == true) {
                            //Guardamos los errores en el array de alertas
                            $alertas_errores[] = 'El EMAIL <b>\"' . $_POST['email_usuario_nuevo'] . '\"</b> ya existe en la base de datos.';
                        }

                        if (!$alertas_errores) {
                            //Recogemos los valores registros y el usuario tras insertarlo en la BD
                            list($registros, $usuario_nuevo) = f_insertar_nuevo_usuario($conexionPDO, $mail, $plantilla1);
                        }
                    }
                }
            }
            //Devolvemos alertas, error, registros y el usuario nuevo
            return array($alertas_errores, $error, $registros, $usuario_nuevo);
        }
        //Si la sesión 'contador_nuevo_usuario' NO está vacía
        else {
            //Borramos la sesión
            unset($_SESSION['contador_nuevo_usuario']);
            //Redirigimos a la página 'index' cuanto se refresque la página
            header('Location: ../index/index.php');
        }
    }
    //Si todos los campos NO están rellenos
    else {
        //Borramos la sesión
        unset($_SESSION['contador_nuevo_usuario']);
    }
}

//Función de las comprobaciones del nuevo usuario en el SECTION
function f_section_nuevo_usuario($alertas_errores, $error, $registros, $usuario_nuevo, $conexionPDO) {
    //Si la sesión 'contador_nuevo_usuario' está vacía
    if (empty($_SESSION['contador_nuevo_usuario'])) {
        //Si NO está vacío el botón 'Crear Nuevo Usuario'
        if (!empty($_POST['crear-nuevo-usuario'])) {

            //Si hay errores...
            if ($alertas_errores) {
                //Incluimos el formulario del nuevo usuario especificando los errores dentro del formulario
                include '../../formularios/formulario_nuevo_usuario.php';
            } else {
                //Si NO están vacíos todos los campos del formulario
                if (!empty($_POST['usuario_nuevo']) &&
                        !empty($_POST['clave_usuario_nuevo']) &&
                        !empty($_POST['nombre_usuario_nuevo']) &&
                        !empty($_POST['apellidos_usuario_nuevo']) &&
                        !empty($_POST['genero_usuario_nuevo']) &&
                        !empty($_POST['telefono_usuario_nuevo']) &&
                        !empty($_POST['email_usuario_nuevo'])) {
                    //Si hay registros...
                    if ($registros) {
                        //Mostramos una alerta de éxito de la inserción
                        $alertas_exitos[] = 'USUARIO <b>\"' . $usuario_nuevo . '\"</b> insertado correctamente en la Base de Datos de FarmaciArcas.';
                        $alertas_exitos[] = 'Email enviado a su bandeja de entrada para confirmar la activación de su cuenta.';
                        f_mostrar_alerta_exito_nuevoUsuario($alertas_exitos, '../index/index.php');
                        //Inicializamos y damos un valor cualquiera a la sesión 'contador_nuevo_usuario'
                        $_SESSION['contador_nuevo_usuario'] = 1000;
                    }
                    //Si no hay registros...
                    else {
                        //Borramos la sesión
                        unset($_SESSION['contador_nuevo_usuario']);
                        //Mostramos un mensaje de error de la inserción
                        $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                        f_mostrar_alerta_errores($alertas_errores);
                    }
                }
                //Incluimos la maquetación del formulario para un nuevo usuario
                include '../../formularios/formulario_nuevo_usuario.php';
            }
        }
        //Si no está pulsado el botón Crear Nuevo Usuario
        else {
            //Incluimos la maquetación del formulario para un nuevo usuario
            include '../../formularios/formulario_nuevo_usuario.php';
        }
    }
}

//Función para COMPROBAR la existencia en la BD del nombra de usuario introducido por el formulario NUEVO USUARIO
function f_comprobar_usuario_existente($conexionPDO, $usuario, $telefono, $email) {
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $sql = 'SELECT usuario, email, telefono FROM usuarios WHERE usuario = "' . $usuario . '" AND email="' . $email . '" AND telefono=' . $telefono . '';
        $resultado = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve VERDADERO
            return true;
        }
        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve VERDADERO
            return true;
        }
        //Si la búsqueda NO nos devuelve una fila...
        else {
            //Devuelve FALSO
            return false;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_buscar_campo_usuario($conexionPDO, $campo, $post) {
    //CONTROL DE ERRORES
    try {
        if ($campo == 'telefono') {
            //1º: preparo la consulta
            $sql = 'SELECT ' . $campo . ' FROM usuarios WHERE ' . $campo . ' = ' . $post . '';
        } else {
            //1º: preparo la consulta
            $sql = 'SELECT ' . $campo . ' FROM usuarios WHERE ' . $campo . ' = "' . $post . '"';
        }

        $resultado = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve VERDADERO
            return true;
        }
        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve VERDADERO
            return true;
        }
        //Si la búsqueda NO nos devuelve una fila...
        else {
            //Devuelve FALSO
            return false;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para conseguir el último código de la base de datos y sumarle 1 ud
function f_obtener_maxCodigoBD_y_sumamos_uno_PDO($codigo, $tabla, $conexionPDO) {
    //Consulta
    $sql = 'SELECT MAX(' . $codigo . ') FROM ' . $tabla . '';
    //Lanzamos la consulta
    $resultado = $conexionPDO->query($sql);
    //Recogemos el resultado en el array $fila
    $fila = $resultado->fetch();
    //Devolvemos el resultado del código obtenido y le sumamos 1
    return ($fila[0] + 1);
}

//Función para realizar la operación de INSERTAR un USUARIO (*INSERT*)
function f_insertar_nuevo_usuario($conexionPDO, $mail, $plantilla1) {
    //CONTROL DE ERRORES
    try {
        //consulta INSERT - con PDO y preparadas
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $registros = $conexionPDO->prepare('INSERT INTO usuarios (id, usuario, clave, nombre, apellidos, genero, telefono, email, usuario_confirmado)'
                . 'VALUES (:id, :usuario, :clave, :nombre, :apellidos, :genero, :telefono, :email, :usuario_confirmado)');
        //2º: poner nombres a las variables
        //Calculamos el código máximo existente en la base de datos y le sumamos uno
        $id_usuario_nuevo = f_obtener_maxCodigoBD_y_sumamos_uno_PDO('id', 'usuarios', $conexionPDO);
        $usuario_nuevo = $_POST['usuario_nuevo'];
        $clave_usuario_nuevo = md5($_POST['clave_usuario_nuevo']);
        $nombre_usuario_nuevo = $_POST['nombre_usuario_nuevo'];
        $apellidos_usuario_nuevo = $_POST['apellidos_usuario_nuevo'];
        $genero_usuario_nuevo = $_POST['genero_usuario_nuevo'];
        $telefono_usuario_nuevo = $_POST['telefono_usuario_nuevo'];
        $email_usuario_nuevo = $_POST['email_usuario_nuevo'];
        $usuario_confirmado = 'FALSE';
        //3º: paso de parámetros
        $registros->bindParam(':id', $id_usuario_nuevo);
        $registros->bindParam(':usuario', $usuario_nuevo);
        $registros->bindParam(':clave', $clave_usuario_nuevo);
        $registros->bindParam(':nombre', $nombre_usuario_nuevo);
        $registros->bindParam(':apellidos', $apellidos_usuario_nuevo);
        $registros->bindParam(':genero', $genero_usuario_nuevo);
        $registros->bindParam(':telefono', $telefono_usuario_nuevo);
        $registros->bindParam(':email', $email_usuario_nuevo);
        $registros->bindParam(':usuario_confirmado', $usuario_confirmado);
        //4º: ejecuto la consulta
        $registros->execute();
        $plantilla1 = str_replace('[NOMBRE]', $nombre_usuario_nuevo, $plantilla1);
        $plantilla1 = str_replace('[ID]', $id_usuario_nuevo, $plantilla1);
        $plantilla1 = str_replace('[USUARIO]', $usuario_nuevo, $plantilla1);
        $plantilla1 = str_replace('[FECHA]', f_fechaCastellano(date('F-Y')), $plantilla1);

        //Función que envía un email al usuario creado
        f_enviar_email($mail, $plantilla1, $email_usuario_nuevo, $nombre_usuario_nuevo, 'Email para confirmación de registro en FarmaciArcas');
        //Devolvemos en un array el valor del REGISTRO de la consulta y el USUARIO NUEVO
        return array($registros, $usuario_nuevo);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para recuperar info de un usuarioo
function f_buscar_usuario_en_BD($campo, $usuario) {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $resultado = $conexionPDO->prepare('SELECT ' . $campo . ' FROM usuarios WHERE usuario = "' . $usuario . '"');
        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve el género
            return $fila['genero'];
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_buscar_usuario_por_id_usuario($id, $usuario) {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $sql = 'SELECT * FROM usuarios WHERE id = ' . $id . ' AND  usuario = "' . $usuario . '"';
        $resultado = $conexionPDO->prepare($sql);

        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve true
            return true;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_buscar_usuario_confirmado($id, $usuario) {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $sql = 'SELECT usuario_confirmado FROM usuarios WHERE id = ' . $id . ' AND  usuario = "' . $usuario . '"';
        $resultado = $conexionPDO->prepare($sql);

        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve true
            return $fila['usuario_confirmado'];
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_actualizar_usuario_confirmado($true, $id, $usuario) {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //consulta UPDATE - con PDO y preparadas
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $sql = 'UPDATE usuarios SET usuario_confirmado=:usuario_confirmado WHERE id=:id AND usuario=:usuario';
        $registros = $conexionPDO->prepare($sql);
        //2º: poner nombres a las variables
        $usuario_confirmado = $true;
        $id_usuario_existente = $id;
        $usuario_existente = $usuario;
        //3º: paso de parámetros
        $registros->bindParam(":usuario_confirmado", $usuario_confirmado);
        $registros->bindParam(":id", $id_usuario_existente);
        $registros->bindParam(":usuario", $usuario_existente);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos true
        return true;
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_recuperar_datos_usuario($id, $usuario) {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $sql = 'SELECT * FROM usuarios WHERE id = ' . $id . ' AND  usuario = "' . $usuario . '"';
        $resultado = $conexionPDO->prepare($sql);

        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve true
            return $fila;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para recuperar info de un usuario
function f_buscar_clave_usuario_en_BD($campo, $usuario) {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $resultado = $conexionPDO->prepare('SELECT ' . $campo . ' FROM usuarios WHERE usuario = "' . $usuario . '"');
        //2º: ejecuto la consulta
        $resultado->execute();

        //Si la búsqueda SÍ nos devuelve una fila...
        if ($fila = $resultado->fetch()) {
            //Devuelve clave
            return $fila['clave'];
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_section_comprobacion_olvido_clave($mail) {

    // Comprobamos que se le ha dado al botón de enviar o no 
    if (!empty($_POST["envio_email"])) {

        // Comprobamos que $_POST["correo"] no está vacío. 
        if (!empty($_POST["correo"])) {
            // Comprobamos que la variable $_POST["correo"] cumple el patrón del email 
            if (f_validar_EMAIL($_POST["correo"])) {
                $email = $_POST["correo"];

                // Abrimos conexión a la base de datos y hacemos la consulta 
                $conexion = f_abrir_conexion_PDO();
                $consulta = $conexion->prepare("SELECT id, nombre, apellidos, email FROM usuarios WHERE email = '" . $email . "'");
                try {
                    $consulta->execute();
                } catch (PDOException $ex) {
                    echo 'Error ' . $ex->getMessage();
                }
                if ($resultado = $consulta->fetch()) {

                    // Generamos una clave aleatoria y la ciframos. Pasamos los datos de la consulta a la plantilla para el email. 
                    $clave = f_generar_clave_aleatoria();
                    $clave_cifrada = md5($clave);
                    $usuario = $resultado["nombre"] . ' ' . $resultado["apellidos"];
                    $plantilla = file_get_contents('../../complementos/plantilla_email/template.html');
                    $plantilla = str_replace('[USUARIO]', $usuario, $plantilla);
                    $plantilla = str_replace('[CLAVE]', $clave, $plantilla);
                    $plantilla = str_replace('[FECHA]', f_fechaCastellano(date('F-Y')), $plantilla);
                    try {
                        // Realizamos un UPDATE a la base de datos para actualizar la contraseña por la nueva que hemos generado /
                        $consulta2 = $conexion->prepare("UPDATE usuarios SET clave = '" . $clave_cifrada . "' WHERE id = " . $resultado["id"]);
                        $resultado2 = $consulta2->execute();
                    } catch (PDOException $error) {
                        // Alerta si hay un fallo en el UPDATE
                        $alertas_errores[] = 'Error en la consulta UPDATE: ' . $error->getMessage();
                    }
                    if ($resultado2) {
                        $alertas_exitos[] = 'Email enviado para recuperar su clave aleatoria.';
                        // Si ha realizado el UPDATE, envía el email 
                        f_enviar_email($mail, $plantilla, $resultado["email"], $resultado["nombre"], 'Generación de clave aleatoria');
                    } else {
                        // Alerta de que no se ha podido enviar el email 
                        $alertas_errores[] = 'No se ha podido enviar el email.';
                    }
                } else {
                    $error = 'correo';
                    //Guardamos los errores en el array de alertas
                    //Alerta de que el email no existe
                    $alertas_errores[] = 'No existe ningún usuario con ese email';
                }
            } else {
                $error = 'correo';
                // Alerta de si el correo no cumple el patron del email 
                //Guardamos los errores en el array de alertas
                $alertas_errores[] = 'EMAIL no válido (ej: \"example@domain.com\").';
            }
        } else {
            $error = 'correo';
            // Alerta de si la variable $_POST["correo"] está vacía 
            //Guardamos los errores en el array de alertas
            $alertas_errores[] = 'No se puede dejar vacío el campo EMAIL.';
        }

        if ($alertas_errores) {
            f_mostrar_alerta_errores($alertas_errores);
        }

        if ($alertas_exitos) {
            f_mostrar_alerta_exitos($alertas_exitos);
            unset($_POST['correo']);
        }
    }
    include '../../formularios/formulario_olvido_clave.php';
}

function f_errores_cambiar_clave() {
    //Si el campo clave existente está vacío...
    if (empty($_POST['clave_existente_usuario'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo CLAVE EXISTENTE';
    }

    //Si el campo clave existente NO está vacío y NO es un campo de texto
    if (!empty($_POST['clave_existente_usuario']) && (f_validar_campo_cadena_texto($_POST['clave_existente_usuario'])) == false) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'CLAVE EXISTENTE no es reconocida como cadena de texto.';
    }

    //Si el campo nueva clave está vacío...
    if (empty($_POST['nueva_clave_usuario'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo NUEVA CLAVE';
    }

    //Si el campo nueva clave NO está vacío y NO es un campo de texto
    if (!empty($_POST['nueva_clave_usuario']) && (f_validar_campo_cadena_texto($_POST['nueva_clave_usuario'])) == false) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'NUEVA CLAVE no es reconocida como cadena de texto.';
    }

    //Si el campo repita clave está vacío...
    if (empty($_POST['repita_nueva_clave_usuario'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo REPITA CLAVE';
    }

    //Si el campo repita clave NO está vacío y NO es un campo de texto
    if (!empty($_POST['repita_nueva_clave_usuario']) && (f_validar_campo_cadena_texto($_POST['repita_nueva_clave_usuario'])) == false) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'REPITA CLAVE no es reconocida como cadena de texto.';
    }

    return $alertas_errores;
}

function f_cambiar_clave_usuario($mail) {

    if (!empty($_POST['cambiar-clave-usuario'])) {

        $alertas_errores = f_errores_cambiar_clave();

        if (!empty($_POST['clave_existente_usuario']) &&
                !empty($_POST['nueva_clave_usuario']) &&
                !empty($_POST['repita_nueva_clave_usuario'])) {

            if (f_buscar_clave_usuario_en_BD('clave', $_SESSION['usuario']) != md5($_POST['clave_existente_usuario'])) {
                $alertas_errores[] = 'CLAVE EXISTENTE no coincide con el usuario actual';
            } else {
                if (md5($_POST['clave_existente_usuario']) == md5($_POST['nueva_clave_usuario'])) {
                    $alertas_errores[] = 'NUEVA CLAVE igual a CLAVE EXISTENTE';
                } else if (md5($_POST['clave_existente_usuario']) == md5($_POST['repita_nueva_clave_usuario'])) {
                    $alertas_errores[] = 'REPITA CLAVE igual a CLAVE EXISTENTE';
                }

                if (md5($_POST['nueva_clave_usuario']) != md5($_POST['repita_nueva_clave_usuario'])) {
                    $alertas_errores[] = 'NUEVA CLAVE distinta a REPITA CLAVE';
                } else {

                    $registros = f_actualizar_clave_en_bd();

                    if ($registros) {
                        $alertas_exitos[] = 'CLAVE modificada con éxito y actualizada en la Base de Datos de FarmaciArcas.';
                        f_mostrar_alerta_exito_modifClave('clave', $alertas_exitos, './index.php');
                    }
                }
            }
        }

        if ($alertas_errores) {
            f_mostrar_alerta_errores($alertas_errores);
        }
    }
}

function f_actualizar_clave_en_bd() {
    $conexionPDO = f_abrir_conexion_PDO();
    //CONTROL DE ERRORES
    try {
        //consulta INSERT - con PDO y preparadas
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $registros = $conexionPDO->prepare('UPDATE usuarios SET clave = :clave WHERE usuario = :usuario');
        //2º: poner nombres a las variables
        $usuario_existente = $_SESSION['usuario'];
        $nueva_clave_usuario = md5($_POST['nueva_clave_usuario']);
        //3º: paso de parámetros
        $registros->bindParam(':usuario', $usuario_existente);
        $registros->bindParam(':clave', $nueva_clave_usuario);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos registros
        return $registros;
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_enviar_clave_email($mail, $plantilla) {
    // Abrimos conexión a la base de datos y hacemos la consulta 
    $conexionPDO = f_abrir_conexion_PDO();
    $consulta = $conexionPDO->prepare('SELECT * FROM usuarios WHERE usuario ="' . $_SESSION['usuario'] . '"');
    try {
        $consulta->execute();
    } catch (PDOException $ex) {
        echo 'Error ' . $ex->getMessage();
    }
    if ($resultado = $consulta->fetch()) {
        $plantilla = file_get_contents('../../complementos/plantilla_email/template.html');
        $plantilla = str_replace('[USUARIO]', $resultado['usuario'], $plantilla);
        $plantilla = str_replace('[CLAVE]', $_POST['nueva_clave_usuario'], $plantilla);
        $plantilla = str_replace('[FECHA]', f_fechaCastellano(date('F-Y')), $plantilla);

        f_enviar_email($mail, $plantilla, $resultado['email'], $resultado['nombre'], 'Cambio de clave');
    }
}

function f_rellenar_datos_template_new_user($id, $usuario, $plantilla) {

    $fila = f_recuperar_datos_usuario($id, $usuario);

    $plantilla = str_replace('[USUARIO]', $fila['usuario'], $plantilla);
    $plantilla = str_replace('[NOMBRE]', $fila['nombre'], $plantilla);
    $plantilla = str_replace('[APELLIDOS]', $fila['apellidos'], $plantilla);
    $plantilla = str_replace('[TELEFONO]', $fila['telefono'], $plantilla);
    $plantilla = str_replace('[EMAIL]', $fila['email'], $plantilla);
    $plantilla = str_replace('[FECHA]', f_fechaCastellano(date('F-Y')), $plantilla);
    //Si el género es 'M'
    if ($fila['genero'] == 'M') {
        $plantilla = str_replace('[GENERO]', 'Masculino', $plantilla);
    }
    //Si no lo es
    else {
        $plantilla = str_replace('[GENERO]', 'Femenino', $plantilla);
    }

    return array($plantilla, $fila);
}
