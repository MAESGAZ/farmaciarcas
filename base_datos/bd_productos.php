<?php
//Función para recoger los posibles errores de un nuevo producto
function f_errores_nuevo_producto($conexionPDO) {
    //Inicializamos un array para guardar los posibles errores
    $alertas_errores = [];

    //Si el campo nombre está vacío...
    if (empty($_POST['nombre_producto_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['nombre_producto_nuevo']) && (f_validar_campo_cadena_texto($_POST['nombre_producto_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_nombre_producto_nuevo = 'nombre_producto_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_producto_nuevo'] . '\"</b> no es reconocido como cadena de texto.';
    }

    //Si el campo descripción está vacío...
    if (empty($_POST['descripcion_producto_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo DESCRIPCIÓN';
    }

    //Si el campo precio abonado está vacío...
    if (empty($_POST['precio_producto_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO';
    }

    //Si el campo precio abonado NO está vacío y NO es un campo numérico
    if (!empty($_POST['precio_producto_nuevo']) && (f_validar_campo_numerico($_POST['precio_producto_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_precio_producto_nuevo = 'precio_producto_nuevo';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El PRECIO <b>\"' . $_POST['precio_producto_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    //Si el campo cantidad está vacío...
    if (empty($_POST['cantidad_producto_nuevo'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo CANTIDAD';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['cantidad_producto_nuevo']) && (f_validar_campo_numerico($_POST['cantidad_producto_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_cantidad_producto_nuevo = 'cantidad_producto_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La CANTIDAD <b>\"' . $_POST['cantidad_producto_nuevo'] . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    $error = [
        $error_nombre_producto_nuevo,
        $error_precio_producto_nuevo,
        $error_cantidad_producto_nuevo
    ];

    //Devolvemos errores
    return array($error, $alertas_errores);
}

//Función sobre las comprobaciones del nuevo producto ( encima del DOCTYPE )
function f_comprobaciones_nuevo_producto($conexionPDO) {

    //Si se ha pulsado el botón
    if (!empty($_POST['crear-nuevo-producto'])) {
        //Si está vacía la SESIÓN 'contador_nuevo_producto'
        if (empty($_SESSION['contador_nuevo_producto'])) {
            //Realizar todas las comprobaciones de los campos del formulario NUEVO PRODUCTO
            //y guardarla en la variable ERRORES
            list($error, $alertas_errores) = f_errores_nuevo_producto($conexionPDO);

            //Comprobar si todos los campos NO están vacíos y continuamos
            if (!empty($_POST['nombre_producto_nuevo']) &&
                    !empty($_POST['descripcion_producto_nuevo']) &&
                    !empty($_POST['precio_producto_nuevo']) &&
                    !empty($_POST['cantidad_producto_nuevo'])) {

                //Si no hay errores
                if (!$alertas_errores) {
                    //Recogemos los registros
                    list($nombre_producto_nuevo, $registros) = f_insertar_nuevo_producto($conexionPDO);
                }
            }
            return array($nombre_producto_nuevo, $error, $alertas_errores, $registros);
        } else {
            //Borramos la sesión
            unset($_SESSION['contador_nuevo_producto']);
            //Redirigimos a la página 'nuevo_producto' cuanto se refresque la página
            header('Location: ./listado_productos.php');
        }
    } else {
        //Borramos la sesión
        unset($_SESSION['contador_nuevo_producto']);
    }
}

//Función para realizar la operación de INSERTAR un PRODUCTO (*INSERT*)
function f_insertar_nuevo_producto($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $sql = 'INSERT INTO productos (id, nombre, descripcion, precio, cantidad)'
                . ' VALUES (:id, :nombre, :descripcion, :precio, :cantidad)';
        $registros = $conexionPDO->prepare($sql);
        //2º: poner nombres a las variables
        //Calculamos el código máximo existente en la base de datos y le sumamos uno
        $id_producto_nuevo = f_obtener_maxCodigoBD_y_sumamos_uno_PDO('id', 'productos', $conexionPDO);
        $nombre_producto_nuevo = $_POST['nombre_producto_nuevo'];
        $descripcion_producto_nuevo = $_POST['descripcion_producto_nuevo'];
        $precio_producto_nuevo = $_POST['precio_producto_nuevo'];
        $cantidad_producto_nuevo = $_POST['cantidad_producto_nuevo'];
        //3º: paso de parámetros
        $registros->bindParam(":id", $id_producto_nuevo);
        $registros->bindParam(":nombre", $nombre_producto_nuevo);
        $registros->bindParam(":descripcion", $descripcion_producto_nuevo);
        $registros->bindParam(":precio", $precio_producto_nuevo);
        $registros->bindParam(":cantidad", $cantidad_producto_nuevo);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor del REGISTRO de la consulta y el,
        //el NOMBRE del producto nuevo
        return array($nombre_producto_nuevo, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función sobre las comprobaciones del nuevo producto ( en el SECTION )
function f_section_nuevo_producto($nombre_producto_nuevo, $error, $alertas_errores, $registros) {

    //Si está vacía la SESIÓN 'contador_nuevo_producto'
    if (empty($_SESSION['contador_nuevo_producto'])) {

        //Si el botón "Crear Nuevo Producto" contiene información...
        if (!empty($_POST['crear-nuevo-producto'])) {

            //Si hay errores...
            if ($alertas_errores) {
                //Incluimos el formulario del nuevo producto especificando los errores dentro del formulario
                include '../../formularios/formulario_nuevo_producto.php';
            } else {
                //Comprobar si todos los campos NO están vacíos y continuamos
                if (!empty($_POST['nombre_producto_nuevo']) &&
                        !empty($_POST['descripcion_producto_nuevo']) &&
                        !empty($_POST['precio_producto_nuevo']) &&
                        !empty($_POST['cantidad_producto_nuevo'])) {


                    //Si hay registros...
                    if ($registros) {
                        //Inicializamos y damos un valor cualquiera a la sesión 'contador_nuevo_producto'
                        $_SESSION['contador_nuevo_producto'] = 1000;

                        //Mostramos una alerta de éxito de la inserción
                        $alertas_exitos[] = 'PRODUCTO <b>\"' . $nombre_producto_nuevo . '\"</b> insertado correctamente.';
                        f_mostrar_alerta_exito_nuevoElemento($alertas_exitos, './listado_productos.php');
                    }
                    //Si no hay registros...
                    else {
                        //Borramos la sesión
                        unset($_SESSION['contador_nuevo_producto']);
                        //Mostramos un mensaje de error de la inserción
                        $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                        f_mostrar_alerta_errores($alertas_errores);
                    }
                }
                //Incluimos la maquetación del formulario para un nuevo producto
                include '../../formularios/formulario_nuevo_producto.php';
            }
        }
        // Si aún no se ha pulsado ningún botón... (posición inicial de la página)
        else {
            //Incluimos la maquetación del formulario para un nuevo producto
            include '../../formularios/formulario_nuevo_producto.php';
        }
    }
}

//Función que contiene la maquetación del section de listado_productos
function f_section_listado_productos($titulo) {
    ?>
    <div class="listado productos">
        <div class="envoltura-titulo-listado">
            <div>
                <label class="titulo-listado tituloListadoSmMdLg">Listado <?php echo $titulo; ?></label>
            </div>
            <div id="tDivPdfBuscador">
                <div>
                    <a class="generarPDF xxl sm" href="./generarPDF_productos.php">
                        <img src="../../imagenes/icono-pdf-verde.png" alt="PDF">
                    </a>
                </div>
                <div>
                    <input class="buscador" type="text" name="busquedaProductos" id="tInpProductos" placeholder="Buscar:"/>
                </div>
            </div>
        </div>
        <div>
            <div id="tDivResultados"></div>
        </div>
    </div>
    <?php
    f_comprobaciones_section_listado_productos();
}

function f_comprobaciones_section_listado_productos() {
    if (!empty($_POST['eliminar-producto'])) {
        f_mostrar_alerta_para_eliminarElemento('producto', './eliminar_producto.php?id=' . $_POST['id_producto_hidden']);
    }
}

//Función que contiene las comprobaciones para eliminar un producto
function f_comprobaciones_eliminar_producto($conexionPDO) {
    //Si NO está vacío el ID en la barra de driecciones del navegador
    if (!empty($_GET['id'])) {
        //Procedemos al **DELETE** del contacto
        //Recogemos los valores REGISTROS y FILA del nuevo producto
        //del array que nos devuelve la función creada para insertar a un producto nuevo
        list($fila, $registros) = f_delete_producto($conexionPDO);

        //Si hay registros...
        if ($registros) {
            //Devolvemos $registros
            return array($fila, $registros);
        }
    }
}

//Función para realizar la operación de ELIMINAR un PRODUCTO (*DELETE*)
function f_delete_producto($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //Guardar en una variable la información obtenida de la CONSULTA
        $fila = f_buscar_informacion_producto_solicitado($conexionPDO);

        //Preparamos una consulta para realizar el DELETE
        //1º: preparo la consulta
        $registros = $conexionPDO->prepare('DELETE FROM productos WHERE id=' . $_GET['id']);
        //2º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor de la FILA y el REGISTRO de la consulta,
        return array($fila, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para extrar la información necesaria del PRODUCTO A ELIMINAR
function f_buscar_informacion_producto_solicitado($conexionPDO) {
    //CONTROL DE ERRORES
    try {

        //Asignamos el valor para el ID
        //Si no hemos pulsado aún el botón Modificar Producto
        if (empty($_POST['modificar-producto'])) {
            //El valor del id será el valor del id de la barra de direcciones ($_GET)
            $id = $_GET['id'];
        }
        //Si SÍ hemos pulsado aún el botón Modificar Producto
        else {
            //El valor del id será el valor del id hidden de haber pulsado el botón
            $id = $_POST['id_producto_hidden'];
        }

        //Realizamos una consulta para poder obtener toda la información del producto que queremos eliminar
        //1º: preparo la consulta
        $sql = 'SELECT * FROM productos WHERE id=' . $id;
        $resultado = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $resultado->execute();
        //Obtenemos el valor para luego mostrar el elemento que queremos mostrar de la tabla PRODUCTOS
        $fila = $resultado->fetch();

        //Devolvemos el valor completo de la FILA del PRODUCTO solicitado
        return $fila;
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para incluir la resolución para eliminar producto en el <section>
function f_section_eliminar_producto($fila, $registros) {
    f_section_listado_productos('Productos');
    //Si hay registros...
    if ($registros) {
        //Mostramos un mensaje de éxito de al eliminar
        $alertas_exitos[] = 'PRODUCTO <b>\"' . $fila['nombre'] . '\"</b> eliminado correctamente.';
        f_mostrar_alerta_exito_elimElemento('producto', $alertas_exitos, './listado_productos.php');
    }
    //Si no hay registros...
    else {
        //Mostramos un mensaje de error de la eliminación
        $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
        f_mostrar_alerta_errores($alertas_errores);
    }
}

//Función para recoger los posibles errores de un nuevo producto
function f_errores_modificar_producto($conexionPDO) {
    //Inicializamos un array para guardar los posibles errores
    $alertas_errores = [];

    //Si el campo nombre está vacío...
    if (empty($_POST['nombre_modificar_producto'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['nombre_modificar_producto']) && (f_validar_campo_cadena_texto($_POST['nombre_modificar_producto'])) == false) {
        //Guardamos el error en la variable
        $error_nombre_modificar_producto = 'nombre_modificar_producto';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_modificar_producto'] . '\"</b> no es reconocido como cadena de texto.';
    }

    //Si el campo descripción está vacío...
    if (empty($_POST['descripcion_modificar_producto'])) {
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo DESCRIPCIÓN';
    }

    //Si el campo precio abonado está vacío...
    if (empty($_POST['precio_modificar_producto'])) {
        if ($_POST['precio_modificar_producto'] == 0) {
            unset($_POST['precio_modificar_producto']);
        }
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo PRECIO.';
    }

    //Si el campo precio abonado NO está vacío y NO es un campo numérico
    if (!empty($_POST['precio_modificar_producto']) && (f_validar_campo_numerico($_POST['precio_modificar_producto'])) == false) {
        //Guardamos el error en la variable
        $error_precio_modificar_producto = 'precio_modificar_producto';
        //Guardamos el error en el array
        $alertas_errores[] = 'PRECIO no es reconocido como campo numérico.';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El PRECIO <b>\"' . $_POST['precio_modificar_producto'] . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    //Si el campo cantidad está vacío...
    if (empty($_POST['cantidad_modificar_producto'])) {
        if ($_POST['cantidad_modificar_producto'] == 0) {
            unset($_POST['cantidad_modificar_producto']);
        }
        //Guardamos el error en el array
        $alertas_errores[] = 'No se puede dejar vacío el campo CANTIDAD';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['cantidad_modificar_producto']) && (f_validar_campo_numerico($_POST['cantidad_modificar_producto'])) == false) {
        //Guardamos el error en la variable
        $error_cantidad_modificar_producto = 'cantidad_modificar_producto';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La CANTIDAD <b>\"' . $_POST['cantidad_modificar_producto'] . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    $error = [
        $error_nombre_modificar_producto,
        $error_precio_modificar_producto,
        $error_cantidad_modificar_producto
    ];

    //Devolvemos errores
    return array($error, $alertas_errores);
}

//Función sobre las comprobaciones del nuevo producto ( encima del DOCTYPE )
function f_comprobaciones_modificar_producto($conexionPDO) {
    //Si NO está vacío el botón 'Modificar Producto'
    if (!empty($_POST['modificar-producto'])) {
        //Realizar todas las comprobaciones de los campos del formulario NUECO PRODUCTO
        //y guardarla en la variable ERRORES
        list($error, $alertas_errores) = f_errores_modificar_producto($conexionPDO);

        //Comprobar si todos los campos NO están vacíos y continuamos
        if (!empty($_POST['nombre_modificar_producto']) &&
                !empty($_POST['descripcion_modificar_producto']) &&
                !empty($_POST['precio_modificar_producto']) &&
                !empty($_POST['cantidad_modificar_producto'])) {

            //Si existe el array 'error' y no hay errores
            if (!$alertas_errores) {
                //Recogemos los registros
                list($nombre_modificar_producto, $registros) = f_modificar_producto($conexionPDO);
            }

            //Devolvemos los registros y el nombre del producto
            return array($error, $alertas_errores, $nombre_modificar_producto, $registros);
        }
        //Si no están todos los campos rellenos
        else {
            //Devolvemos errores
            return array($error, $alertas_errores);
        }
    }
}

//Función para realizar la operación de MODIFICAR un PRODUCTO (*UPDATE*)
function f_modificar_producto($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //consulta UPDATE - con PDO y preparadas
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $sql = 'UPDATE productos SET nombre=:nombre, '
                . 'descripcion=:descripcion, precio=:precio, cantidad=:cantidad '
                . 'WHERE id=:id';
        $registros = $conexionPDO->prepare($sql);
        //2º: poner nombres a las variables
        $nombre_modificar_producto = $_POST['nombre_modificar_producto'];
        $descripcion_modificar_producto = $_POST['descripcion_modificar_producto'];
        $precio_modificar_producto = $_POST['precio_modificar_producto'];
        $cantidad_modificar_producto = $_POST['cantidad_modificar_producto'];
        $id_modificar_producto = $_POST['id_producto_hidden'];
        //3º: paso de parámetros
        $registros->bindParam(":nombre", $nombre_modificar_producto);
        $registros->bindParam(":descripcion", $descripcion_modificar_producto);
        $registros->bindParam(":precio", $precio_modificar_producto);
        $registros->bindParam(":cantidad", $cantidad_modificar_producto);
        $registros->bindParam(":id", $id_modificar_producto);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor del REGISTRO de la consulta,
        //el NOMBRE del producto modificado
        return array($nombre_modificar_producto, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función sobre las comprobaciones de modificar producto ( en el SECTION )
function f_section_modificar_producto($error, $alertas_errores, $nombre_modificar_producto, $registros, $conexionPDO) {

    //Si NO está vacío el botón
    if (!empty($_POST['modificar-producto'])) {

        //Si hay error o errores...
        if ($alertas_errores) {
            //Incluimos el formulario del nuevo producto especificando los errores dentro del formulario
            include '../../formularios/formulario_modificar_producto.php';
        } else {
            //Comprobar si todos los campos NO están vacíos y continuamos
            if (!empty($_POST['nombre_modificar_producto']) &&
                    !empty($_POST['descripcion_modificar_producto']) &&
                    !empty($_POST['precio_modificar_producto']) &&
                    !empty($_POST['cantidad_modificar_producto'])) {
                //Si hay registros...
                if ($registros) {
                    // Inicializamos y damos un valor cualquiera a la sesión 'contador_nuevo_producto'
                    //Mostramos una alerta de éxito de la inserción
                    $alertas_exitos[] = 'PRODUCTO <b>\"' . $nombre_modificar_producto . '\"</b> modificado correctamente.';
                    f_mostrar_alerta_exito_modifElemento('producto', $alertas_exitos, './listado_productos.php');
                }
                //Si no hay registros...
                else {
                    //Mostramos un mensaje de error de la inserción
                    $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                    f_mostrar_alerta_errores($alertas_errores);
                }
            }
            //Incluimos la maquetación del formulario para un modificar producto
            include '../../formularios/formulario_modificar_producto.php';
        }
    }
    // Si aún no se ha pulsado ningún botón... (posición inicial de la página)
    else {
        //Incluimos la maquetación del formulario para un modificar producto
        include '../../formularios/formulario_modificar_producto.php';
    }
}
