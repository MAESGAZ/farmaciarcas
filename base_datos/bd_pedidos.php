<?php
//Función para las comprobaciones para un nuevo pedido;
function f_comprobaciones_nuevo_pedido($conexionPDO) {
    return array($error, $alertas_errores);
}

//Función que recoge la información del <section> de la página "nuevo_pedido.php"
function f_section_nuevo_pedido($conexionPDO, $error, $alertas_errores) {
    ?>
    <div class="contenedor-nuevo-pedido">
        <div>
            <label class="titulo-listado">&nbsp;Nuevo pedido&nbsp;</label>
        </div>
    </div>
    <div class="contenedor-pagina_construccion">
        <img src="../../imagenes/pagina_construccion.png">
    </div>
    <?php
}

//Función para mostrar el listado de pedidos en "listado_pedidos.php"
function f_section_listado_pedidos($titulo) {
    ?>
    <div class="contenedor-listado-pedidos">
        <div>
            <label class="titulo-listado">&nbsp;Listado <?php echo $titulo; ?>&nbsp;</label>
        </div>
    </div>
    <div class="contenedor-pagina_construccion">
        <img src="../../imagenes/pagina_construccion.png">
    </div>
    <?php
}

//Función para mostrar
function f_section_graficas_pedidos() {
    ?>
    <div class="contenedor-graficas-pedidos">
        <div>
            <label class="titulo-listado">&nbsp;Gráficas de pedidos&nbsp;</label>
        </div>
    </div>
    <div class="contenedor-pagina_construccion">
        <img src="../../imagenes/pagina_construccion.png">
    </div>
    <?php
}
