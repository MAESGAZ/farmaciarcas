<?php

//Función para realizar las comprobaciones de los campos del formulario NUEVO CLIENTE
function f_errores_nuevo_cliente($conexionPDO) {
    //Si el campo nombre está vacío...
    if (empty($_POST['nombre_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['nombre_cliente_nuevo']) && (f_validar_campo_cadena_texto($_POST['nombre_cliente_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_nombre_cliente_nuevo = 'nombre_cliente_nuevo';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_cliente_nuevo'] . '\"</b> no es reconocido como cadena de texto.';
    }

    //Si el campo apellidos está vacío...
    if (empty($_POST['apellidos_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo APELLIDOS.';
    }

    //Si el campo apellidos NO está vacío y NO es un campo de texto
    if (!empty($_POST['apellidos_cliente_nuevo']) && (f_validar_campo_cadena_texto($_POST['apellidos_cliente_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_apellidos_cliente_nuevo = 'apellidos_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'Los APELLIDOS <b>\"' . $_POST['apellidos_cliente_nuevo'] . '\"</b> no son reconocidos como cadena de texto.';
    }

    //Si el campo fecha nacimiento está vacío...
    if (empty($_POST['fecha_nacimiento_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo FECHA.';
    }

    //Si el campo fecha nacimiento contiene información y NO es una fecha de entrada válida (lo comprobamos con la función creada)
    if ((!empty($_POST['fecha_nacimiento_cliente_nuevo'])) && (f_validar_fecha_entrada($_POST['fecha_nacimiento_cliente_nuevo'])) == false) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La FECHA NACIMIENTO <b>\"' . $_POST['fecha_nacimiento_cliente_nuevo'] . '\"</b> no sigue el formato válido (ej: \"dd/mm/aaaa\").';
    }

    //Si el campo fecha nacimiento contiene información
    if (!empty($_POST['fecha_nacimiento_cliente_nuevo'])) {

        list($error_fecha_nacimiento, $alertas_errores_fecha) = f_verificar_fecha_nacimiento('fecha_nacimiento_cliente_nuevo');

        //Recorremos el array de alertas de errores
        foreach ($alertas_errores_fecha as $alerta_error_fecha) {
            //Guardamos cada error de la fecha en el array de alertas
            $alertas_errores[] = $alerta_error_fecha;
        }
    }

    //Si el campo dni está vacío...
    if (empty($_POST['dni_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo DNI.';
    }

    //Si el campo dni contiene información y NO es un dni válido (lo comprobamos con la función creada)
    if ((!empty($_POST['dni_cliente_nuevo'])) && (f_validar_DNI($_POST['dni_cliente_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_dni_cliente_nuevo = 'dni_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El DNI <b>\"' . $_POST['dni_cliente_nuevo'] . '\"</b> no sigue el formato válido <br />(ej: \"00000000A\").';
    }

    //Si el campo dni contiene información y SÍ encontramos ese DNI en la base de datos... ( validar campos únicos )
    if ((!empty($_POST['dni_cliente_nuevo'])) && (f_validar_existencia_campo_en_bd('crear-nuevo-cliente', 'dni_cliente_nuevo', 'dni', 'clientes')) == true) {
        //Guardamos el error en la variable
        $error_dni_cliente_nuevo = 'dni_cliente_nuevo';
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El DNI <b>\"' . $_POST['dni_cliente_nuevo'] . '\"</b> ya existe en la base de datos.';
    }

    //Si el campo Nº Afiliación Seg. social está vacío...
    if (empty($_POST['num_afi_ss_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo Nº SEG. SOCIAL.';
    }

    //Si el campo dni contiene información y NO es un dni válido (lo comprobamos con la función creada)
    if ((!empty($_POST['num_afi_ss_cliente_nuevo'])) && (f_validar_num_ss($_POST['num_afi_ss_cliente_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_num_afi_ss_cliente_nuevo = 'num_afi_ss_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El Nº SEG. SOCIAL <b>\"' . $_POST['num_afi_ss_cliente_nuevo'] . '\"</b> no sigue el formato válido ( longitud 12 dígitos ).';
    }

    //Si el campo dni contiene información y SÍ encontramos ese DNI en la base de datos... ( validar campos únicos )
    if ((!empty($_POST['num_afi_ss_cliente_nuevo'])) && (f_validar_existencia_campo_en_bd('crear-nuevo-cliente', 'num_afi_ss_cliente_nuevo', 'num_afi_ss', 'clientes')) == true) {
        //Guardamos el error en la variable
        $error_num_afi_ss_cliente_nuevo = 'num_afi_ss_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El Nº SEG. SOCIAL <b>\"' . $_POST['num_afi_ss_cliente_nuevo'] . '\"</b> ya existe en la base de datos.';
    }

    //Si el campo dirección está vacío...
    if (empty($_POST['direccion_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo DIRECCIÓN.';
    }

    //Si el campo direccion NO está vacío y NO es un campo de texto
    if (!empty($_POST['direccion_cliente_nuevo']) && (f_validar_campo_cadena_texto($_POST['direccion_cliente_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_direccion_cliente_nuevo = 'direccion_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La DIRECCIÓN <b>\"' . $_POST['direccion_cliente_nuevo'] . '\"</b> no es reconocida como cadena de texto.';
    }

    //Si el campo email está vacío...
    if (empty($_POST['email_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo EMAIL.';
    }

    //Si el campo email contiene información y NO es un email válido (lo comprobamos con la función creada)
    if ((!empty($_POST['email_cliente_nuevo'])) && (f_validar_EMAIL($_POST['email_cliente_nuevo'])) == false) {
        //Guardamos el error en la variable
        $error_email_cliente_nuevo = 'email_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El EMAIL <b>\"' . $_POST['email_cliente_nuevo'] . '\"</b> no sigue el formato válido (ej: \"example@domain.com\").';
    }

    //Si el campo email contiene información y SÍ encontramos ese EMAIL en la base de datos... ( validar campos únicos )
    if ((!empty($_POST['email_cliente_nuevo'])) && (f_validar_existencia_campo_en_bd('crear-nuevo-cliente', 'email_cliente_nuevo', 'email', 'clientes')) == true) {
        //Guardamos el error en la variable
        $error_email_cliente_nuevo = 'email_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El EMAIL <b>\"' . $_POST['email_cliente_nuevo'] . '\"</b> ya existe en la base de datos.';
    }

    //Si el campo teléfono está vacío...
    if (empty($_POST['telefono_cliente_nuevo'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo TELÉFONO.';
    }

    //Si el campo teléfono NO está vacío...
    if (!empty($_POST['telefono_cliente_nuevo'])) {

        list($error_telefono_cliente_nuevo, $alertas_errores_telefono) = f_validar_telefono($_POST['telefono_cliente_nuevo'], 'telefono_cliente_nuevo');

        //Recorremos el array de alertas de errores
        foreach ($alertas_errores_telefono as $alerta_error_telefono) {
            //Guardamos cada error de la fecha en el array de alertas
            $alertas_errores[] = $alerta_error_telefono;
        }
    }

    //Si el campo email contiene información y SÍ encontramos ese DNI en la base de datos... ( validar campos únicos )
    if ((!empty($_POST['telefono_cliente_nuevo'])) && (f_validar_existencia_campo_en_bd('crear-nuevo-cliente', 'telefono_cliente_nuevo', 'telefono', 'clientes')) == true) {
        //Guardamos el error en la variable
        $error_telefono_cliente_nuevo = 'telefono_cliente_nuevo';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El TELÉFONO <b>\"' . $_POST['telefono_cliente_nuevo'] . '\"</b> ya existe en la base de datos.';
    }

    //Guardamos todas estas variables en un array 'error'
    $error = [
        $error_nombre_cliente_nuevo,
        $error_apellidos_cliente_nuevo,
        $error_fecha_nacimiento,
        $error_dni_cliente_nuevo,
        $error_num_afi_ss_cliente_nuevo,
        $error_direccion_cliente_nuevo,
        $error_email_cliente_nuevo,
        $error_telefono_cliente_nuevo
    ];

    //Devolvemos error y alertas
    return array($error, $alertas_errores);
}

function f_comprobaciones_nuevo_cliente($conexionPDO) {

    //Si está pulsado el botón de crear nuevo cliente
    if (!empty($_POST['crear-nuevo-cliente'])) {
        if (empty($_SESSION['contador_nuevo_cliente'])) {
            list($error, $alertas_errores) = f_errores_nuevo_cliente($conexionPDO);

            if (!$alertas_errores) {
                //Si SÍ existe el usuario ( comprobación con los 3 campos únicos: 
                //dni, email, telefono )
                if (f_buscar_exitencia_cliente_por_campos_unicos($conexionPDO,
                                $_POST['dni_cliente_nuevo'],
                                $_POST['num_afi_ss_cliente_nuevo'],
                                $_POST['email_cliente_nuevo'],
                                $_POST['telefono_cliente_nuevo']) == false) {
                    //Recogemos los valores REGISTROS, NOMBRE y APELLIDOS del nuevo cliente
                    //del array que nos devuelve la función creada para insertar a un cliente nuevo
                    //Insertamos el cliente en la Base de Datos
                    list($registros, $nombre_cliente_nuevo, $apellidos_cliente_nuevo) = f_insertar_nuevo_cliente($conexionPDO);
                    return array($error, $alertas_errores, $registros, $nombre_cliente_nuevo, $apellidos_cliente_nuevo);
                }
            } else {
                return array($error, $alertas_errores);
            }
        } else {
            //Borramos la sesión
            unset($_SESSION['contador_nuevo_cliente']);
            //Redirigimos a la página 'nuevo_medicamento' cuanto se refresque la página
            header('Location: ./listado_clientes.php');
        }
    } else {
        //Borramos la sesión
        unset($_SESSION['contador_nuevo_cliente']);
    }
}

//Función con comprobaciones para el <head> de "nuevo_cliente.php"
function f_comprobacion_head_nuevo_cliente() {

    //Funcion para colorear el INPUT del tipo DATE
    f_colorear_input_type_date_nuevo_cliente('crear-nuevo-cliente', 'fecha_nacimiento_cliente_nuevo', $error[2]);
}

//Función para las comprobaciones del nuevo cliente
function f_section_nuevo_cliente($error, $alertas_errores, $registros, $nombre_cliente_nuevo, $apellidos_cliente_nuevo, $conexionPDO) {

    //Si el botón "Crear Nuevo Cliente" contiene información...
    if (!empty($_POST['crear-nuevo-cliente'])) {
        //Si hay errores...
        if ($alertas_errores) {
            //Si todos los campos del formulario NO están vacíos
            if (!empty($_POST['nombre_cliente_nuevo']) &&
                    !empty($_POST['apellidos_cliente_nuevo']) &&
                    !empty($_POST['fecha_nacimiento_cliente_nuevo']) &&
                    !empty($_POST['dni_cliente_nuevo']) &&
                    !empty($_POST['num_afi_ss_cliente_nuevo']) &&
                    !empty($_POST['direccion_cliente_nuevo']) &&
                    !empty($_POST['email_cliente_nuevo']) &&
                    !empty($_POST['telefono_cliente_nuevo'])) {
                //Si SÍ existe el usuario ( comprobación con los 3 campos únicos: 
                //dni, email, telefono )
                if (f_buscar_exitencia_cliente_por_campos_unicos($conexionPDO,
                                $_POST['dni_cliente_nuevo'],
                                $_POST['num_afi_ss_cliente_nuevo'],
                                $_POST['email_cliente_nuevo'],
                                $_POST['telefono_cliente_nuevo']) == true) {
                    //Limpiamos los campos del formulario y los errores
                    unset($_POST['nombre_cliente_nuevo'],
                            $_POST['apellidos_cliente_nuevo'],
                            $_POST['fecha_nacimiento_cliente_nuevo'],
                            $_POST['dni_cliente_nuevo'],
                            $_POST['num_afi_ss_cliente_nuevo'],
                            $_POST['direccion_cliente_nuevo'],
                            $_POST['email_cliente_nuevo'],
                            $_POST['telefono_cliente_nuevo'],
                            $alertas_errores,
                            $error
                    );
                }
            }
            //Incluimos el formulario del nuevo cliente especificando los errores dentro del formulario
            include '../../formularios/formulario_nuevo_cliente.php';
        }
        //Si no hay alertas
        else {
            //Comprobar si todos los campos NO están vacíos y continuamos
            if (!empty($_POST['nombre_cliente_nuevo']) &&
                    !empty($_POST['apellidos_cliente_nuevo']) &&
                    !empty($_POST['fecha_nacimiento_cliente_nuevo']) &&
                    !empty($_POST['dni_cliente_nuevo']) &&
                    !empty($_POST['num_afi_ss_cliente_nuevo']) &&
                    !empty($_POST['direccion_cliente_nuevo']) &&
                    !empty($_POST['email_cliente_nuevo']) &&
                    !empty($_POST['telefono_cliente_nuevo'])) {

                if (empty($_SESSION['contador_nuevo_cliente'])) {
                    //Si hay registros...
                    if ($registros) {
                        //Mostramos un mensaje de éxito de la inserción
                        $alertas_exitos[] = 'CLIENTE <b>\"' . $nombre_cliente_nuevo . ' ' . $apellidos_cliente_nuevo . '\"</b> insertado correctamente.';
                        f_mostrar_alerta_exito_nuevoElemento($alertas_exitos, './listado_clientes.php');
                        $_SESSION['contador_nuevo_cliente'] = 1000;
                    }
                    //Si no hay registros...
                    else {
                        //Mostramos un mensaje de error de la inserción
                        $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
                        f_mostrar_alerta_errores($alertas_errores);
                        unset($_SESSION['contador_nuevo_cliente']);
                    }
                }
            }
            //Incluimos la maquetación del formulario para un nuevo cliente
            include '../../formularios/formulario_nuevo_cliente.php';
        }
    }
    // Si aún no se ha pulsado ningún botón... (posición inicial de la página)
    else {
        //Incluimos la maquetación del formulario para un nuevo cliente
        include '../../formularios/formulario_nuevo_cliente.php';
    }
}

//Función para realizar la operación de INSERTAR un CLIENTE (*INSERT*)
function f_insertar_nuevo_cliente($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $registros = $conexionPDO->prepare('INSERT INTO clientes (id, nombre, apellidos,'
                . ' fecha_nacimiento, dni, num_afi_ss, direccion, email, telefono)'
                . ' VALUES (:id, :nombre, :apellidos, :fecha_nacimiento, :dni,'
                . ' :num_afi_ss, :direccion, :email, :telefono)');
        //2º: poner nombres a las variables
        //Calculamos el código máximo existente en la base de datos y le sumamos uno
        $id_cliente_nuevo = f_obtener_maxCodigoBD_y_sumamos_uno_PDO('id', 'clientes', $conexionPDO);
        $nombre_cliente_nuevo = $_POST['nombre_cliente_nuevo'];
        $apellidos_cliente_nuevo = $_POST['apellidos_cliente_nuevo'];
        $fecha_nacimiento_cliente_nuevo = $_POST['fecha_nacimiento_cliente_nuevo'];
        $dni_cliente_nuevo = $_POST['dni_cliente_nuevo'];
        $num_afi_ss_cliente_nuevo = $_POST['num_afi_ss_cliente_nuevo'];
        $direccion_cliente_nuevo = $_POST['direccion_cliente_nuevo'];
        $email_cliente_nuevo = $_POST['email_cliente_nuevo'];
        $telefono_cliente_nuevo = $_POST['telefono_cliente_nuevo'];
        //3º: paso de parámetros
        $registros->bindParam(":id", $id_cliente_nuevo);
        $registros->bindParam(":nombre", $nombre_cliente_nuevo);
        $registros->bindParam(":apellidos", $apellidos_cliente_nuevo);
        $registros->bindParam(":fecha_nacimiento", $fecha_nacimiento_cliente_nuevo);
        $registros->bindParam(":dni", $dni_cliente_nuevo);
        $registros->bindParam(":num_afi_ss", $num_afi_ss_cliente_nuevo);
        $registros->bindParam(":direccion", $direccion_cliente_nuevo);
        $registros->bindParam(":email", $email_cliente_nuevo);
        $registros->bindParam(":telefono", $telefono_cliente_nuevo);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor del REGISTRO de la consulta,
        //el NOMBRE y los APELLIDOS del cliente nuevo
        return array($registros, $nombre_cliente_nuevo, $apellidos_cliente_nuevo);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_buscar_clientes_en_BD($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //1º: preparo la consulta
        $resultado = $conexionPDO->prepare('SELECT * FROM clientes');
        //2º: ejecuto la consulta
        $resultado->execute();
        //Devolvems el RESULTADO de la consulta
        return $resultado;
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para realizar las comprobaciones de los campos del formulario MODIFICAR CLIENTE
function f_errores_modificar_cliente($conexionPDO) {

    //Si el campo nombre está vacío...
    if (empty($_POST['nombre_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo NOMBRE';
    }

    //Si el campo nombre NO está vacío y NO es un campo de texto
    if (!empty($_POST['nombre_modificar_cliente']) && (f_validar_campo_cadena_texto($_POST['nombre_modificar_cliente'])) == false) {

        //Guardamos el error en la variable
        $error_nombre_modificar_cliente = 'nombre_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El NOMBRE <b>\"' . $_POST['nombre_modificar_cliente'] . '\"</b> no es reconocido como cadena de texto.';
    }

    //Si el campo apellidos está vacío...
    if (empty($_POST['apellidos_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo APELLIDOS.';
    }

    //Si el campo apellidos NO está vacío y NO es un campo de texto
    if (!empty($_POST['apellidos_modificar_cliente']) && (f_validar_campo_cadena_texto($_POST['apellidos_modificar_cliente'])) == false) {
        //Guardamos el error en la variable
        $error_apellidos_modificar_cliente = 'apellidos_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'Los APELLIDOS <b>\"' . $_POST['apellidos_modificar_cliente'] . '\"</b> no son reconocidos como cadena de texto.';
    }

    //Si el campo fecha nacimiento está vacío...
    if (empty($_POST['fecha_nacimiento_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo FECHA.';
    }

    //Si el campo fecha nacimiento contiene información y NO es una fecha de entrada válida (lo comprobamos con la función creada)
    if ((!empty($_POST['fecha_nacimiento_modificar_cliente'])) && (f_validar_fecha_entrada($_POST['fecha_nacimiento_modificar_cliente'])) == false) {
        //Guardamos el error en la variable
        $error_fecha_nacimiento_modificar_cliente = 'fecha_nacimiento_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La FECHA NACIMIENTO <b>\"' . $_POST['fecha_nacimiento_modificar_cliente'] . '\"</b> no sigue el formato válido (ej: \"dd/mm/aaaa\").';
    }

    //Si el campo fecha nacimiento contiene información
    if (!empty($_POST['fecha_nacimiento_modificar_cliente'])) {

        list($error_fecha_nacimiento, $alertas_errores_fecha) = f_verificar_fecha_nacimiento('fecha_nacimiento_modificar_cliente');

        //Recorremos el array de alertas de errores
        foreach ($alertas_errores_fecha as $alerta_error_fecha) {
            //Guardamos cada error de la fecha en el array de alertas
            $alertas_errores[] = $alerta_error_fecha;
        }
    }

    //Si el campo dni está vacío...
    if (empty($_POST['dni_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo DNI.';
    }

    //Si el campo dni contiene información y NO es un dni válido (lo comprobamos con la función creada)
    if ((!empty($_POST['dni_modificar_cliente'])) && (f_validar_DNI($_POST['dni_modificar_cliente'])) == false) {
        //Guardamos el error en la variable
        $error_dni_modificar_cliente = 'dni_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El DNI <b>\"' . $_POST['dni_modificar_cliente'] . '\"</b> no sigue el formato válido <br />(ej: \"00000000A\").';
    }

    //Si el campo dni contiene información y SÍ encontramos ese DNI en la base de datos... 
    if ((!empty($_POST['dni_modificar_cliente'])) && (f_buscar_existencia_campo_bd($conexionPDO, 'dni', $_POST['dni_modificar_cliente'])) == true) {

        //Comprobar que ese DNI sea del cliente que queramos modificar o no
        if (f_buscar_info_cliente_solicitado($conexionPDO, 'dni', $_POST['dni_modificar_cliente']) == false) {

            //Guardamos el error en la variable
            $error_dni_modificar_cliente = 'dni_modificar_cliente';

            //Guardamos los errores en el array de alertas
            $alertas_errores[] = 'El DNI <b>\"' . $_POST['dni_modificar_cliente'] . '\"</b> ya existe en la base de datos.';
        }
    }

    //Si el campo Nº Afiliación Seg. social está vacío...
    if (empty($_POST['num_afi_ss_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo Nº SEG. SOCIAL.';
    }

    //Si el campo dni contiene información y NO es un dni válido (lo comprobamos con la función creada)
    if ((!empty($_POST['num_afi_ss_modificar_cliente'])) && (f_validar_num_ss($_POST['num_afi_ss_modificar_cliente'])) == false) {
        //Guardamos el error en la variable
        $error_num_afi_ss_modificar_cliente = 'num_afi_ss_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El Nº SEG. SOCIAL <b>\"' . $_POST['num_afi_ss_modificar_cliente'] . '\"</b> no sigue el formato válido ( longitud 12 dígitos ).';
    }

    //Si el campo Nº SEG. SOCIAL contiene información y SÍ existe en la base de datos...
    if ((!empty($_POST['num_afi_ss_modificar_cliente'])) && (f_buscar_existencia_campo_bd($conexionPDO, 'num_afi_ss', $_POST['num_afi_ss_modificar_cliente'])) == true) {

        //Comprobar que ese Nº SEG. SOCIAL sea del cliente que queramos modificar o no
        if (f_buscar_info_cliente_solicitado($conexionPDO, 'num_afi_ss', $_POST['num_afi_ss_modificar_cliente']) == false) {

            //Guardamos el error en la variable
            $error_num_afi_ss_modificar_cliente = 'num_afi_ss_modificar_cliente';

            //Guardamos los errores en el array de alertas
            $alertas_errores[] = 'El Nº SEG. SOCIAL <b>\"' . $_POST['num_afi_ss_modificar_cliente'] . '\"</b> ya existe en la base de datos.';
        }
    }
    //Si el campo dirección está vacío...
    if (empty($_POST['direccion_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo DIRECCIÓN.';
    }

    //Si el campo direccion NO está vacío y NO es un campo de texto
    if (!empty($_POST['direccion_modificar_cliente']) && (f_validar_campo_cadena_texto($_POST['direccion_modificar_cliente'])) == false) {
        //Guardamos el error en la variable
        $error_direccion_modificar_cliente = 'direccion_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'La DIRECCIÓN <b>\"' . $_POST['direccion_modificar_cliente'] . '\"</b> no es reconocida como cadena de texto.';
    }

    //Si el campo email está vacío...
    if (empty($_POST['email_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo EMAIL.';
    }

    //Si el campo email contiene información y NO es un email válido (lo comprobamos con la función creada)
    if ((!empty($_POST['email_modificar_cliente'])) && (f_validar_EMAIL($_POST['email_modificar_cliente'])) == false) {
        //Guardamos el error en la variable
        $error_email_modificar_cliente = 'email_modificar_cliente';

        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'El EMAIL <b>\"' . $_POST['email_modificar_cliente'] . '\"</b> no sigue el formato válido (ej: \"example@domain.com\").';
    }

    //Si el campo email contiene información y SÍ existe en la base de datos...
    if ((!empty($_POST['email_modificar_cliente'])) && (f_buscar_existencia_campo_bd($conexionPDO, 'email', $_POST['email_modificar_cliente'])) == true) {

        //Comprobar que ese EMAIL sea del cliente que queramos modificar o no
        if (f_buscar_info_cliente_solicitado($conexionPDO, 'email', $_POST['email_modificar_cliente']) == false) {

            //Guardamos el error en la variable
            $error_email_modificar_cliente = 'email_modificar_cliente';

            //Guardamos los errores en el array de alertas
            $alertas_errores[] = 'El EMAIL <b>\"' . $_POST['email_modificar_cliente'] . '\"</b> ya existe en la base de datos.';
        }
    }

    //Si el campo teléfono móvil está vacío...
    if (empty($_POST['telefono_modificar_cliente'])) {
        //Guardamos los errores en el array de alertas
        $alertas_errores[] = 'No se puede dejar vacío el campo TELÉFONO.';
    }

    //Si el campo teléfono NO está vacío...
    if (!empty($_POST['telefono_modificar_cliente'])) {

        list($error_telefono_modificar_cliente, $alertas_errores_telefono) = f_validar_telefono($_POST['telefono_modificar_cliente'], 'telefono_modificar_cliente');

        //Recorremos el array de alertas de errores
        foreach ($alertas_errores_telefono as $alerta_error_telefono) {
            //Guardamos cada error del telefono en el array de alertas
            $alertas_errores[] = $alerta_error_telefono;
        }
    }


    //Si el campo teléfono contiene información y SÍ existe en la base de datos... ( validar campos únicos )
    if ((!empty($_POST['telefono_modificar_cliente'])) && (f_buscar_existencia_campo_bd($conexionPDO, 'telefono', $_POST['telefono_modificar_cliente'])) == true) {

        //Comprobar que ese TELÉFONO sea del cliente que queramos modificar o no
        if (f_buscar_info_cliente_solicitado($conexionPDO, 'telefono', $_POST['telefono_modificar_cliente']) == false) {

            //Guardamos el error en la variable
            $error_telefono_modificar_cliente = 'telefono_modificar_cliente';

            //Guardamos los errores en el array de alertas
            $alertas_errores[] = 'El TELÉFONO <b>\"' . $_POST['telefono_modificar_cliente'] . '\"</b> ya existe en la base de datos.';
        }
    }

    //Guardamos todas estas variables en un array 'error'
    $error = [
        $error_nombre_modificar_cliente,
        $error_apellidos_modificar_cliente,
        $error_fecha_nacimiento,
        $error_dni_modificar_cliente,
        $error_num_afi_ss_modificar_cliente,
        $error_direccion_modificar_cliente,
        $error_email_modificar_cliente,
        $error_telefono_modificar_cliente
    ];

    //Devolvemos los errores
    return array($error, $alertas_errores);
}

//Función con comprobaciones para el <head> de "nuevo_cliente.php"
function f_comprobacion_head_modificar_cliente($error) {

    //Funcion para colorear el INPUT del tipo DATE
    f_colorear_input_type_date_modificar_cliente('modificar-cliente', 'fecha_nacimiento_modificar_cliente', $error);
}

//Función para realizar las comprobaciones para modificar un cliente
function f_comprobaciones_modificar_cliente($conexionPDO) {

    //Si hemos pulsado el botón "Modificar Cliente"
    if (!empty($_POST['modificar-cliente'])) {
        //Recogemos 'error' y 'errores' de la función
        list($error, $alertas_errores) = f_errores_modificar_cliente($conexionPDO);

        //Comprobar si todos los campos NO están vacíos y continuamos
        if (!empty($_POST['nombre_modificar_cliente']) &&
                !empty($_POST['apellidos_modificar_cliente']) &&
                !empty($_POST['fecha_nacimiento_modificar_cliente']) &&
                !empty($_POST['dni_modificar_cliente']) &&
                !empty($_POST['num_afi_ss_modificar_cliente']) &&
                !empty($_POST['direccion_modificar_cliente']) &&
                !empty($_POST['email_modificar_cliente']) &&
                !empty($_POST['telefono_modificar_cliente'])) {

            //Si no hay errores
            if (!$alertas_errores) {
                //Recogemos los registros
                list($nombre_modificar_cliente, $apellidos_modificar_cliente, $registros) = f_modificar_cliente($conexionPDO);
            }
        }
        //Devolvemos errores, registros, nombre y apellidos del cliente a modificar
        return array($nombre_modificar_cliente, $apellidos_modificar_cliente, $registros, $error, $alertas_errores);
    }
}

//Función para realizar la operación de MODIFICAR un CLIENTE (*UPDATE*)
function f_modificar_cliente($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //consulta UPDATE - con PDO y preparadas
        //utilizando prepare, parámetros, execute
        //1º: preparo la consulta
        $sql = 'UPDATE clientes SET nombre=:nombre,'
                . ' apellidos=:apellidos, fecha_nacimiento=:fecha_nacimiento, dni=:dni,'
                . ' num_afi_ss=:num_afi_ss, direccion=:direccion, email=:email, telefono=:telefono'
                . ' WHERE id=:id';
        $registros = $conexionPDO->prepare($sql);
        //2º: poner nombres a las variables
        $nombre_modificar_cliente = $_POST['nombre_modificar_cliente'];
        $apellidos_modificar_cliente = $_POST['apellidos_modificar_cliente'];
        $fecha_nacimiento_modificar_cliente = $_POST['fecha_nacimiento_modificar_cliente'];
        $dni_modificar_cliente = $_POST['dni_modificar_cliente'];
        $num_afi_ss_modificar_cliente = $_POST['num_afi_ss_modificar_cliente'];
        $direccion_modificar_cliente = $_POST['direccion_modificar_cliente'];
        $email_modificar_cliente = $_POST['email_modificar_cliente'];
        $telefono_modificar_cliente = $_POST['telefono_modificar_cliente'];
        $id_modificar_cliente = $_POST['id_cliente_hidden'];
        //3º: paso de parámetros
        $registros->bindParam(":nombre", $nombre_modificar_cliente);
        $registros->bindParam(":apellidos", $apellidos_modificar_cliente);
        $registros->bindParam(":fecha_nacimiento", $fecha_nacimiento_modificar_cliente);
        $registros->bindParam(":dni", $dni_modificar_cliente);
        $registros->bindParam(":num_afi_ss", $num_afi_ss_modificar_cliente);
        $registros->bindParam(":direccion", $direccion_modificar_cliente);
        $registros->bindParam(":email", $email_modificar_cliente);
        $registros->bindParam(":telefono", $telefono_modificar_cliente);
        $registros->bindParam(":id", $id_modificar_cliente);
        //4º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor del REGISTRO de la consulta,
        //el NOMBRE y los APELLIDOS del cliente modificado
        return array($nombre_modificar_cliente, $apellidos_modificar_cliente, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función sobre las comprobaciones de modificar cliente ( en el SECTION )
function f_section_modificar_cliente($nombre_modificar_cliente, $apellidos_modificar_cliente, $registros, $error, $alertas_errores, $conexionPDO) {

    //Si el botón "Modificar Cliente" contiene información...
    if (!empty($_POST['modificar-cliente'])) {

        //Si hay error o errores...
        if ($alertas_errores) {
            //Incluimos el formulario de modificar cliente especificando los errores dentro del formulario
            include '../../formularios/formulario_modificar_cliente.php';
        }
        //Si no hay errores
        else {
            //Comprobar si todos los campos NO están vacíos y continuamos
            if (!empty($_POST['nombre_modificar_cliente']) &&
                    !empty($_POST['apellidos_modificar_cliente']) &&
                    !empty($_POST['fecha_nacimiento_modificar_cliente']) &&
                    !empty($_POST['dni_modificar_cliente']) &&
                    !empty($_POST['num_afi_ss_modificar_cliente']) &&
                    !empty($_POST['direccion_modificar_cliente']) &&
                    !empty($_POST['email_modificar_cliente']) &&
                    !empty($_POST['telefono_modificar_cliente'])) {

                //Si hay registros...
                if ($registros) {
                    //Mostramos un mensaje de éxito de la modificación
                    $alertas_exitos[] = 'CLIENTE <b>\"' . $nombre_modificar_cliente . ' ' . $apellidos_modificar_cliente . '\"</b> modificado correctamente.';
                    f_mostrar_alerta_exito_modifElemento('cliente', $alertas_exitos, './listado_clientes.php');
                }
                //Si no hay registros...
                else {
                    //Mostramos un mensaje de error de la inserción
                    $alertas_errores[] = 'No se ha podido modificar. Inténtelo de nuevo más tarde.';
                    f_mostrar_alerta_errores($alertas_errores);
                }
            }
            //Incluimos la maquetación del formulario para modificar cliente
            include '../../formularios/formulario_modificar_cliente.php';
        }
    }
    // Si aún no se ha pulsado ningún botón... (posición inicial de la página)
    else {
        //Incluimos la maquetación del formulario para modificar cliente
        include '../../formularios/formulario_modificar_cliente.php';
    }
}

//Función que contiene la maquetación del section de listado_clientes
function f_section_listado_clientes($titulo) {
    ?>
    <div class="listado clientes">
        <div class="envoltura-titulo-listado">
            <div>
                <label class="titulo-listado tituloListadoSmMdLg">&nbsp;Listado <?php echo $titulo; ?>&nbsp;</label>
            </div>
            <div id="tDivPdfBuscador">
                <div>
                    <a class="generarPDF xxl sm" href="./generarPDF_clientes.php">
                        <img src="../../imagenes/icono-pdf-verde.png" alt="PDF">
                    </a>
                </div>
                <div>
                    <input class="buscador" type="text" name="busquedaClientes" id="tInpClientes" placeholder="Buscar:"/>
                </div>
            </div>
        </div>
        <div class="resultados-clientes">
            <div id="tDivResultados"></div>
        </div>
    </div>
    <?php
    f_comprobaciones_section_listado_clientes();
}

function f_comprobaciones_section_listado_clientes() {

    if (!empty($_POST['eliminar-cliente']) || !empty($_POST['id_cliente_hidden'])) {
        f_mostrar_alerta_para_eliminarElemento('cliente', './eliminar_cliente.php?id=' . $_POST['id_cliente_hidden']);
    }
}

//Función para extrar la información necesaria del CLIENTE A ELIMINAR
function buscar_informacion_cliente_solicitado($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //Asignamos el valor para el ID
        //Si no hemos pulsado aún el botón Modificar Cliente
        if (empty($_POST['modificar-cliente'])) {
            //El valor del id será el valor del id de la barra de direcciones ($_GET)
            $id = $_GET['id'];
        }
        //Si SÍ hemos pulsado aún el botón Modificar Cliente
        else {
            //El valor del id será el valor del id hidden de haber pulsado el botón
            $id = $_POST['id_cliente_hidden'];
        }

        //Realizamos una consulta para poder obtener toda la información del cliente que queremos eliminar
        //1º: preparo la consulta
        $sql = 'SELECT * FROM clientes WHERE id=' . $id;
        $resultado = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $resultado->execute();
        //Obtenemos el valor para luego mostrar el elemento que queremos mostrar de la tabla CLIENTES
        $fila = $resultado->fetch();
        //Devolvemos el valor completo de la FILA del CLIENTE solicitado
        return $fila;
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_buscar_existencia_campo_bd($conexionPDO, $elemento, $campo_input) {
    //CONTROL DE ERRORES
    try {
        //Realizamos una consulta para poder obtener toda la información del cliente que queremos eliminar
        //1º: preparo la consulta
        $sql = 'SELECT ' . $elemento . ' FROM clientes WHERE ' . $elemento . '= "' . $campo_input . '"';
        $resultado = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $resultado->execute();
        //Si existen resultados
        if ($fila = $resultado->fetch()) {
            //Devuelve verdadero
            return true;
        }
        //Si no existen resultados
        else {
            //Devuelve falso
            return false;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

function f_buscar_info_cliente_solicitado($conexionPDO, $elemento, $campo_input) {
    //CONTROL DE ERRORES
    try {
        //Asignamos el valor para el ID
        //Si no hemos pulsado aún el botón Modificar Cliente
        if (empty($_POST['modificar-cliente'])) {
            //El valor del id será el valor del id de la barra de direcciones ($_GET)
            $id = $_GET['id'];
        }
        //Si SÍ hemos pulsado aún el botón Modificar Cliente
        else {
            //El valor del id será el valor del id hidden de haber pulsado el botón
            $id = $_POST['id_cliente_hidden'];
        }

        //Realizamos una consulta para poder obtener toda la información del cliente que queremos
        //1º: preparo la consulta
        $sql = 'SELECT ' . $elemento . ' FROM clientes WHERE id=' . $id . ' AND ' . $elemento . '= "' . $campo_input . '"';
        $resultado = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $resultado->execute();
        //Si existen resultados
        if ($fila = $resultado->fetch()) {
            //Devuelve verdadero
            return true;
        } else {
            return false;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función que contiene la maquetación del section de listado_clientes
function f_comprobaciones_eliminar_cliente($conexionPDO) {
    //Si NO está vacío el ID en la barra de direcciones del navegador
    if (!empty($_GET['id'])) {
        //Procedemos al **DELETE** del contacto
        //Recogemos los valores REGISTROS, NOMBRE y APELLIDOS del nuevocliente
        //del array que nos devuelve la función creada para insertar a un cliente nuevo
        list($fila, $registros) = f_delete_cliente($conexionPDO);

        //Si hay registros...
        if ($registros) {
            //Devolvemos $fila y $registros
            return array($fila, $registros);
        }
    }
}

//Función para realizar la operación de ELIMINAR un CLIENTE (*DELETE*)
function f_delete_cliente($conexionPDO) {
    //CONTROL DE ERRORES
    try {
        //Guardar en una variable la información obtenida de la CONSULTA
        $fila = buscar_informacion_cliente_solicitado($conexionPDO);

        //Preparamos una consulta para realizar el DELETE
        //1º: preparo la consulta
        $registros = $conexionPDO->prepare('DELETE FROM clientes WHERE id=' . $_GET['id']);
        //2º: ejecuto la consulta
        $registros->execute();

        //Devolvemos en un array el valor de la FILA y el REGISTRO de la consulta,
        return array($fila, $registros);
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}

//Función para incluir lo que queremos que esté en el <section> de 'eliminar_cliente.php'
function f_section_eliminar_cliente($fila, $registros) {
    f_section_listado_clientes('Clientes');
    //Si hay registros...
    if ($registros) {
        //Mostramos un mensaje de error de la inserción
        $alertas_exitos[] = 'CLIENTE <b>\"' . $fila['nombre'] . ' ' . $fila['apellidos'] . '\"</b> eliminado correctamente.';
        f_mostrar_alerta_exito_elimElemento('cliente', $alertas_exitos, './listado_clientes.php');
    }
    //Si no hay registros...
    else {
        //Mostramos un mensaje de error de la eliminación
        $alertas_errores[] = 'No se ha podido insertar. Inténtelo de nuevo más tarde.';
        f_mostrar_alerta_errores($alertas_errores);
    }
}

//Función para buscar clientes por sus campos únicos
function f_buscar_exitencia_cliente_por_campos_unicos($conexionPDO, $dni, $num_afi_ss, $email, $telefono) {
    //CONTROL DE ERRORES
    try {
        //Preparamos una consulta para realizar el SELECT
        //1º: preparo la consulta
        $sql = 'SELECT dni, num_afi_ss, email, telefono'
                . ' FROM clientes '
                . ' WHERE dni="' . $dni . '" AND num_afi_ss="' . $num_afi_ss . '"'
                . ' AND email="' . $email . '" AND telefono=' . $telefono . '';
        $consulta = $conexionPDO->prepare($sql);
        //2º: ejecuto la consulta
        $consulta->execute();

        if ($fila = $consulta->fetch()) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $p) {
        echo "Error " . $p->getMessage() . "<br />";
    }
}
