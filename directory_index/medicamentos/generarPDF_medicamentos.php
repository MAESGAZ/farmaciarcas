<?php

session_start();

//Si la sesión está vacía, redireccionar la página al index
if (empty($_SESSION['usuario'])) {
    header('Location: ../index/index.php');
}

require_once '../../vendor/autoload.php';
require_once '../../base_datos/bd.php';
$conexion = f_abrir_conexion_PDO();

$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

$estilo = file_get_contents('../../css/pdf/estilosPDF.css');
$mpdf->WriteHTML($estilo, 1);

$cabecera = '
        <div class="envoltura-logo">
            <img class="farmaciarcas-logo" alt="FarmaciArcas" src="../../imagenes/cruz_verde.png">
        </div>
        
            <div class="farmaci">FARMACI</div>
            <div class="arcas">ARCAS</div>
        ';
$mpdf->SetHTMLHeader($cabecera);

$pie = '<div class="pdf-footer">Página: {PAGENO} </div>';
$mpdf->SetHTMLFooter($pie);

$introduccion = '<div class="titulo-parrafo">Listado actual de MEDICAMENTOS a fecha: <span class="fecha">' . date('d/m/Y') . '</span></div>';
$mpdf->WriteHTML($introduccion);

$tabla = '
    <div class="contenedor-tabla">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>DESCRIPCIÓN</th>
                    <th>CANTIDAD</th> 
                    <th>PRECIO PVP</th>
                    <th>PRECIO ABONO</th>
                    <th>RESTRICCIÓN</th>
                </tr>
            </thead>
            <tbody>';

try {
    $consulta = $conexion->prepare('SELECT * FROM medicamentos');
    $consulta->execute();
    while ($resultado = $consulta->fetch()) {
        $tabla .= '
            <tr>
                <td>' . $resultado['id'] . '</td>
                <td>' . $resultado['nombre'] . '</td>
                <td>' . $resultado['descripcion'] . '</td>
                <td>' . $resultado['cantidad'] . 'ud/s</td>';
        if ($resultado['restricciones_medicamento'] == 'CON receta') {
            $tabla .= '<td>---</td>';
            $tabla .= '<td>' . $resultado['precio_abono'] . '€/ud</td>';
            $tabla .= '<td>SÓLO abonados</td>';
        } elseif ($resultado['restricciones_medicamento'] == 'SIN receta') {
            $tabla .= '<td>' . $resultado['precio_pvp'] . '€/ud</td>';
            $tabla .= '<td>---</td>';
            $tabla .= '<td>SÓLO pvp</td>';
        } elseif ($resultado['restricciones_medicamento'] == 'Ninguna restricción') {
            $tabla .= '<td>' . $resultado['precio_pvp'] . '€/ud</td>';
            $tabla .= '<td>' . $resultado['precio_abono'] . '€/ud</td>';
            $tabla .= '<td>---</td>';
        }

        $tabla .= '</tr>';
    }
} catch (PDOException $ex) {
    echo '<p>Error: ' . $ex->getMessage() . '</p>';
}

$tabla .= '     </tbody>
            </table>
        </div>';

$mpdf->WriteHTML($tabla);
$mpdf->SetTitle('ListadoMedicamentos');
$mpdf->Output('listado_medicamentos.pdf', 'D');
