<?php
//no cache
header('Cache-Control: no cache');
// limitador de caché actual
session_cache_limiter('private_no_expire');
//Iniciar la sesión
session_start();
//Incluimos el archivo con las funciones genéricas para la Base de Datos
include '../../base_datos/bd.php';
//Incluimos el archivo de funciones genéricas
include '../../complementos/funciones.php';

//Si el botón "Cerrar Sesión" contiene información...
if ((isset($_POST['cerrar_sesion'])) || (!empty($_POST['cerrar_sesion']))) {
    //borramos la variable de sesion del USUARIO
    unset($_SESSION['usuario']);
    //destruimos la sesion
    session_destroy();
}

// Incluimos lo necesario para mandar un email cuando se han registrado.
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once '../../vendor/phpmailer/phpmailer/src/Exception.php';
require_once '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once '../../vendor/phpmailer/phpmailer/src/SMTP.php';

require_once '../../vendor/autoload.php';
$mail = new PHPMailer(true);
$plantilla = file_get_contents("../../complementos/plantilla_email/template_new_user.html");

//Llamada a la función con las comprobaciones del login
list($alertas_errores, $clave_cifrada) = f_comprobaciones_login();
?>
<!DOCTYPE html>
<!-- PROYECTO -->
<!-- FARMACIARCAS -->
<!-- Autores: Miguel Ángel Espín Gázquez -->
<!----------- Juan Pablo Sáez Sánchez ----->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../imagenes/ico/favicon.ico" rel="icon">
        <link rel="stylesheet" href="../../css/estilo_general.css"/>
        <link rel="stylesheet" href="../../css/sm.css"/>
        <link rel="stylesheet" href="../../css/lg.css"/>
        <link rel="stylesheet" href="../../css/md.css"/>
        <link rel="stylesheet" href="../../css/xl.css"/>
        <link rel="stylesheet" href="../../css/xxl.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert.css" />
        <script src="../../javascript/sweetalert.min.js"></script>
        <?php f_comprobacion_head_index(); ?>
        <title>FARMACIARCAS</title>
    </head>
    <body>
        <header><?php include '../../maquetacion/header.php'; ?></header>
        <main>
            <nav><?php include '../../maquetacion/menu.php'; ?></nav>
            <section><?php include '../../maquetacion/usuarios.php'; ?></section>
        </main>
        <footer><?php include '../../maquetacion/footer.php'; ?></footer>
    </body>
</html>