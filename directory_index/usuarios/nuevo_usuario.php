<?php
//no cache
header('Cache-Control: no cache');
// limitador de caché actual
session_cache_limiter('private_no_expire');
//Iniciar la sesión
session_start();
//Incluimos el archivo con las funciones genéricas para la Base de Datos
include '../../base_datos/bd.php';
//Incluimos el archivo de funciones genéricas
include '../../complementos/funciones.php';

// Incluimos lo necesario para mandar un email cuando se han registrado.
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once '../../vendor/phpmailer/phpmailer/src/Exception.php';
require_once '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once '../../vendor/phpmailer/phpmailer/src/SMTP.php';

require_once '../../vendor/autoload.php';
$mail = new PHPMailer(true);
$plantilla1 = file_get_contents("../../complementos/plantilla_email/template_link_confirmation_user.html");

//Abrimos la CONEXIÓN PDO
$conexionPDO = f_abrir_conexion_PDO();
//Recogemos el boton, la clave, los errores y registros de las comprobaciones del nuevo usuario
list($alertas_errores, $error, $registros, $usuario_nuevo) = f_comprobaciones_nuevo_usuario($conexionPDO, $mail, $plantilla1);
if (!empty($_SESSION['usuario'])) {
    header("Location: ../index/index.php");
}
?>
<!DOCTYPE html>
<!-- PROYECTO -->
<!-- FARMACIARCAS -->
<!-- Autores: Miguel Ángel Espín Gázquez -->
<!----------- Juan Pablo Sáez Sánchez ----->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../imagenes/ico/favicon.ico" rel="icon">
        <link rel="stylesheet" href="../../css/estilo_general.css"/>
        <link rel="stylesheet" href="../../css/sm.css"/>
        <link rel="stylesheet" href="../../css/lg.css"/>
        <link rel="stylesheet" href="../../css/md.css"/>
        <link rel="stylesheet" href="../../css/xl.css"/>
        <link rel="stylesheet" href="../../css/xxl.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert.css" />
        <script src="../../javascript/sweetalert.min.js"></script>
        <script defer src="../../javascript/nuevo_usuario.js"></script>
        <title>FARMACIARCAS</title>
    </head>
    <body>
        <header><?php include '../../maquetacion/header.php'; ?></header>
        <main>
            <nav><?php include '../../maquetacion/menu.php'; ?></nav>
            <section><?php f_section_nuevo_usuario($alertas_errores, $error, $registros, $usuario_nuevo, $conexionPDO); ?></section>
        </main>
        <footer><?php include '../../maquetacion/footer.php'; ?></footer>
    </body>
</html>
