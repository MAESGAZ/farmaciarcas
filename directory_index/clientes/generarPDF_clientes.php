<?php

session_start();

//Si la sesión está vacía, redireccionar la página al index
if (empty($_SESSION['usuario'])) {
    header('Location: ../index/index.php');
}

require_once '../../base_datos/bd.php';
require_once '../../complementos/funciones.php';
require_once '../../vendor/autoload.php';

$conexionPDO = f_abrir_conexion_PDO();

$mpdf = new \Mpdf\Mpdf(['orientation' => 'L']);

$estilo = file_get_contents('../../css/pdf/estilosPDF.css');

$mpdf->WriteHTML($estilo, 1);

$cabecera = '
        <div class="envoltura-logo">
            <img class="farmaciarcas-logo" alt="FarmaciArcas" src="../../imagenes/cruz_verde.png">
        </div>
        
            <div class="farmaci">FARMACI</div>
            <div class="arcas">ARCAS</div>
        ';
$mpdf->SetHTMLHeader($cabecera);

$pie = '<div class="pdf-footer">Página: {PAGENO} </div>';
$mpdf->SetHTMLFooter($pie);
$introduccion = '<div class="titulo-parrafo">Listado actual de CLIENTES a fecha: <span class="fecha">' . date('d/m/Y') . '</span></div>';
$mpdf->WriteHTML($introduccion);
$tabla = '
    <div class="contenedor-tabla">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>APELLIDOS</th>
                    <th>FECHA NACIMIENTO</th>
                    <th>DNI</th>
                    <th>DIRECCIÓN</th>
                    <th>EMAIL</th>
                    <th>TELÉFONO</th>
                </tr>
            </thead>
            <tbody>';
try {
    $consulta = $conexionPDO->prepare('SELECT * FROM clientes');
    $consulta->execute();
    while ($resultado = $consulta->fetch()) {
        $tabla .= '<tr>
            <td>' . $resultado['id'] . '</td>
            <td>' . $resultado['nombre'] . '</td>
            <td>' . $resultado['apellidos'] . '</td>
            <td class="celda-fecha">' . f_validar_fecha_salida($resultado['fecha_nacimiento']) . '</td>
            <td>' . $resultado['dni'] . '</td>
            <td>' . $resultado['direccion'] . '</td>
            <td>' . $resultado['email'] . '</td>
            <td>' . $resultado['telefono'] . '</td>
                </tr>';
    }
} catch (PDOException $ex) {
    echo '<p>Error: ' . $ex->getMessage() . '</p>';
}
$tabla .= '     </tbody>
            </table>
        </div>';
$mpdf->WriteHTML($tabla);
$mpdf->SetTitle('ListadoClientes');
$mpdf->Output('listado_clientes.pdf', 'D');
