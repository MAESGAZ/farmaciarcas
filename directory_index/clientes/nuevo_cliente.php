<?php
//Iniciar la sesión
session_start();
//Incluimos el archivo con las funciones genéricas para la Base de Datos
include '../../base_datos/bd.php';
//Incluimos el archivo con las funciones específicas para la Base de Datos
include '../../base_datos/bd_clientes.php';
//Incluimos el archivo de funciones genéricas
include '../../complementos/funciones.php';
//Abrimos la CONEXIÓN PDO
$conexionPDO = f_abrir_conexion_PDO();
list($error, $alertas_errores, $registros, $nombre_cliente_nuevo, $apellidos_cliente_nuevo) = f_comprobaciones_nuevo_cliente($conexionPDO);
//Si la sesión está vacía, redireccionar la página al index
if (empty($_SESSION['usuario'])) {
    header('Location: ../index/index.php');
}
?>
<!DOCTYPE html>
<!-- PROYECTO -->
<!-- FARMACIARCAS -->
<!-- Autores: Miguel Ángel Espín Gázquez -->
<!----------- Juan Pablo Sáez Sánchez ----->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../imagenes/ico/favicon.ico" rel="icon">
        <link rel="stylesheet" href="../../css/estilo_general.css"/>
        <link rel="stylesheet" href="../../css/sm.css"/>
        <link rel="stylesheet" href="../../css/lg.css"/>
        <link rel="stylesheet" href="../../css/md.css"/>
        <link rel="stylesheet" href="../../css/xl.css"/>
        <link rel="stylesheet" href="../../css/xxl.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert.css" />
        <script src="../../javascript/sweetalert.min.js"></script>
        <script defer src="../../javascript/nuevo_modificar_cliente.js"></script>
        <?php f_comprobacion_head_nuevo_cliente(); ?>
        <title>FARMACIARCAS</title>
    </head>
    <body>
        <header><?php include '../../maquetacion/header.php'; ?></header>
        <main>
            <nav><?php include '../../maquetacion/menu.php'; ?></nav>
            <section><?php f_section_nuevo_cliente($error, $alertas_errores, $registros, $nombre_cliente_nuevo, $apellidos_cliente_nuevo, $conexionPDO); ?></section>
        </main>
        <footer><?php include '../../maquetacion/footer.php'; ?></footer>
    </body>
</html>
