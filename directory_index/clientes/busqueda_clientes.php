<?php
require_once '../../base_datos/bd.php';
if (file_get_contents('php://input')) {
    $datos = json_decode(file_get_contents('php://input'));
    $conexion = f_abrir_conexion_PDO();
    try {
        if ($datos !== "") {
            $consulta = $conexion->prepare(
                    "SELECT id, nombre, apellidos, fecha_nacimiento, dni, num_afi_ss, direccion, email, telefono"
                    . " FROM clientes"
                    . " WHERE id LIKE :parametro OR nombre LIKE :parametro OR apellidos LIKE :parametro OR fecha_nacimiento LIKE :parametro OR dni LIKE :parametro"
                    . " OR num_afi_ss LIKE :parametro OR direccion LIKE :parametro OR email LIKE :parametro OR telefono LIKE :parametro");

            $consulta->bindValue(":parametro", "%" . $datos . "%", PDO::PARAM_STR);
        } else {
            $consulta = $conexion->prepare("SELECT id, nombre, apellidos, fecha_nacimiento, dni, num_afi_ss, direccion, email, telefono"
                    . " FROM clientes");
        }
        $consulta->execute();
        $contenido = [];
        while ($resultado = $consulta->fetch()) {
            array_push($contenido, $resultado);
        }
    } catch (PDOException $e) {
        echo '<p>Error: ' . $e->getMessage() . '</p>';
    }
    header('HTTP/ 200 Clientes obtenidos');
    echo json_encode($contenido);
} else {
    //Si la sesión está vacía, redireccionar la página al index
    if (empty($_SESSION['usuario'])) {
        header('Location: ../index/index.php');
    }
}



