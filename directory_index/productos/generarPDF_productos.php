<?php

session_start();

//Si la sesión está vacía, redireccionar la página al index
if (empty($_SESSION['usuario'])) {
    header('Location: ../index/index.php');
}

require_once '../../vendor/autoload.php';
require_once '../../base_datos/bd.php';
$conexionPDO = f_abrir_conexion_PDO();

$mpdf = new \Mpdf\Mpdf();

$estilo = file_get_contents('../../css/pdf/estilosPDF_productos.css');
$mpdf->WriteHTML($estilo, 1);

$cabecera = '
        <div class="envoltura-logo">
            <img class="farmaciarcas-logo" alt="FarmaciArcas" src="../../imagenes/cruz_verde.png">
        </div>
            <div class="farmaci">FARMACI</div>
            <div class="arcas">ARCAS</div>
        ';
$mpdf->SetHTMLHeader($cabecera);

$pie = '<div class="pdf-footer">Página: {PAGENO} </div>';
$mpdf->SetHTMLFooter($pie);

$introduccion = '<div class="titulo-parrafo">Listado actual de PRODUCTOS a fecha: <span class="fecha">'.date('d/m/Y').'</span></div>';
$mpdf->WriteHTML($introduccion);

$tabla = '
    <div class="contenedor-tabla">
        <table align="center">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOMBRE</th>
                    <th>DESCRIPCIÓN</th>
                    <th>PRECIO</th>
                    <th>CANTIDAD</th>
                </tr>
            </thead>
            <tbody>';

try {
    $consulta = $conexionPDO->prepare('SELECT * FROM productos');
    $consulta->execute();
    while ($resultado = $consulta->fetch()) {
        $tabla .= '
            <tr>
                <td>' . $resultado['id'] . '</td>
                <td>' . $resultado['nombre'] . '</td>
                <td>' . $resultado['descripcion'] . '</td>
                <td>' . $resultado['precio'] . '</td>
                <td>' . $resultado['cantidad'] . '</td>
            </tr>';
    }
} catch (PDOException $ex) {
    echo '<p>Error: ' . $ex->getMessage() . '</p>';
}

$tabla .= '     </tbody>
            </table>
        </div>';

$mpdf->WriteHTML($tabla);

$mpdf->SetTitle('ListadoProductos');

$mpdf->Output('listado_productos.pdf', 'D');