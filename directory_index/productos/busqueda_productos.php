<?php

require_once '../../base_datos/bd.php';

if (file_get_contents('php://input')) {
    $datos = json_decode(file_get_contents('php://input'));
    $conexion = f_abrir_conexion_PDO();
    try {
        if ($datos !== "") {
            $consulta = $conexion->prepare(
                    "SELECT id, nombre, descripcion, precio, cantidad"
                    . " FROM productos"
                    . " WHERE id LIKE :parametro OR nombre LIKE :parametro OR descripcion LIKE :parametro"
                    . " OR precio LIKE :parametro OR cantidad LIKE :parametro");
            $consulta->bindValue(":parametro", "%" . $datos . "%", PDO::PARAM_STR);
        } else {
            $consulta = $conexion->prepare("SELECT id, nombre, descripcion, precio, cantidad"
                    . " FROM productos");
        }
        $consulta->execute();
        $contenido = [];
        while ($resultado = $consulta->fetch()) {
            array_push($contenido, $resultado);
        }
    } catch (PDOException $ex) {
        echo '<p>Error: ' . $ex->getMessage() . '</p>';
    }
    header('HTTP/ 200 Productos obtenidos');
    echo json_encode($contenido);
} else {
    //Si la sesión está vacía, redireccionar la página al index
    if (empty($_SESSION['usuario'])) {
        header('Location: ../index/index.php');
    }
}