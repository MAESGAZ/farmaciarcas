<?php
//Iniciar la sesión
session_start();
//Incluimos el archivo con las funciones genéricas para la Base de Datos
include '../../base_datos/bd.php';
//Incluimos el archivo con las funciones específicas para la Base de Datos
include '../../base_datos/bd_productos.php';
//Incluimos el archivo de funciones genéricas
include '../../complementos/funciones.php';
//Abrimos la CONEXIÓN PDO
$conexionPDO = f_abrir_conexion_PDO();
//Recogemos los valores REGISTROS y NOMBRE del medicamento de las comprobaciones
list($error, $alertas_errores, $nombre_modificar_producto, $registros) = f_comprobaciones_modificar_producto($conexionPDO);
//Si la sesión está vacía, redireccionar la página al index
if (empty($_SESSION['usuario'])) {
    header('Location: ../index/index.php');
}
//Si no existe ningún ID de producto en la barra de direcciones
if(empty($_POST['modificar-producto']) && empty($_GET['id'])){
    header('Location: ./listado_productos.php');
}
?>
<!DOCTYPE html>
<!-- PROYECTO -->
<!-- FARMACIARCAS -->
<!-- Autores: Miguel Ángel Espín Gázquez -->
<!----------- Juan Pablo Sáez Sánchez ----->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../imagenes/ico/favicon.ico" rel="icon">
        <link rel="stylesheet" href="../../css/estilo_general.css"/>
        <link rel="stylesheet" href="../../css/sm.css"/>
        <link rel="stylesheet" href="../../css/lg.css"/>
        <link rel="stylesheet" href="../../css/md.css"/>
        <link rel="stylesheet" href="../../css/xl.css"/>
        <link rel="stylesheet" href="../../css/xxl.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert.css" />
        <script src="../../javascript/sweetalert.min.js"></script>
        <script defer src="../../javascript/cargarCodigoJS.js"></script>
        <title>FARMACIARCAS</title>
    </head>
    <body>
        <header><?php include '../../maquetacion/header.php'; ?></header>
        <main>
            <nav><?php include '../../maquetacion/menu.php'; ?></nav>
            <section><?php f_section_modificar_producto($error, $alertas_errores, $nombre_modificar_producto, $registros, $conexionPDO); ?></section>
        </main>
        <footer><?php include '../../maquetacion/footer.php'; ?></footer>
    </body>
</html>
