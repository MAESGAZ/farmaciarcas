<?php
//no cache
header('Cache-Control: no cache');
// limitador de caché actual
session_cache_limiter('private_no_expire');
//Iniciar la sesión
session_start();
require_once '../../base_datos/bd.php';
require_once '../../complementos/funciones.php';

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require_once '../../vendor/phpmailer/phpmailer/src/Exception.php';
require_once '../../vendor/phpmailer/phpmailer/src/PHPMailer.php';
require_once '../../vendor/phpmailer/phpmailer/src/SMTP.php';

require_once '../../vendor/autoload.php';
$mail = new PHPMailer(true);

if (!empty($_SESSION['usuario'])) {
    header("Location: ../index/index.php");
}
?>
<!DOCTYPE html>
<!-- PROYECTO -->
<!-- FARMACIARCAS -->
<!-- Autores: Miguel Ángel Espín Gázquez -->
<!----------- Juan Pablo Sáez Sánchez ----->
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="../../imagenes/ico/favicon.ico" rel="icon">
        <link rel="stylesheet" href="../../css/estilo_general.css"/>
        <link rel="stylesheet" href="../../css/sm.css"/>
        <link rel="stylesheet" href="../../css/lg.css"/>
        <link rel="stylesheet" href="../../css/md.css"/>
        <link rel="stylesheet" href="../../css/xl.css"/>
        <link rel="stylesheet" href="../../css/xxl.css"/>
        <link rel="stylesheet" type="text/css" href="../../css/sweetalert.css" />
        <script src="../../javascript/sweetalert.min.js"></script>
        <script defer src="../../javascript/olvido_clave.js"></script>
        <?php f_colorear_placeholder_input_vacio('envio_email', 'correo', 'tInpCorreo'); ?>
        <title>FARMACIARCAS</title>
    </head>
    <body>
        <header><?php include '../../maquetacion/header.php'; ?></header>
        <main>
            <nav><?php include '../../maquetacion/menu.php'; ?></nav>
            <section><?php f_section_comprobacion_olvido_clave($mail); ?></section>
        </main>        
        <footer><?php include '../../maquetacion/footer.php'; ?></footer>
    </body>
</html>