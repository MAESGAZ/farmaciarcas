<div class="contenedor-header">
    <div class="contenedor-todo-titulo">
        <form class="formCruzVerde" action="../index/index.php" method="POST">
            <button class="buttonCruzVerde" type="submit" name="entrar">
                <img class="cruz_verde" src="../../imagenes/cruz_verde.png" alt="FarmaciArcas"/>
            </button>
        </form>
        <div class="titulo-farmaciarcas">
            <span>FARMACI</span>
            <span>ARCAS</span>
        </div>
    </div>
    <?php
    //Si SÍ está vacía la sesión del usuario...
    if (empty($_SESSION['usuario'])) {
        ?>
        <div class="contenedor-decoracion">
            <div class="contenedor-curvas">
                <img class="curvas" src="../../imagenes/curvas.png" alt="Curvas"/>
            </div>
            <div class="contenedor-hojas">
                <img class="hojas" src="../../imagenes/hojas.png" alt="Hojas"/>
            </div>
        </div>
        <?php
    }
    //Si NO está vacía la sesión del usuario...
    else {
        ?>
        <div class="contenedor-todo-cerrar-sesion">
            <div>
                <?php
                if (f_buscar_usuario_en_BD('genero', $_SESSION['usuario']) == 'M') {
                    ?>
                    <img class="icono-usuario" src="../../imagenes/farmaceutico.png">
                    <?php
                } elseif (f_buscar_usuario_en_BD('genero', $_SESSION['usuario']) == 'F') {
                    ?>
                    <img class="icono-usuario" src="../../imagenes/farmaceutica.png">
                    <?php
                }
                ?>
                <label class="titulo-nombre-usuario-sesion-header"><?php echo ' ' . $_SESSION['usuario']; ?></label>
                <div class="contenedor-triangulo-cambiar-clave">
                    <div class="triangulo-cambiar-clave"></div>
                </div>
                <div class="contenedor-formulario-cambiar-clave">
                    <?php include '../../formularios/formulario_cambio_clave_usuario.php'; ?>
                </div>
            </div>
            <div>
                <form class="form-logout" action="../index/index.php" method="POST">
                    <button class="buttonCerrarSesion" type="submit" name="cerrar_sesion">
                        <img class="logout-blanco" src="../../imagenes/logout-blanco.png" alt="Cerrar sesión">
                        <img class="logout-verde" src="../../imagenes/logout-verde.png" alt="Cerrar sesión">
                        <span>Cerrar sesión</span>
                    </button>
                </form>
            </div>
        </div>
        <?php
    }
    ?>
</div>


