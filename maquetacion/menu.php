<?php
//Si la variable $_SESSION['usuario'] NO está vacía...
if (!empty($_SESSION['usuario'])) {
    //Gestión CLIENTES
    ?>
    <div id="tDivBotonHamburguesa" class="boton-hamburguesa-verde boton-hamburguesa-blanco">
        <div class="conjunto-lineas-hamburguesa">
            <div id="tDivLinea1" class="linea-hamburguesa-blanca linea-hamburguesa-verde"></div>
            <div id="tDivLinea2" class="linea-hamburguesa-blanca linea-hamburguesa-verde"></div>
            <div id="tDivLinea3" class="linea-hamburguesa-blanca linea-hamburguesa-verde"></div>
        </div>
    </div>
    <ul id="tUlContenedorMenuNav" class="contenedor-menu-nav contenedor-menu-nav-movil">
        <li>
            <div class="gestionar-clientes">Gestionar Clientes</div>
            <div class="contenedor-desplegable contenedor-desplegable-clientes">
                <ul class="ul-contenedor-desplegable-clientes">
                    <li class="li-listado-clientes">
                        <a class="a-listado-clientes" href="../clientes/listado_clientes.php">Listado clientes</a>
                    </li>
                    <li class="li-nuevo-cliente">
                        <a class="a-nuevo-cliente" href="../clientes/nuevo_cliente.php">Nuevo cliente</a>
                    </li>
                </ul>
            </div>
        </li>
        <?php
        //Gestión PRODUCTOS
        ?>
        <li>
            <div class="gestionar-productos">Gestionar Productos</div>
            <div class="contenedor-desplegable contenedor-desplegable-productos">
                <ul class="ul-contenedor-desplegable-productos">
                    <li class="li-listado-productos">
                        <a class="a-listado-productos" href="../productos/listado_productos.php">Listado productos</a>
                    </li>
                    <li class="li-nuevo-cliente">
                        <a class="a-nuevo-cliente" href="../productos/nuevo_producto.php">Nuevo producto</a>
                    </li>
                </ul>
            </div>
        </li>
        <?php
        //Gestión MEDICAMENTOS
        ?>
        <li>
            <div class="gestionar-medicamentos">Gestionar Medicamentos</div>
            <div class="contenedor-desplegable contenedor-desplegable-medicamentos">
                <ul class="ul-contenedor-desplegable-medicamentos">
                    <li class="li-listado-medicamentos">
                        <a class="a-listado-medicamentos" href="../medicamentos/listado_medicamentos.php">Listado medicamentos</a>
                    </li>
                    <li class="li-nuevo-medicamentos">
                        <a class="a-nuevo-medicamentos" href="../medicamentos/nuevo_medicamento.php">Nuevo medicamento</a>
                    </li>
                </ul>
            </div>
        </li>
        <?php
        //Gestión PEDIDOS
        ?>
        <li>
            <div class="gestionar-pedidos">Gestionar Pedidos</div>
            <div class="contenedor-desplegable contenedor-desplegable-pedidos">
                <ul class="ul-contenedor-desplegable-pedidos">
                    <li class="li-listado-pedidos">
                        <div class="desplegable-listado-pedidos">Listado pedidos</div>
                    </li>
                    <div class="listado-pedidos">
                        <ul class="ul-listado-pedidos">
                            <li class="li-listado-pedidos">
                                <a class="a-listado-pedidos" href="../pedidos/listado_pedidos.php">Ver listado</a>
                            </li>
                            <li class="li-graficas-pedidos">
                                <a class="a-graficas-pedidos" href="../pedidos/graficas_pedidos.php">Gráficas</a>
                            </li>
                        </ul>
                    </div>
                    <li class="li-nuevo-pedido">
                        <a class="a-nuevo-pedido" href="../pedidos/nuevo_pedido.php">Nuevo pedido</a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
    <?php
}