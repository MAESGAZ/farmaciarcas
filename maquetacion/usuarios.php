<?php
if (empty($_SESSION['usuario'])) {
    //Si el Botón Entrar está vacío (no está pulsado aún...)
    if (empty($_POST['entrar'])) {
        if (!empty($_GET['id']) && !empty($_GET['usuario'])) {
            if (f_buscar_usuario_por_id_usuario($_GET['id'], $_GET['usuario']) == true) {
                if (f_buscar_usuario_confirmado($_GET['id'], $_GET['usuario']) === 'FALSE') {
                    f_actualizar_usuario_confirmado('TRUE', $_GET['id'], $_GET['usuario']);
                    list($plantilla, $fila) = f_rellenar_datos_template_new_user($_GET['id'], $_GET['usuario'], $plantilla);
                    f_enviar_email($mail, $plantilla, $fila['email'], $fila['nombre'], 'Bienvenido a FarmaciArcas');
                    $alertas_exitos[] = 'Usuario <b>\"' . $_GET['usuario'] . '\"</b> activado correctamente.';
                    $alertas_exitos[] = 'Ya tienes acceso a la aplicación.';
                    $alertas_exitos[] = 'Se le ha enviado un email informativo a su bandeja de entrada con todos los datos de registro del usuario.';
                    f_mostrar_alerta_exito_nuevoElemento($alertas_exitos, './index.php');
                } 
            }
        }
        //Incluimos el formulario del login de usuarios
        include '../../formularios/formulario_control_acceso_usuarios.php';
    }
    //Si SÍ está pulsado el Botón ENTRAR
    else {
        // Si no se ha validado el usuario...
        if (f_validar_usuario($_POST['usuario'], $clave_cifrada) == false) {
            //Si el Botón Entrar NO está vacío (ya está pulsado...)
            if (!empty($_POST['entrar'])) {

                //Incluimos el formulario del login de usuarios 
                include '../../formularios/formulario_control_acceso_usuarios.php';
            }
        }
    }
} else {
    //Función que realiza todas las instrucciones necesarias para hacer un cambio de clave del USUARIO
    f_cambiar_clave_usuario($mail);
}

