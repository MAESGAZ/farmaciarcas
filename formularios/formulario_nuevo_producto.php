<div class="formulario formularioNuevoProducto">
    <label>&nbsp;FORMULARIO NUEVO PRODUCTO&nbsp;</label>
    <label>&nbsp;NUEVO PRODUCTO&nbsp;</label>
    <form action="../productos/nuevo_producto.php" method="POST">
        <div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_producto_nuevo" placeholder="Ingrese nombre del producto" autocomplete="off"  <?php f_mostrar_campo_nuevo_elemento("nombre_producto_nuevo", $error); ?> />
            </div>
            <div>
                <div>Descripción*:</div>
                <textarea id="tInpDescripcion"
                          name="descripcion_producto_nuevo" 
                          placeholder="Ingrese una descripción del producto*"
                          ><?php f_mostrar_contenido_textarea('crear-nuevo-producto', 'descripcion_producto_nuevo', $valor_bd); ?></textarea>
            </div>
            <div>
                <div>Precio*:</div>
                <input id="tInpPrecio" type="number" step="any" name="precio_producto_nuevo" placeholder="Ingrese precio del producto" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("precio_producto_nuevo", $error); ?> />
            </div>
            <div>
                <div>Cantidad*:</div>
                <input id="tInpCantidad" type="number" name="cantidad_producto_nuevo" placeholder="Ingrese cantidad del producto" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("cantidad_producto_nuevo", $error); ?> />
            </div>
        </div>
        <?php
        //Si hay errores...
        if ($alertas_errores) {
            //Mostramos los errores
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="submit" value="Crear Nuevo Producto" name="crear-nuevo-producto"/>
        </div>
    </form>
</div>