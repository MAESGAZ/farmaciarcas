<div class="formulario formularioNuevoMedicamento">
    <label>&nbsp;FORMULARIO NUEVO MEDICAMENTO&nbsp;</label>
    <label>&nbsp;NUEVO MEDICAMENTO&nbsp;</label>
    <form action="../medicamentos/nuevo_medicamento.php" method="POST">
        <div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_medicamento_nuevo" placeholder="Ingrese nombre del medicamento" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("nombre_medicamento_nuevo", $error); ?> />
            </div>
            <div>
                <div>Descripción*:</div>
                <textarea id="tInpDescripcion"
                          name="descripcion_medicamento_nuevo"
                          placeholder="Ingrese una descripción del medicamento"
                          ><?php f_mostrar_contenido_textarea('crear-nuevo-medicamento', 'descripcion_medicamento_nuevo', $valor_bd); ?></textarea>
            </div>
            <div>
                <div>Restricciones del Medicamento*:</div>
                <div>
                    <div>
                        <input id="tRadMedSoloConReceta" type="radio" name="restricciones_medicamento_nuevo" value="CON receta" <?php f_mostrar_botonRadio_marcado_nuevo("crear-nuevo-medicamento", "restricciones_medicamento_nuevo", "CON receta"); ?>/>
                        <label for="tRadMedSoloConReceta">Sólo CON receta</label>
                    </div>
                    <div>
                        <input id="tRadMedSoloSinReceta" type="radio" name="restricciones_medicamento_nuevo" value="SIN receta" <?php f_mostrar_botonRadio_marcado_nuevo("crear-nuevo-medicamento", "restricciones_medicamento_nuevo", "SIN receta"); ?>/>
                        <label for="tRadMedSoloSinReceta">Sólo SIN receta</label>
                    </div>
                    <div>
                        <input id="tRadMedNingunaRestriccion" type="radio" name="restricciones_medicamento_nuevo" value="Ninguna restricción" <?php f_mostrar_botonRadio_marcado_nuevo("crear-nuevo-medicamento", "restricciones_medicamento_nuevo", "Ninguna restricción"); ?> />
                        <label for="tRadMedNingunaRestriccion">Ninguna restricción</label>
                    </div>
                </div>
            </div>
            <div>
                <div>Precio Abonado ( con receta ):</div>
                <input id="tInpPrecioAbono" type="number" step="any" name="precio_medicamento_abono_nuevo" placeholder="Ingrese precio abonado del medicamento" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("precio_medicamento_abono_nuevo", $error); ?> />
            </div>
            <div>
                <div>Precio PVP:</div>
                <input id="tInpPrecioPVP" type="number" step="any" name="precio_medicamento_pvp_nuevo" placeholder="Ingrese precio pvp del medicamento" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento('precio_medicamento_pvp_nuevo', $error); ?> />
            </div>
            <div>
                <div>Cantidad*:</div>
                <input id="tInpCantidad" type="number" name="cantidad_medicamento_nuevo" placeholder="Ingrese cantidad del medicamento" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("cantidad_medicamento_nuevo", $error); ?> />
            </div>
        </div>
        <?php
        //Si hay errores...
        if ($alertas_errores) {
            //Mostramos los errores
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="submit" value="Crear Nuevo Medicamento" name="crear-nuevo-medicamento"/>
        </div>
    </form>
</div>