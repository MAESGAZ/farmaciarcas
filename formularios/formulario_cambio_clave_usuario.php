<div class="formularioCambiarClave">
    <div class="desplegable-cambiar-clave">CAMBIAR CLAVE</div>
    <div class="contenedor-formCambClave">
        <form  class="form-cambiar-clave" action="../index/index.php" method="POST">
            <div class="div-form-cambiar-clave">
                <label class="label-form-cambiar-clave">Clave existente*:</label>
                <input class="input-form-cambiar-clave" type="password" name="clave_existente_usuario" placeholder="Introduce clave existente" />
            </div>
            <div class="div-form-cambiar-clave">
                <label class="label-form-cambiar-clave">Nueva clave*:</label>
                <input class="input-form-cambiar-clave" type="password" name="nueva_clave_usuario" placeholder="Introduce nueva clave" />
            </div>
            <div class="div-form-cambiar-clave">
                <label class="label-form-cambiar-clave">Repita clave*:</label>
                <input class="input-form-cambiar-clave" type="password" name="repita_nueva_clave_usuario" placeholder="Repita nueva clave" />
            </div>
            <div class="div-form-cambiar-clave">
                <input class="input-form-cambiar-clave" type="submit" name="cambiar-clave-usuario" value="Enviar" />
            </div>
        </form>
    </div>
</div>