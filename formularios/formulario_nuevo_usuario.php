<div class="formulario formularioNuevoUsuario">
    <label>&nbsp;FORMULARIO NUEVO USUARIO&nbsp;</label>
    <label>&nbsp;NUEVO USUARIO&nbsp;</label>
    <form action="../usuarios/nuevo_usuario.php" method = "POST">
        <div>
            <div>
                <div>Usuario*:</div>
                <input id="tInpUsuario" type="text" name="usuario_nuevo" placeholder="Ingrese su usuario" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento('usuario_nuevo', $error); ?> />
            </div>
            <div>
                <div>Clave*:</div>
                <input id="tInpClave" type="password" name="clave_usuario_nuevo" placeholder="Ingrese su clave" autocomplete="off" />
            </div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_usuario_nuevo" placeholder="Ingrese su nombre" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento('nombre_usuario_nuevo', $error); ?> />
            </div>
            <div>
                <div>Apellidos*:</div>
                <input id="tInpApellidos" type="text" name="apellidos_usuario_nuevo" placeholder="Ingrese sus apellidos" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento('apellidos_usuario_nuevo', $error); ?> />
            </div>
            <div>
                <div>Género*:</div>
                <div>
                    <div>                                                                                                                       
                        <input id="tRadMasculino" type="radio" name="genero_usuario_nuevo" value="M" <?php f_mostrar_botonRadio_marcado_nuevo("crear-nuevo-usuario", "genero_usuario_nuevo", "M"); ?>>
                        <label for="tRadMasculino">Masculino</label>
                    </div>
                    <div>
                        <input id="tRadFemenino" type="radio" name="genero_usuario_nuevo" value="F" <?php f_mostrar_botonRadio_marcado_nuevo("crear-nuevo-usuario", "genero_usuario_nuevo", "F"); ?>>
                        <label for="tRadFemenino">Femenino</label>
                    </div>
                </div>
            </div>
            <div>
                <div>Teléfono de contacto*:</div>
                <input id="tInpTelefono" type="number" name="telefono_usuario_nuevo" placeholder="666666666" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento('telefono_usuario_nuevo', $error); ?> />
            </div>
            <div>
                <div>Email*:</div>
                <input id="tInpEmail" type="text" name="email_usuario_nuevo" placeholder="example@domain.com" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento('email_usuario_nuevo', $error); ?> />
            </div>
        </div>
        <?php
        //Si hay alertas
        if ($alertas_errores) {
            //Mostrar el error guardado en el array
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input name="crear-nuevo-usuario" type="submit" value="Crear Nuevo Usuario" />
        </div>
    </form>
</div>