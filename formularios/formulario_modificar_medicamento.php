<?php
//Realizamos una consulta para poder obtener toda la información del cliente que queremos modificar
//Guardamos el resultado obtenido en una variable
$fila = f_buscar_informacion_medicamento_solicitado($conexionPDO);
?>
<div class="formulario formularioModificarMedicamento">
    <label>&nbsp;FORMULARIO MODIFICAR MEDICAMENTO&nbsp;</label>
    <label>&nbsp;MODIFICAR MEDICAMENTO&nbsp;</label>
    <form action="../medicamentos/modificar_medicamento.php" method="POST">
        <div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_modificar_medicamento" placeholder="Ingrese nombre del medicamento" autocomplete="off" <?php f_mostrar_campo_modificar_medicamento('modificar-medicamento', $fila['nombre'], 'nombre_modificar_medicamento', $error, $alertas_errores); ?> />
            </div>
            <div>
                <div>Descripción*:</div>
                <textarea id="tInpDescripcion"
                          name="descripcion_modificar_medicamento"
                          placeholder="Ingrese una descripción del medicamento"
                          ><?php f_mostrar_contenido_textarea('modificar-medicamento', 'descripcion_modificar_medicamento', $fila['descripcion']); ?></textarea>
            </div>
            <div>
                <div>Restricciones del Medicamento*:</div>
                <div>
                    <div>
                        <input id="tRadMedSoloConReceta" type="radio"  name="restricciones_medicamento_modificar" value="CON receta" <?php f_mostrar_botonRadio_marcado_modificar("restricciones_medicamento_modificar", $fila['restricciones_medicamento'], "CON receta"); ?>/>
                        <label for="tRadMedSoloConReceta">Sólo CON receta</label>
                    </div>
                    <div>
                        <input id="tRadMedSoloSinReceta" type="radio" name="restricciones_medicamento_modificar" value="SIN receta" <?php f_mostrar_botonRadio_marcado_modificar("restricciones_medicamento_modificar", $fila['restricciones_medicamento'], "SIN receta"); ?>/>
                        <label for="tRadMedSoloSinReceta">Sólo SIN receta</label>
                    </div>
                    <div>
                        <input id="tRadMedNingunaRestriccion" type="radio" name="restricciones_medicamento_modificar" value="Ninguna restricción" <?php f_mostrar_botonRadio_marcado_modificar("restricciones_medicamento_modificar", $fila['restricciones_medicamento'], "Ninguna restricción"); ?> />
                        <label for="tRadMedNingunaRestriccion">Ninguna restricción</label>
                    </div>
                </div>
            </div>
            <div>
                <div>Precio Abonado ( con receta ):</div>
                <input id="tInpPrecioAbono" type="number" step="any" name="precio_abono_modificar_medicamento" placeholder="Ingrese precio abonado del medicamento" autocomplete="off" <?php f_mostrar_campo_modificar_medicamento('modificar-medicamento', $fila['precio_abono'], 'precio_abono_modificar_medicamento', $error, $alertas_errores); ?> />
            </div>
            <div>
                <div>Precio PVP:</div>
                <input id="tInpPrecioPVP" type="number" step="any" name="precio_pvp_modificar_medicamento" placeholder="Ingrese precio pvp del medicamento" autocomplete="off" <?php f_mostrar_campo_modificar_medicamento('modificar-medicamento', $fila['precio_pvp'], 'precio_pvp_modificar_medicamento', $error, $alertas_errores); ?> />
            </div>
            <div>
                <div>Cantidad*:</div>
                <input id="tInpCantidad" type="number" name="cantidad_modificar_medicamento" placeholder="Ingrese cantidad del medicamento" autocomplete="off" <?php f_mostrar_campo_modificar_medicamento('modificar-medicamento', $fila['cantidad'], 'cantidad_modificar_medicamento', $error, $alertas_errores); ?> />
            </div>
        </div>
        <?php
        //Si hay errores...
        if ($alertas_errores) {
            //Mostramos los errores
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="hidden" name="id_medicamento_hidden" value="<?php echo $fila['id'] ?>" />
            <input type="submit" value="Modificar Medicamento" name="modificar-medicamento"/>
        </div>
    </form>
</div>
