<div class="contenedor-todo-control-acceso">
    <form class="formulario-control-acceso" action="./index.php" method="POST">  
        <div>
            <div>
                <h1>Iniciar sesión</h1>
            </div>
            <div>
                <label>Usuario:</label>
                <div>
                    <div>
                        <img src="../../imagenes/usuario_blanco.png" alt="Usuario" />
                    </div>
                    <input id="usuario" type="text" name="usuario"  placeholder="campo obligatorio*" autocomplete="off" 
                    <?php
                    if (f_validar_existencia_campo_en_bd('entrar', 'usuario', 'usuario', 'usuarios') == false) {
                        f_colorear_rojo_texto_error('usuario');
                    } f_mostrar_campo('usuario');
                    ?> 
                           />
                </div>
            </div>
            <div>
                <label>Clave:</label>
                <div>
                    <div>
                        <img src="../../imagenes/clave_blanco.png" alt="Clave" />
                    </div>
                    <input id="clave" type="password" name="clave"  placeholder="campo obligatorio*" autocomplete="off" />
                </div>
            </div>
        </div>
        <div class="contenedor-clave-olvidada">
            <div><a class="a-clave-olvidada" href="../olvido_clave/olvido_clave.php">He olvidado mi clave</a></div>
        </div>
        <?php
        //Si hay alertas
        if ($alertas_errores) {
            //Mostrar las alertas
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="submit" value="Entrar" id="entrar" name="entrar"/>
        </div>
    </form>
    <form class="form-boton-nuevo-usuario" action="../usuarios/nuevo_usuario.php" method="POST">
        <input type="submit" class="input-crear-nuevo-usuario" name="boton-crear-nuevo-usuario" value="Nuevo usuario" />
    </form>
</div>