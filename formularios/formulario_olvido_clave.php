<div class="formularioOlvidoClave">
    <form action="<?php $_SERVER["PHP_SELF"] ?>" method="POST">
        <div>
            <h1>¿Has olvidado tu contraseña?</h1>
        </div>
        <div>
            <label>Introduce tu correo electrónico:</label>
            <div>
                <div>
                    <img src="../../imagenes/usuario_blanco.png" alt="Usuario" />
                </div>
                <input id="tInpCorreo" type="text" name="correo" placeholder="example@domain.com" autocomplete="off" <?php f_mostrar_campo_envio_email('envio_email', 'correo', $error); ?> />
            </div>
        </div>
        <div>
            <input type="submit" name="envio_email" value="Enviar"/>
        </div>
    </form>
</div>