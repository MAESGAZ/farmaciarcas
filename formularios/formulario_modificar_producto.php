<?php
//Realizamos una consulta para poder obtener toda la información del cliente que queremos modificar
//Guardamos el resultado obtenido en una variable
$fila = f_buscar_informacion_producto_solicitado($conexionPDO);
?>
<div class="formulario formularioModificarProducto">
    <label>&nbsp;FORMULARIO MODIFICAR PRODUCTO&nbsp;</label>
    <label>&nbsp;MODIFICAR PRODUCTO&nbsp;</label>
    <form action="../productos/modificar_producto.php" method="POST">
        <div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_modificar_producto" placeholder="Ingrese nombre del producto" autocomplete="off"  <?php f_mostrar_campo_modificar_cliente_producto($fila['nombre'], 'nombre_modificar_producto', $error); ?> />
            </div>
            <div>
                <div>Descripción*:</div>
                <textarea id="tInpDescripcion"
                          name="descripcion_modificar_producto" 
                          placeholder="Ingrese una descripción del producto" 
                          ><?php f_mostrar_contenido_textarea('modificar-producto', 'descripcion_modificar_producto', $fila['descripcion']); ?></textarea>
            </div>
            <div>
                <div>Precio*:</div>
                <input id="tInpPrecio" type="number" step="any" name="precio_modificar_producto" placeholder="Ingrese precio del producto" autocomplete="off" <?php f_mostrar_campo_modificar_cliente_producto($fila['precio'], 'precio_modificar_producto', $error); ?> />
            </div>
            <div>
                <div>Cantidad*:</div>
                <input id="tInpCantidad" type="number" name="cantidad_modificar_producto" placeholder="Ingrese cantidad del producto" autocomplete="off" <?php f_mostrar_campo_modificar_cliente_producto($fila['cantidad'], 'cantidad_modificar_producto', $error); ?> />
            </div>
        </div>
        <?php
        //Si hay errores...
        if ($alertas_errores) {
            //Mostramos los errores
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="hidden" name="id_producto_hidden" value="<?php echo $fila['id'] ?>" />
            <input type="submit" value="Modificar Producto" name="modificar-producto"/>
        </div>
    </form>
</div>