<div class="formulario formularioNuevoCliente">
    <label>&nbsp;FORMULARIO NUEVO CLIENTE&nbsp;</label>
    <label>&nbsp;NUEVO CLIENTE&nbsp;</label>
    <form id="formulario" action="../clientes/nuevo_cliente.php" method="POST">
        <div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_cliente_nuevo" placeholder="Ingrese su nombre" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("nombre_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>Apellidos*:</div>
                <input id="tInpApellidos" type="text" name="apellidos_cliente_nuevo" placeholder="Ingrese sus Apellidos" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("apellidos_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>Fecha de nacimiento*:</div>
                <input id="tInpFechaNac" type="date" name="fecha_nacimiento_cliente_nuevo" autocomplete="off" max="9999-12-31" <?php f_mostrar_campo_nuevo_elemento("fecha_nacimiento_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>DNI*:</div>
                <input id="tInpDNI" type="text" name="dni_cliente_nuevo" placeholder="00000000A" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("dni_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>Nº Afiliación Seg. Social*:</div>
                <input id="tInpNumAfiSS" type="number" name="num_afi_ss_cliente_nuevo" placeholder="000000000000" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("num_afi_ss_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>Dirección*:</div>
                <input id="tInpDireccion" type="text" name="direccion_cliente_nuevo" placeholder="Ingrese una dirección" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("direccion_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>Email*:</div>
                <input id="tInpEmail" type="text" name="email_cliente_nuevo" placeholder="example@domain.com" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("email_cliente_nuevo", $error); ?> />
            </div>
            <div>
                <div>Teléfono de contacto*:</div>
                <input id="tInpTelefono" type="number" name="telefono_cliente_nuevo" placeholder="666666666" autocomplete="off" <?php f_mostrar_campo_nuevo_elemento("telefono_cliente_nuevo", $error); ?> />
            </div>
        </div>
        <?php
        //Si hay errores...
        if ($alertas_errores) {
            //Mostramos los errores
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="submit" value="Crear Nuevo Cliente" name="crear-nuevo-cliente"/>
        </div>
    </form>
</div>