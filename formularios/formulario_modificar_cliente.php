<?php
//Realizamos una consulta para poder obtener toda la información del cliente que queremos modificar
//Guardamos el resultado obtenido en una variable
$fila = buscar_informacion_cliente_solicitado($conexionPDO);
?>
<div class="formulario formularioModificarCliente">
    <label>&nbsp;FORMULARIO MODIFICAR CLIENTE&nbsp;</label>
    <label>&nbsp;MODIFICAR CLIENTE&nbsp;</label>
    <form action="./modificar_cliente.php" method="POST" novalidate="novalidate">
        <div>
            <div>
                <div>Nombre*:</div>
                <input id="tInpNombre" type="text" name="nombre_modificar_cliente" placeholder="Ingrese su nombre" <?php f_mostrar_campo_modificar_cliente_producto($fila['nombre'], 'nombre_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
            <div>
                <div>Apellidos*:</div>
                <input id="tInpApellidos" type="text" name="apellidos_modificar_cliente" placeholder="Ingrese sus Apellidos" <?php f_mostrar_campo_modificar_cliente_producto($fila['apellidos'], 'apellidos_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
            <div>
                <div>Fecha de nacimiento*:</div>
                <input id="tInpFechaNac" type="date" name="fecha_nacimiento_modificar_cliente" min="1900-01-01" max="9999-12-31" <?php f_mostrar_campo_modificar_cliente_producto($fila['fecha_nacimiento'], 'fecha_nacimiento_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
            <div>
                <div>DNI*:</div>
                <input id="tInpDNI" type="text" name="dni_modificar_cliente" placeholder="00000000A" <?php f_mostrar_campo_modificar_cliente_producto($fila['dni'], 'dni_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
            <div>
                <div>Nº Afiliación Seg. Social*:</div>
                <input id="tInpNumAfiSS" type="number" name="num_afi_ss_modificar_cliente" placeholder="000000000000" autocomplete="off" <?php f_mostrar_campo_modificar_cliente_producto($fila['num_afi_ss'], "num_afi_ss_modificar_cliente", $error); ?> />
            </div>
            <div>
                <div>Dirección*:</div>
                <input id="tInpDireccion" type="text" name="direccion_modificar_cliente" placeholder="Ingrese una dirección" <?php f_mostrar_campo_modificar_cliente_producto($fila['direccion'], 'direccion_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
            <div>
                <div>Email*:</div>
                <input id="tInpEmail" type="text" name="email_modificar_cliente" placeholder="example@domain.com" <?php f_mostrar_campo_modificar_cliente_producto($fila['email'], 'email_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
            <div>
                <div>Teléfono de contacto*:</div>
                <input id="tInpTelefono" type="number" name="telefono_modificar_cliente" placeholder="666666666" <?php f_mostrar_campo_modificar_cliente_producto($fila['telefono'], 'telefono_modificar_cliente', $error); ?> autocomplete="off" />
            </div>
        </div>
        <?php
        //Si hay alertas...
        if ($alertas_errores) {
            //Mostramos las alertas
            f_mostrar_alerta_errores($alertas_errores);
        }
        ?>
        <div>
            <input type="hidden" name="id_cliente_hidden" value="<?php echo $fila['id'] ?>" />
            <input type="submit" value="Modificar Cliente" name="modificar-cliente"/>
        </div>
    </form>
</div>