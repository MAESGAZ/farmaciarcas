<?php

// Notificar todos los errores excepto E_NOTICE y E_WARNING
error_reporting(E_ALL ^ E_NOTICE ^ E_WARNING);

//Función para mostrar la alerta de los ERRORES
function f_mostrar_alerta_errores($alertas_errores) {
    echo "<script>swal({title: '¡¡¡ ERROR !!!',text: ";
    echo '"';
    echo '<div>';
    echo '<ul>';
    foreach ($alertas_errores as $alerta_error) {
        echo '<li>' . $alerta_error . '</li>';
    }
    echo '</ul>';
    echo '</div>';
    echo '"';
    echo ", html: true, type: 'error'} ";
    echo ");</script>";
}

//Función para mostrar la alerta de los ERRORES
function f_mostrar_alerta_exitos($alertas_exitos) {
    echo "<script>
        setTimeout(function () {
            swal({title: '¡¡¡ CORRECTO !!!',text: ";
    echo '"';
    echo '<ul>';
    foreach ($alertas_exitos as $alerta_exito) {
        echo '<li>' . $alerta_exito . '</li>';
    }
    echo '</ul>';
    echo '"';
    echo ",html: true, type: 'success'});
        }, 200);
    </script>";
}

//Función para mostrar la alerta de los ÉXITOS (nuevo elemento)
function f_mostrar_alerta_exito_nuevoElemento($alertas_exitos, $ruta) {
    echo "<script>swal({title: '¡¡¡ CORRECTO !!!', text: ";
    echo '"';
    echo '<ul>';
    foreach ($alertas_exitos as $alerta_exito) {
        echo '<li>' . $alerta_exito . '</li>';
    }
    echo '</ul>';
    echo '"';
    echo ", html: true, type: 'success'} ";
    echo ", function() {";
    echo " window.location = '" . $ruta . "';";
    echo "}";
    echo ");";
    echo "</script>";
}

//Función para mostrar la alerta de los ÉXITOS (nuevo elemento)
function f_mostrar_alerta_exito_nuevoUsuario($alertas_exitos, $ruta) {
    echo "
        <script>
        setTimeout(function () {
                    swal({
                        title: '¡¡¡ CORRECTO !!!',
                        text: '";
    echo '<ul>';
    foreach ($alertas_exitos as $alerta_exito) {
        echo '<li style="text-align: left;">' . $alerta_exito . '</li>';
    }
    echo '</ul>';
    echo "',
                        html: true,
                        type: 'success'
                    }, function () {
                            window.location = '" . $ruta . "';
                    });
        }, 200);
        </script>                                    
    ";
}

//Función para mostrar la alerta de los ÉXITOS (modificación de un elemento)
function f_mostrar_alerta_exito_modifClave($elemento, $alertas_exitos, $ruta) {
    echo "
        <script>
        setTimeout(function () {
            swal({
                title: '¿Estás seguro?',
                text: '¿Realmente quieres modificar la " . $elemento . "?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: 'var(--green-600)',
                confirmButtonText: 'Sí, modificar',
                closeOnConfirm: false
            },
                function () {
                    swal({
                        title: '¡¡¡ MODIFICADO !!!',
                        text: '";
    echo '<ul>';
    foreach ($alertas_exitos as $alerta_exito) {
        echo '<li style="text-align: left;">' . $alerta_exito . '</li>';
    }
    echo '</ul>';
    echo "',
                        html: true,
                        type: 'success',
                        closeOnConfirm: false
                    }, function (isConfirm) {
                        if (isConfirm) {
                            window.location = '" . $ruta . "';
                        }
                    });
                }
            );
        }, 200);
        </script>                                    
    ";
}

//Función para mostrar la alerta de los ÉXITOS (modificación de un elemento)
function f_mostrar_alerta_exito_modifElemento($elemento, $alertas_exitos, $ruta) {
    echo "<script>";
    echo "setTimeout(function () {
                swal({
                    title: '¿Estás seguro?',
                    text: '¿Realmente quieres modificar el " . $elemento . "?',
                    type: 'warning',
                    showCancelButton: true,
                    cancelButtonText: 'Cancelar',
                    confirmButtonColor: 'var(--green-600)',
                    confirmButtonText: 'Sí, modificar',
                    closeOnConfirm: false
                },
                    function (isConfirm) {
                            if(isConfirm) {";
    echo "swal({title: '¡¡¡ MODIFICADO !!!', text: ";
    echo '"';
    echo '<ul>';
    foreach ($alertas_exitos as $alerta_exito) {
        echo '<li>' . $alerta_exito . '</li>';
    }
    echo '</ul>';
    echo '"';
    echo ", html: true, type: 'success', closeOnConfirm: false } ";
    echo ", function(isConfirm) {";
    echo "if(isConfirm){";
    echo " window.location = '" . $ruta . "';";
    echo "}";
    echo "});"; // 2º swal termina aquí
    echo "}";
    echo "}";
    echo ");"; // 1er swal termina aquí
    echo "}, 200);";
    echo "</script>";
}

function f_mostrar_alerta_para_eliminarElemento($elemento, $ruta_eliminar) {
    echo "
        <script>
        setTimeout(function () {
            swal({
                title: '¿Estás seguro?',
                text: '¿Realmente quieres eliminar el " . $elemento . "?',
                type: 'warning',
                showCancelButton: true,
                cancelButtonText: 'Cancelar',
                confirmButtonColor: 'red',
                confirmButtonText: 'Sí, eliminar',
                closeOnConfirm: false
            },
                function (isConfirm) {
                    if(isConfirm) {
                        window.location = '" . $ruta_eliminar . "';
                    }
                },    
            );
        }, 200);
        </script>                                    
    ";
}

//Función para mostrar la alerta de los ÉXITOS (eliminar un elemento)
function f_mostrar_alerta_exito_elimElemento($elemento, $alertas_exitos, $ruta) {
    echo "
        <script>
        setTimeout(function () {
            swal({
                title: '¡¡¡ ELIMINADO !!!',";
    echo "text: '<ul>";
    foreach ($alertas_exitos as $alerta_exito) {
        echo '<li>' . $alerta_exito . '</li>';
    }
    echo "</ul>',";
    echo "html: true,
                type: 'success',
                closeOnConfirm: false
            }, function (isConfirm) {
                    if (isConfirm) {
                        window.location = '" . $ruta . "';
                    }  
               }
            );
        }, 200);
        </script>                                    
    ";
}

//Función para mostrar un campo de un nuevo elemento
function f_mostrar_campo($campo) {
    //Si no está vacío el campo en el formulario
    if (!empty($_POST[$campo]))
        //Mostramos el valor del campo
        echo 'value="' . $_POST[$campo] . '" ';
}

//Función para mostrar un campo formulario envío email
function f_mostrar_campo_envio_email($boton, $campo, $error) {
    if (!empty($_POST[$boton])) {
        //Si no está vacío el campo en el formulario
        if (!empty($_POST[$campo])) {
            //Mostramos el valor del campo
            echo 'value="' . $_POST[$campo] . '" ';

            //Si hay error
            if ($error) {
                //Coloreamos de rojo el texto erróneo
                f_colorear_rojo_texto_error($campo);
            }
        }
    }
}

//Colorear un placeholder de un campo vacío
function f_colorear_placeholder_input_vacio($boton, $campo, $id) {
    if (!empty($_POST[$boton])) {
        if (empty($_POST[$campo])) {
            //Imprimimos la etiqueta <style>
            echo '<style>';
            echo ' #' . $id . '::placeholder {';
            echo ' color: var(--red-200);';
            echo '} ';
            echo '</style>';
        }
    }
}

//Función para mostrar un campo de un nuevo elemento
function f_mostrar_campo_nuevo_elemento($campo, $error) {
    //Si no está vacío el campo en el formulario
    if (!empty($_POST[$campo]))
        //Mostramos el valor del campo
        echo 'value="' . $_POST[$campo] . '" ';

    //Si hay error
    if ($error) {
        //Recorremos el array de error
        foreach ($error as $value) {
            //Si el $value tiene el mismo valor que el $campo
            if ($value == $campo)
                //Coloreamos de rojo el texto erróneo
                f_colorear_rojo_texto_error($campo);
        }
    }
}

//Función para mostrar un campo elemento a modificar
function f_mostrar_campo_modificar_cliente_producto($valor_campo_en_bd, $campo, $error) {
    //Si hay error
    if ($error) {
        //Mostramos el valor del campo
        echo 'value="' . $_POST[$campo] . '" ';

        //Recorremos el array de error
        foreach ($error as $value) {
            //Si el $value tiene el mismo valor que el $campo
            if ($value == $campo) {
                //Coloreamos de rojo el texto erróneo
                f_colorear_rojo_texto_error($campo);
            }
        }
    } else {
        echo 'value="' . $valor_campo_en_bd . '" ';
    }
}

//Función para mostrar un campo elemento a modificar
function f_mostrar_campo_modificar_medicamento($boton, $valor_campo_en_bd, $campo, $error, $errores) {
    //Si NO está vacío el botón
    if (!empty($_POST[$boton])) {
        //Si NO ESTÁ RELLENO el array ERROR
        if ($error) {
            //Recorremos el array ERROR
            foreach ($error as $value) {
                //Si el $value tiene el mismo valor que el $campo
                if ($value == $campo) {
                    //Mostramos el valor del campo
                    echo ' value="' . $_POST[$campo] . '"';
                    //Coloreamos de rojo el texto erróneo
                    f_colorear_rojo_texto_error($campo);
                }
            }
        }

        //Si hay ERRORES
        if ($errores) {
            echo ' value="' . $_POST[$campo] . '"';
        }
        //Si NO hay errores
        else {
            echo ' value="' . $valor_campo_en_bd . '"';
        }
    } else {
        echo 'value="' . $valor_campo_en_bd . '" ';
    }
}

//Función para mostrar el contenido de un textarea
function f_mostrar_contenido_textarea($boton, $campo_input, $valor_bd) {
    $texto = 'crear-nuevo-';
    $posicion = strpos($boton, $texto);
    if ($posicion === 0) {
        //Si no está vacío el campo en el formulario
        if (!empty($_POST[$campo_input]))
            //Mostramos el valor del campo
            echo $_POST[$campo_input];
    } else {
        if (!empty($_POST[$boton])) {
            if (!empty($_POST[$campo_input])) {
                echo $_POST[$campo_input];
            }
        } else {
            echo $valor_bd;
        }
    }
}

//Funcion para colorear el rojo los errores de los input de los formularios
function f_colorear_rojo_texto_error($campo) {
    if (!empty($_POST[$campo])) {
        echo ' style="';
        echo 'color: red';
        echo '; ';
        echo 'font-weight: bolder';
        echo '; ';
        echo 'letter-spacing: 0.2rem';
        echo ';" ';
    }
}

//Función para mostrar un boton radio marcado
function f_mostrar_botonRadio_marcado_nuevo($boton, $campo_input, $value_input) {
    if (!empty($_POST[$boton])) {
        if (!empty($_POST[$campo_input]) && $_POST[$campo_input] == $value_input) {
                echo ' checked';
        }
    } else {
        if ($value_input == 'Ninguna restricción')
            echo ' checked';
    }
}

//Función para mostrar un boton radio marcado
function f_mostrar_botonRadio_marcado_modificar($campo_input, $valor_bd, $value_input) {
    if (!empty($_POST[$campo_input])) {
        if ($_POST[$campo_input] == $value_input) {
            echo ' checked';
        }
    } else {
        if (isset($_POST[$campo_input])) {
            echo ' checked';
        } else {
            if ($valor_bd == $value_input)
                echo ' checked';
        }
    }
}

//Función para validar un campo de una cadena de texto
function f_validar_campo_cadena_texto($texto) {
// Función TRIM() para eliminar espacios delante y detrás del texto
    $texto = trim($texto);
    if ($texto == "") {
        return false;
    } else {
        $patron = '/^[a-zA-ZáéíóúÁÉÍÓÚäëïöüÄËÏÖÜàèìòùÀÈÌÒÙñÑ0-9(),.\-_\w\/\s]+$/';
        if (preg_match($patron, $texto)) {
            return true;
        } else {
            return false;
        }
    }
}

//Función para validar un campo numérico
function f_validar_campo_numerico($numero) {
    if (is_numeric($numero)) {
        if ($numero >= 0) {
            if (filter_var($numero, FILTER_VALIDATE_INT)) {
                return true;
            } elseif (filter_var($numero, FILTER_VALIDATE_FLOAT)) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//Función para VALIDAR una FECHA intrducida por el usuario
function f_validar_fecha_entrada($fecha_introducida_usuario) {
    if (((list($anio, $mes, $dia) = preg_split('[/|-| |:|\*|\.|\+|\?]', $fecha_introducida_usuario)) == true) && ((strlen($fecha_introducida_usuario) == 10))) {
        if (strlen($anio) == 4) {
            return $anio . '-' . $mes . '-' . $dia;
        } else {
            return $dia . '-' . $mes . '-' . $anio;
        }
    } else {
        return false;
    }
}

//Función para VALIDAR una FECHA de salida por pantalla en formato español
function f_validar_fecha_salida($fecha_introducida_usuario) {
    if ((list($anio, $mes, $dia) = preg_split('[/|-| |:|\*|\.|\+|\?]', $fecha_introducida_usuario)) == true) {
        return $dia . '/' . $mes . '/' . $anio;
    }
}

//Función para validar el DNI en una inserción de datos en un formulario
function f_validar_DNI($dni) {
    $letra = substr($dni, -1);
    $letra = strtoupper($letra);
    $numeros = substr($dni, 0, -1);
    if ((substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros % 23, 1) == $letra) && (strlen($letra) == 1) && (strlen($numeros) == 8)) {
        return true;
    } else {
        return false;
    }
}

//Función para validar el Nº de Afiliación de la Seg. Social
function f_validar_num_ss($num_ss) {
    if (f_validar_campo_numerico($num_ss)) {
        if (strlen($num_ss) == 12) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

//Función para validar el EMAIL en una inserción de datos en un formulario
function f_validar_EMAIL($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        return true;
    } else {
        return false;
    }
}

//Función para validar el TELÉFONO MÓVIL en una inserción de datos en un formulario
function f_validar_telefono($telefono, $campo) {
    $patron = '/^[6|7|8|9][0-9]{8}$/';
    if (f_validar_campo_numerico($telefono)) {
        if (strlen($telefono) == 9) {
            if (preg_match($patron, $telefono)) {
                return true;
            } else {
                if ($campo == 'telefono_cliente_nuevo') {
                    //Guardamos el error en la variable
                    $error_telefono = $campo;
                } else if ($campo == 'telefono_modificar_cliente') {
                    //Guardamos el error en la variable
                    $error_telefono = $campo;
                } else if ($campo == 'telefono_usuario_nuevo') {
                    //Guardamos el error en la variable
                    $error_telefono = $campo;
                }
                $alertas_errores[] = 'El TELÉFONO <b>\"' . $telefono . '\"</b> tiene que empezar por el número: 6, 7, 8 ó 9.';
            }
        } else {
            if ($campo == 'telefono_cliente_nuevo') {
                //Guardamos el error en la variable
                $error_telefono = $campo;
            } else if ($campo == 'telefono_modificar_cliente') {
                //Guardamos el error en la variable
                $error_telefono = $campo;
            } else if ($campo == 'telefono_usuario_nuevo') {
                //Guardamos el error en la variable
                $error_telefono = $campo;
            }
            $alertas_errores[] = 'El TELÉFONO <b>\"' . $telefono . '\"</b> no sigue el formato válido ( longitud 9 digitos ).';
        }
    } else {
        if ($campo == 'telefono_cliente_nuevo') {
            //Guardamos el error en la variable
            $error_telefono = $campo;
        } else if ($campo == 'telefono_modificar_cliente') {
            //Guardamos el error en la variable
            $error_telefono = $campo;
        } else if ($campo == 'telefono_usuario_nuevo') {
            //Guardamos el error en la variable
            $error_telefono = $campo;
        }
        $alertas_errores[] = 'El TELÉFONO <b>\"' . $telefono . '\"</b> no es reconocido como campo numérico válido'
                . ' (las cifras numéricas han de ser valores positivos).';
    }

    return array($error_telefono, $alertas_errores);
}

//Función para personalizar la FECHA que mostramos en el PIE DE PÁGINA
function f_fechaCastellano($fecha) {
    $mes = date('F', strtotime($fecha));
    $anio = date('Y', strtotime($fecha));
    $meses_ES = array("Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre");
    $meses_EN = array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
    $nombreMes = str_replace($meses_EN, $meses_ES, $mes);

    return $nombreMes . " " . $anio;
}

//Función para colorear el input type date en nuevo_cliente.php
function f_colorear_input_type_date_nuevo_cliente($boton, $campo_input, $error) {
    //Si se haya pulsado o no el botón... y el campo SÍ está vacío
    if ((empty($_POST[$boton]) || (!empty($_POST[$boton]))) &&
            empty($_POST[$campo_input])) {
        //Abrimos la etiqueta <style> y damos color gris al input type date
        echo '<style>';
        echo ' input[type=date] {';
        echo ' color: var(--grey-400);';
        echo ' }';
        echo '</style>';
    }

    //Si el botón NO está pulsado y el campo del formulario NO está vacío
    if ((empty($_POST[$boton]) || (!empty($_POST[$boton]))) && !empty($_POST[$campo_input])) {
        //Si NO hay 'error' o 'errores'
        if (!$error) {
            //Abrimos la etiqueta <style> y damos color negro al input type date
            echo '<style>';
            echo ' input[type=date] {';
            echo ' color: var(--green-900);';
            echo ' font-weight: bolder;';
            echo ' letter-spacing: 0.2rem';
            echo ' }';
            echo '</style>';
        }
    }
}

//Función para colorear el input type date en modificar_cliente.php
function f_colorear_input_type_date_modificar_cliente($boton, $campo_input, $error) {
    //Si el botón NO está pulsado 
    if (empty($_POST[$boton])) {
        //Abrimos la etiqueta <style> y damos color negro al input type date
        echo '<style>';
        echo ' input[type=date] {';
        echo ' color: var(--green-900);';
        echo ' font-weight: bolder;';
        echo ' letter-spacing: 0.2rem';
        echo ' }';
        echo '</style>';
    }

    //Si el botón SÍ está pulsado y el campo del formulario NO está vacío
    if (!empty($_POST[$boton]) && !empty($_POST[$campo_input])) {
        //Si NO hay 'error'
        if (!$error) {
            //Abrimos la etiqueta <style> y damos color negro al input type date
            echo '<style>';
            echo ' input[type=date] {';
            echo ' color: var(--green-900);';
            echo ' font-weight: bolder;';
            echo ' letter-spacing: 0.2rem';
            echo ' }';
            echo '</style>';
        }
    }

    //Si el botón SÍ está pulsado y el campo del formulario está vacío
    if (!empty($_POST[$boton]) && empty($_POST[$campo_input])) {
        //Abrimos la etiqueta <style> y damos color gris al input type date
        echo '<style>';
        echo ' input[type=date] {';
        echo ' color: var(--grey-400);';
        echo ' }';
        echo '</style>';
    }
}

//Función para verificar que una fecha de nacimiento introducida nunca sea posterior
//al día de "HOY" en el sistema del ordenador
//function f_verificar_fecha_nacimiento($fecha_nacimiento, $campo) {
function f_verificar_fecha_nacimiento($campo) {
    //Recogemos las 3 variables devueltas por la función ( fecha nacimiento )
    list($anio_nac, $mes_nac, $dia_nac) = f_separar_fecha_tipo_bd(f_validar_fecha_entrada($_POST[$campo]));

    //Recogemos las 3 variables devueltas por la función ( hoy )
    list($anio_hoy, $mes_hoy, $dia_hoy) = f_separar_fecha_tipo_bd(date('Y-m-d'));

    //Si el año de nacimiento es menor al año de hoy
    if ($anio_nac < $anio_hoy) {
        if ($anio_hoy - $anio_nac >= 150) {
            //Calculamos el año "prohibido" para insertar un cliente (tomamos de 
            //referencia que una persona no va a tener más de 150 años, por poner un número redondo)
            $anio_calculado = $anio_hoy - 150;
            if ($campo == 'fecha_nacimiento_cliente_nuevo') {
                //Guardamos el error en la variable
                $error_fecha_nacimiento = $campo;
            } else if ($campo == 'fecha_nacimiento_modificar_cliente') {
                //Guardamos el error en la variable
                $error_fecha_nacimiento = $campo;
            }

            //Guardamos el error en el array de errores de fecha
            $alertas_errores_fecha[] = 'FECHA NACIMIENTO no puede ser anterior a <b>\"' . $anio_calculado . '\"</b>.'
                    . '<br />No se permite insertar un cliente con más de 150 años.';
        }
    }
    //Si el año de nacimiento es mayor al año de hoy
    elseif ($anio_nac > $anio_hoy) {
        if ($campo == 'fecha_nacimiento_cliente_nuevo') {
            //Guardamos el error en la variable
            $error_fecha_nacimiento = $campo;
        } else if ($campo == 'fecha_nacimiento_modificar_cliente') {
            //Guardamos el error en la variable
            $error_fecha_nacimiento = $campo;
        }
        //Guardamos el error en el array de errores de fecha
        $alertas_errores_fecha[] = 'La FECHA NACIMIENTO <b>\"' . f_validar_fecha_salida($_POST[$campo]) . '\"</b> no puede ser posterior al día de hoy.';
    }
    //Si el año de nacimiento es igual al año de hoy
    elseif ($anio_nac == $anio_hoy) {
        //Si el mes de nacimiento es mayor al mes de hoy
        if ($mes_nac > $mes_hoy) {
            if ($campo == 'fecha_nacimiento_cliente_nuevo') {
                //Guardamos el error en la variable
                $error_fecha_nacimiento = $campo;
            } else if ($campo == 'fecha_nacimiento_modificar_cliente') {
                //Guardamos el error en la variable
                $error_fecha_nacimiento = $campo;
            }
            //Guardamos el error en el array de errores de fecha
            $alertas_errores_fecha[] = 'La FECHA NACIMIENTO <b>\"' . f_validar_fecha_salida($_POST[$campo]) . '\"</b> no puede ser posterior al día de hoy.';
        }
        //Si el mes de nacimiento es menor o igual al mes de hoy
        elseif ($mes_nac <= $mes_hoy) {
            //Si el día de nacimiento es mayor al día de hoy
            if ($dia_nac > $dia_hoy) {
                if ($campo == 'fecha_nacimiento_cliente_nuevo') {
                    //Guardamos el error en la variable
                    $error_fecha_nacimiento = $campo;
                } else if ($campo == 'fecha_nacimiento_modificar_cliente') {
                    //Guardamos el error en la variable
                    $error_fecha_nacimiento = $campo;
                }
                //Guardamos el error en el array de errores de fecha
                $alertas_errores_fecha[] = 'La FECHA NACIMIENTO <b>\"' . f_validar_fecha_salida($_POST[$campo]) . '\"</b> no puede ser posterior al día de hoy.';
            }
        }
    }

    return array($error_fecha_nacimiento, $alertas_errores_fecha);
}

//Función para separar fechas del tipo de la base de datos
function f_separar_fecha_tipo_bd($fecha) {
    $fechaSeparada = explode("-", $fecha);
    $anio = $fechaSeparada[0];
    $mes = $fechaSeparada[1];
    $dia = $fechaSeparada[2];
    return array($anio, $mes, $dia);
}

function f_generar_clave_aleatoria() {
    $opciones = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    $clave = "";
    for ($i = 0; $i <= 7; $i++) {
        $clave .= substr($opciones, rand(0, 62), 1);
    }
    return $clave;
}

//Función para enviar un email
function f_enviar_email($mail, $plantilla, $destinatario, $nombre_destinatario, $asunto) {
    try {
        $mail->isSMTP();
        $mail->CharSet = 'utf8_unicode_ci';
        $mail->SMTPDebug = 0;
        $mail->Debugoutput = 'html';
        $mail->Host = 'smtp.gmail.com';
        $mail->Port = 587;
        $mail->SMTPSecure = 'tls';
        $mail->SMTPAuth = true;
        $mail->Username = 'farmaciarcas@gmail.com';
        $mail->Password = 'farmaciarcas2020';
        $mail->setFrom('farmaciarcas@gmail.com', 'FarmaciArcas');
        $mail->addAddress($destinatario, $nombre_destinatario);
        $mail->Subject = $asunto;
        $mail->msgHTML($plantilla);
        $mail->send();
    } catch (Exception $ex) {
        echo "El mensaje no ha podido ser enviado. Mailer Error: {$mail->ErrorInfo}";
    }
}
